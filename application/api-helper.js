var config = require('config');

module.exports = {
    performParametersCheck: function(cb, next) {
        if (config.get('Backend.api.params_check_mode') === 'strict') {
            return cb();
        }

        next();
    }
};