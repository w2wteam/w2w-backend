var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('config');

require('q').longStackSupport = true;

module.exports = function(options, imports, register) {
    var passport = imports.passport;
    var routerDefault = imports['router-default'];
    var wErrorScope = imports.wErrorScope;
    var log = imports.logger;
    var apiHelper = require('./api-helper');

    var app = express();

    app.disable('view engine');
    app.disable('views');

    app.set('jwtTokenSecret', config.get('Backend.token.jwt_token_secret'));

    app.use(favicon(__dirname + '/../public/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());

    if (process.env.NODE_ENV === 'development') {
        app.use(require('errorhandler')({ dumpExceptions: true, showStack: true }));
    }

    app.use(function(req, res, next) {
        res.set({
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST, GET, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
        });
        next();
    });

    app.use(passport.initialize());
    app.use(function(req, res, next) {
        req.performParamsCheck = apiHelper.performParametersCheck;
        next();
    });
    app.use(routerDefault);

    app.use(function(req, res, next) {
        var reason = new wErrorScope.W2WErrorReason(wErrorScope.W2WReasonKeys.WResourceNotFound);
        var internalErr = new wErrorScope.W2WError("Not Found", wErrorScope.W2WErrorCodes.WNotFound, reason);
        internalErr.status = 404;
        next(internalErr);
    });


    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            err.status = err.status || 500;
            res.status(err.status).json(err);
        });
    }

    app.use(function(err, req, res, next) {
        err.status = err.status || 500;
        res.status(err.status).json(err);
    });

    app.set('port', process.env.OPENSHIFT_NODEJS_PORT || config.get('Backend.default_values.port'));
    app.set('host_address', process.env.OPENSHIFT_NODEJS_IP || config.get('Backend.default_values.host'));

    var server = app.listen(app.get('port'), app.get('host_address'), function() {
        log.info('Express server listening on port ' + server.address().port);

        register(null, {
            app: {
                express: app,
                server: server
            }
        });
    });
};
