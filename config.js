/**
 * Created by alex_rudyak on 2/25/15.
 */

module.exports = [
    "./api/routes",
    "./api/controllers",
    "./api/presenters",
    "./core/network/request",
    "./core/security/passport-auth",
    "./core/security/oauth2orize-extensions/simple-token",
    "./core/security/oauth2",
    "./core/data/providers",
    "./core/data/facades/movies",
    "./core/data/collections/finder",
    "./core/data/collections/database-query",
    "./core/data/models/movies",
    "./core/error",
    "./core/validation",
    "./core/loggers/logger-default",
    "./external-services/internal-merging",
    "./external-services/tmdb/global",
    "./database",
    "./application"
];

if (process.env.NODE_ENV === 'development') {
    module.exports.push("./core/caches/redis");
} else {
    module.exports.push("./core/caches/empty");
}