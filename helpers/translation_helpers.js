/**
 * Created by alex_rudyak on 11/10/14.
 */

module.exports.toBoolean = function(value) {
    if (typeof value === 'undefined'
        || value === null) {
        return false;
    }

    if ((typeof value).toLowerCase() === 'string') {
        if (value.toLowerCase() === 'false'
            || value === '0'
            || value === '') {
            return false;
        }
    }

    if (typeof value === 'Number') {
        return Boolean(value);
    }

    return true;
};
