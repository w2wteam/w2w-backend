/**
 * Created by alesanro on 11/16/14.
 */

var _ = require('lodash');
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('config');

module.exports.token = {
    generateAccessToken: function(payload) {
        var defaultPayload = {};
        _.assign(defaultPayload, payload);

        var token = jwt.encode(defaultPayload, config.get('Backend.token.jwt_token_secret'));

        return token;
    },

    generateRefreshToken: function(payload) {
        var defaultPayload = { name: 'refresh_token' };
        _.assign(defaultPayload, payload);

        var refreshToken = jwt.encode(defaultPayload, config.get('Backend.token.jwt_token_secret'));

        return refreshToken;
    },

    decodeAccessToken: function(token) {
        return jwt.decode(token, config.get('Backend.token.jwt_token_secret'));
    }

};

module.exports.date = {
    expiredInWeek: function() {
        return moment().add(7, 'days').toDate();
    },

    now: function() {
        return moment().toDate();
    }
};