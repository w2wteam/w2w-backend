/**
 * Created by alex_rudyak on 11/10/14.
 */

var orm = require('orm');

module.exports.isNotFoundError = function(ormError) {
    return !ormError || ormError.code === orm.ErrorCodes.NOT_FOUND;
}