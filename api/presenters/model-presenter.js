/**
 * Created by alex_rudyak on 2/4/15.
 */

var _ = require('lodash');


function ModelPresenter () {
}

ModelPresenter.prototype.name = 'DefaultModelPresenter';
ModelPresenter.prototype = _.create(Object.prototype);

/*
Instance methods for presenting a model
 */
ModelPresenter.prototype.toShortPreview = function(model) {
    return ModelPresenter.toShortPreviewObject(model);
};
ModelPresenter.prototype.toSearchPreview = function(model) {
    return ModelPresenter.toSearchPreview(model);
};

ModelPresenter.prototype.toClearObject = function(model) {
    return ModelPresenter.toClearObject(model);
};
ModelPresenter.prototype.toFlattenObject = function(model) {
    return ModelPresenter.toFlattenObject(model);
};


/*
Static implementations for instance functions
 */
ModelPresenter.toShortPreviewObject = function(model) {
    return ModelPresenter.toClearObject(model);
};
ModelPresenter.toSearchPreview = function(model) {
    return ModelPresenter.toClearObject(model);
};
ModelPresenter.toClearObject = function(model) {
    if (_.isArray(model)) {
        return model;
    }

    var keys = _.keys(model);
    var obj = {};
    for(var key in keys) {
        var prop = keys[key];
        obj[prop] = model.hasOwnProperty(prop) && !_.isUndefined(model[prop]) ? model[prop] : null;
        if (prop.charAt(0) === '_' || _.contains(prop, 'UpdatedAt')) {
            delete obj[prop];
        }
    }
    return obj;
};
ModelPresenter.toFlattenObject = function(model) {
    return ModelPresenter.toClearObject(model);
};


module.exports.ModelPresenter = ModelPresenter;