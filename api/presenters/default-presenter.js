/**
 * Created by alex_rudyak on 2/4/15.
 */

var _ = require('lodash');
var ModelPresenter = require('./model-presenter').ModelPresenter;

var modelPresenter = new ModelPresenter();

/*
    Default Movie Presenter
 */
function MoviePresenter() {
    ModelPresenter.call(this);
}
MoviePresenter.prototype = _.create(ModelPresenter.prototype);
MoviePresenter.prototype.toSearchPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    var previewObject = {
        id: model.id,
        adult: model.adult,
        title: model.title,
        releaseDate: model.releaseDate,
        posterPath: model.posterPath,
        backdropPath: model.backdropPath,
        runtime: model.runtime,
        status: model.status,
        tmdbId: model.tmdbId,
        tmdbRateAverage: model.tmdbRateAverage,
        tmdbVoteCount: model.tmdbVoteCount
    };

    var userProperties = ['like', 'wantToWatch', 'notToOffer', 'rate'];
    _.forEach(userProperties, function(prop) {
        if (model[prop] !== null && model[prop] !== undefined) {
            previewObject[prop] = model[prop];
        }
    });

    return previewObject;
};
MoviePresenter.prototype.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    return {
        id: model.id,
        title: model.title,
        releaseDate: model.releaseDate,
        posterPath: model.posterPath,
        backdropPath: model.backdropPath,
        runtime: model.runtime,
        status: model.status,
        wantToWatch: model.wantToWatch,
        like: model.like,
        rate: model.rate,
        tmdbRateAverage: model.tmdbRateAverage,
        tmdbVoteCount: model.tmdbVoteCount
    };
};
MoviePresenter.prototype.toFlattenObject = function(model) {
    var genres = _.map(model.genres, function(genre) {
        if (typeof genre !== 'string') {
            return genre.genreLabel;
        }
        return genre;
    });

    model.populateWithGenres(genres);
    model = modelPresenter.toClearObject(model);
    return model;
};

function MovieDetailsPresenter() {
    ModelPresenter.call(this);
    this.moviePresenter = new MoviePresenter();
    this.creditPresenter = new CreditPresenter();
}
MovieDetailsPresenter.prototype = _.create(ModelPresenter.prototype);
MovieDetailsPresenter.prototype.toFlattenObject = function(model) {
    var obj = this.moviePresenter.toFlattenObject(model);
    obj.credits = _.map(obj.credits, this.creditPresenter.toShortPreview);
    return obj;
};

/*
    Default Credit Presenter
 */
function CreditPresenter() {
    ModelPresenter.call(this);
}
CreditPresenter.prototype = _.create(ModelPresenter.prototype);
CreditPresenter.prototype.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    var previewObject = {
        id: model.id,
        creditType: model.creditType,
        mediaType: model.mediaType,
        tmdbId: model.tmdbId,
        movie: model.movie_id,
        person: model.person_id
    };

    var userProperties = ['like', 'character', 'job', 'department'];
    _.forEach(userProperties, function(prop) {
        if (model[prop] !== null && model[prop] !== 'undefined') {
            previewObject[prop] = model[prop];
        }
    });

    _.forEach(['personName', 'personProfilePath', 'moviePosterPath'], function(prop) {
        if (model[prop] !== 'undefined') {
            previewObject[prop] = model[prop];
        }
    });

    return previewObject;
};


/*
    Default Person Presenter
 */
function PersonPresenter() {
    ModelPresenter.call(this);
}
PersonPresenter.prototype = _.create(ModelPresenter.prototype);
PersonPresenter.prototype.toSearchPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    var previewObject = {
        id: model.id,
        tmdbId: model.tmdbId,
        name: model.name,
        profilePath: model.profilePath,
        birthday: model.birthday || null,
        deathday: model.deathday || null
    };

    var userProperties = ['favorite'];
    _.forEach(userProperties, function(prop) {
        if (model[prop] !== null && model[prop] !== undefined) {
            previewObject[prop] = model[prop];
        }
    });

    return previewObject;
};

function PersonDetailsPresenter() {
    ModelPresenter.call(this);
    this.personPresenter = new PersonPresenter();
    this.creditPresenter = new CreditPresenter();
}
PersonDetailsPresenter.prototype = _.create(ModelPresenter.prototype);
PersonDetailsPresenter.prototype.toFlattenObject = function(model) {
    var obj = this.personPresenter.toFlattenObject(model);
    obj.credits = _.map(obj.credits, this.creditPresenter.toShortPreview);
    return obj;
};

/*
    Default Genre Presenter
 */
function GenrePresenter() {
    ModelPresenter.call(this);
}
GenrePresenter.prototype = _.create(ModelPresenter.prototype);


/*
    Default User Account Presenter
 */
function UserAccountPresenter() {
    ModelPresenter.call(this);
}
UserAccountPresenter.prototype = _.create(ModelPresenter.prototype);
UserAccountPresenter.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    return {
        nickname: model.nickname,
        email: model.email,
        firstName: model.firstName,
        lastName: model.lastName
    };
};


/*
    Default UserRelationsToCredit Presenter
 */
function UserRelationsToCreditPresenter() {
    ModelPresenter.call(this);
}
UserRelationsToCreditPresenter.prototype = _.create(ModelPresenter.prototype);
UserRelationsToCreditPresenter.prototype.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    return {
        credit: model.credit_id,
        like: model.like
    };
};


/*
    Default UserRelationsToMovie Presenter
 */
function UserRelationsToMoviePresenter() {
    ModelPresenter.call(this);
}
UserRelationsToMoviePresenter.prototype = _.create(ModelPresenter.prototype);
UserRelationsToMoviePresenter.prototype.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    return {
        movie: model.movie_id,
        wantToWatch: model.wantToWatch,
        like: model.like,
        rate: model.rate,
        notToOffer: model.notToOffer
    };
};


/*
    Default UserRelationsToPerson Presenter
 */
function UserRelationsToPersonPresenter() {
    ModelPresenter.call(this);
}
UserRelationsToPersonPresenter.prototype = _.create(ModelPresenter.prototype);
UserRelationsToPersonPresenter.prototype.toShortPreview = function(model) {
    model = modelPresenter.toClearObject(model);
    return {
        person: model.person_id,
        favorite: model.favorite
    };
};


module.exports = {
    moviePresenter: new MoviePresenter(),
    personPresenter: new PersonPresenter(),
    creditPresenter: new CreditPresenter(),
    genrePresenter: new GenrePresenter(),
    movieDetailsPresenter: new MovieDetailsPresenter(),
    personDetailsPresenter: new PersonDetailsPresenter(),
    userAccountPresenter: new UserAccountPresenter(),
    userToMoviePresenter: new UserRelationsToMoviePresenter(),
    userToPersonPresenter: new UserRelationsToPersonPresenter(),
    userToCreditPresenter: new UserRelationsToCreditPresenter()
};