/**
 * Created by alesanro on 3/19/15.
 */

var _ = require('lodash');

module.exports = function(imports) {
    return {
        wrapResultsForPage: function(page, results) {
            return {
                'page': page,
                'results': results,
                'total_pages': 1,
                'total_results': _.size(results)
            };
        }
    };
};