/**
 * Created by alesanro on 2/4/15.
 */

module.exports = function(options, imports, register) {
    register(null, {
        responsePresenters: require('./default-presenter'),
        responsePaginator: require('./paginator')(imports)
    });
};