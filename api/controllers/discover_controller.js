/**
 * Created by alesanro on 12/26/14.
 */

var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var dbModels = imports.database.models;
    var tmdbApi = imports.tmdbAPI;

    return {
        getMovies: function (req, res) {
            var page = req.query['page'] || 1;

            tmdbApi.discover.getMovies({
                "vote_average.gte": 7,
                "vote_count.gte": 200,
                "sort_by": "vote_average.desc",
                "page": page
            }).then(function (body) {
                return dataProviders.UserRelationsProvider.getMarkedMovies(req.user.id)
                    .then(function (movies) {
                        var updatedMovies = _.map(body.results, function (movie) {
                            var foundMovie = _.find(movies, {tmdbId: movie.id});
                            dbModels.UserRelationsToMovie.markObjectWithMovie(movie, foundMovie);
                            return movie;
                        });
                        res.status(200).json(updatedMovies);
                    });
            }).fail(function (err) {
                res.status(err.status).json(err);
            }).done();
        }
    };
};