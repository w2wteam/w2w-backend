/**
 * Created by alex_rudyak on 1/29/15.
 */

var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paramsCheck = imports.requestParametersCheckerMiddleware.check;

    return {
        getById: function (req, res) {
            var personId = req.params.id;
            var userId = req.user.id;

            dataProviders.PersonProvider.getById(personId)
                .then(function (person) {
                    return dataProviders.PersonProvider.getPersonPopulatedForUser(person, userId);
                })
                .then(responsePresenters.personPresenter.toFlattenObject)
                .then(function (person) {
                    res.status(200).json(person);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        getDetails: function (req, res) {
            var personId = req.params.id;
            var userId = req.user.id;

            dataProviders.PersonProvider.getPersonDetails(personId)
                .then(function (person) {
                    return dataProviders.PersonProvider.getPersonDetailsPopulatedForUser(person, userId);
                })
                .then(_.bind(responsePresenters.personDetailsPresenter.toFlattenObject, responsePresenters.personDetailsPresenter))
                .then(function (person) {
                    res.status(200).json(person);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        postFavorite: [
            _.curry(paramsCheck)(['favorite']),

            function (req, res) {
                var userId = req.user.id;
                var personId = req.params.id;
                var favorite = req.param('favorite', false);

                dataProviders.UserRelationsProvider.updateRelationsToPerson(userId, personId, {
                    "favorite": favorite
                })
                    .then(responsePresenters.userToPersonPresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ]
    };
};