/**
 * Created by alesanro on 11/10/14.
 */

var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paginator = imports.responsePaginator;
    var paramCheck = imports.requestParametersCheckerMiddleware;
    var paramsSyntaxConfig = require('./get-params-syntax');

    return {
        movie: [
            _.curry(paramCheck.query.check)([paramsSyntaxConfig['query']]),

            function (req, res) {
                var query = req.query[paramsSyntaxConfig['query']] || "";
                var userId = req.user.id;
                var page = req.param('page', 1);

                dataProviders.MovieProvider.getSearchMovieWithQuery(query)
                    .then(function (results) {
                        return dataProviders.MovieProvider.getSearchMoviesPopulatedForUser(results, userId);
                    })
                    .then(function(movies) {
                        return _.map(movies, responsePresenters.moviePresenter.toFlattenObject);
                    })
                    .then(_.curry(paginator.wrapResultsForPage)(page))
                    .then(function (results) {
                        res.status(200).json(results);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        person: [
            _.curry(paramCheck.query.check)([paramsSyntaxConfig['query']]),

            function (req, res) {
                var query = req.query[paramsSyntaxConfig['query']] || "";
                var userId = req.user.id;
                var page = req.param('page', 1);

                dataProviders.PersonProvider.getSearchPersonWithQuery(query)
                    .then(function (results) {
                        return dataProviders.PersonProvider.getSearchPeoplePopulatedForUser(results, userId);
                    })
                    .then(function(movies) {
                        return _.map(movies, responsePresenters.personPresenter.toFlattenObject);
                    })
                    .then(_.curry(paginator.wrapResultsForPage)(page))
                    .then(function (results) {
                        res.status(200).json(results);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ]
    };
};