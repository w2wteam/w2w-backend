/**
 * Created by alesanro on 11/15/14.
 */

"use strict";

var _ = require('lodash');
var Q = require('q');
var config = require('config');

module.exports = function(imports) {
    var oauth2 = imports.oauth2schemes;
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paramsCheck = imports.requestParametersCheckerMiddleware.check;

    return {
        postRegisterNewUser: [
            _.curry(paramsCheck)(['nickname', 'email', 'password']),

            function (req, res) {
                var nickname = req.param('nickname');
                var email = req.param('email');
                var password = req.param('password');

                dataProviders.UserAuthenticationProvider.registerUser(nickname, email, password)
                    .then(responsePresenters.userAccountPresenter.toShortPreview)
                    .then(function (user) {
                        res.status(201).json(user);
                    }).fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        postToken: oauth2.token,

        postRefreshToken: oauth2.refreshToken,

        postRevokeToken: function (req, res) {
            dataProviders.UserAuthenticationProvider.revokeBearerToken(req.user)
                .then(function() {
                    res.status(204).end();
                })
                .fail(function(err) {
                    res.status(err.status).json(err);
                });
        }
    };
};