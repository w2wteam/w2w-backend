/**
 * Created by alesanro on 11/10/14.
 */

var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paramsCheck = imports.requestParametersCheckerMiddleware.check;
    var paginator = imports.responsePaginator;

    return {
        getWantList: function (req, res) {
            var userId = req.user.id;
            var page = req.param('page', 1);

            dataProviders.UserRelationsProvider.getWantMovies(userId)
                .then(function(movies) {
                    return _.map(movies, responsePresenters.moviePresenter.toFlattenObject);
                })
                .then(_.curry(paginator.wrapResultsForPage)(page))
                .then(function (movies) {
                    res.status(200).json(movies);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        updateWantList: [
            _.curry(paramsCheck)(['movie']),

            function (req, res) {
                var movieId = req.param('movie');
                var userId = req.user.id;

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, { "wantToWatch": true })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    }).fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        deleteRecordFromWantList: [
            _.curry(paramsCheck)(['movie']),

            function (req, res) {
                var movieId = req.param('movie');
                var userId = req.user.id;

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, { "wantToWatch": false })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    }).fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        getWatchedList: function (req, res) {
            var userId = req.user.id;
            var page = req.param('page', 1);

            dataProviders.UserRelationsProvider.getWatchedMovies(userId)
                .then(function(movies) {
                    return _.map(movies, responsePresenters.moviePresenter.toFlattenObject);
                })
                .then(_.curry(paginator.wrapResultsForPage)(page))
                .then(function (movies) {
                    res.status(200).json(movies);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        }
    };
};