/**
 * Created by alesanro on 11/10/14.
 */

var _  = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paramsCheck = imports.requestParametersCheckerMiddleware.check;

    return {
        getById: function (req, res) {
            var movieId = req.params.id;
            var userId = req.user.id;

            dataProviders.MovieProvider.getById(movieId)
                .then(function (movie) {
                    return dataProviders.MovieProvider.getMoviePopulatedForUser(movie, userId);
                })
                .then(responsePresenters.moviePresenter.toFlattenObject)
                .then(function (movie) {
                    res.status(200).json(movie);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        getDetails: function (req, res) {
            var movieId = req.params.id;
            var userId = req.user.id;

            dataProviders.MovieProvider.getMovieDetails(movieId)
                .then(function (movie) {
                    return dataProviders.MovieProvider.getMovieDetailsPopulatedForUser(movie, userId)
                })
                .then(_.bind(responsePresenters.movieDetailsPresenter.toFlattenObject, responsePresenters.movieDetailsPresenter))
                .then(function (movie) {
                    res.status(200).json(movie);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        postLike: [
            _.curry(paramsCheck)(['like']),

            function (req, res) {
                var movieId = req.params.id;
                var userId = req.user.id;
                var like = req.param('like', 'unknown');

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, {
                    "like": like,
                    "wantToWatch": false
                })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        postRate: [
            _.curry(paramsCheck)(['rate']),

            function (req, res) {
                var movieId = req.params.id;
                var userId = req.user.id;
                var rate = req.param('rate', 1);

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, {
                    "rate": rate
                })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        postWantToWatch: [
            _.curry(paramsCheck)(['want_to_watch']),

            function (req, res) {
                var movieId = req.params.id;
                var userId = req.user.id;
                var wantToWatch = req.param('want_to_watch', false);

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, {
                    "wantToWatch": wantToWatch
                })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        postNotToOffer: [
            _.curry(paramsCheck)(['not_to_offer']),

            function (req, res) {
                var movieId = req.params.id;
                var userId = req.user.id;
                var notToOffer = req.param('not_to_offer', 'unknown');

                dataProviders.UserRelationsProvider.updateRelationsToMovie(userId, movieId, {
                    "notToOffer": notToOffer
                })
                    .then(responsePresenters.userToMoviePresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ],

        getGenres: function (req, res) {
            dataProviders.MovieProvider.getGenresLabels()
                .then(responsePresenters.genrePresenter.toShortPreview)
                .then(function (genres) {
                    res.status(200).json(genres);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        }
    };
};