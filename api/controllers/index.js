/**
 * Created by alesanro on 11/10/14.
 */

"use strict";

module.exports = function(options, imports, register) {
    register(null, {
        "basic-controllers": {
            movie: require('./movie_controller')(imports),
            people: require('./person_controller')(imports),
            credit: require('./credit_controller')(imports),
            search: require('./search_controller')(imports),
            user: require('./user_controller')(imports),
            auth: require('./authentication_controller')(imports),
            discover: require('./discover_controller')(imports)
        }
    });
};
