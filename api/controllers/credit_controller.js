/**
 * Created by alex_rudyak on 2/4/15.
 */

var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var dataProviders = imports.dataProviders;
    var responsePresenters = imports.responsePresenters;
    var paramsCheck = imports.requestParametersCheckerMiddleware.check;

    return {
        getById: function (req, res) {
            var userId = req.user.id;
            var creditId = req.params.id;

            dataProviders.CreditProvider.getById(creditId)
                .then(function (credit) {
                    return dataProviders.CreditProvider.getCreditsPopulatedForUser([credit], userId);
                })
                .then(_.first)
                .then(responsePresenters.creditPresenter.toShortPreview)
                .then(function (credit) {
                    res.status(200).json(credit);
                })
                .fail(function (err) {
                    res.status(err.status).json(err);
                }).done();
        },

        postLike: [
            _.curry(paramsCheck)(['like']),

            function (req, res) {
                var userId = req.user.id;
                var creditId = req.params.id;
                var like = req.param('like', false);

                dataProviders.UserRelationsProvider.updateRelationsToCredit(userId, creditId, {
                    'like': like
                })
                    .then(responsePresenters.userToCreditPresenter.toShortPreview)
                    .then(function (relation) {
                        res.status(200).json(relation);
                    })
                    .fail(function (err) {
                        res.status(err.status).json(err);
                    }).done();
            }
        ]
    };
};