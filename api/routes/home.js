module.exports = function(imports) {
  var router = require('express').Router();

  router.get('/', function(req, res) {
    res.status(200).json({
      message:"Welcome to the home page of W2W service!"
    });
  });

  return router;
};
