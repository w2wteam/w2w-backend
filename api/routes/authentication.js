/**
 * Created by alesanro on 11/15/14.
 */

module.exports = function(imports) {
    var passport = imports.passport;

    var router = require('express').Router();
    var authController = imports['basic-controllers'].auth;

    var clientPasswordAuth = passport.authenticate(['oauth2-client-password'], { session: false });
    var bearerAuth = passport.authenticate(['bearer'], { session: false });

    router.post('/register', authController.postRegisterNewUser);
    router.post('/token', clientPasswordAuth, authController.postToken);
    router.post('/refreshtoken', bearerAuth, authController.postRefreshToken);
    router.post('/revoketoken', bearerAuth, authController.postRevokeToken);

    return router;
};
