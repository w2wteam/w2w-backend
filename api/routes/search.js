/**
 * Created by alesanro on 11/2/14.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var searchController = imports['basic-controllers'].search;

    router.get('/movie', searchController.movie);
    router.get('/person', searchController.person);

    return router;
};