/**
 * Created by alesanro on 11/2/14.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var movieController = imports['basic-controllers'].movie;

    router.get('/:id', movieController.getById);
    router.get('/:id/details', movieController.getDetails);
    router.post('/:id/like', movieController.postLike);
    router.post('/:id/want', movieController.postWantToWatch);
    router.post('/:id/notoffer', movieController.postNotToOffer);
    router.post('/:id/rate', movieController.postRate);

    return router;
};


