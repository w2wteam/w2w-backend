/**
 * Created by alesanro on 11/2/14.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var userController = imports['basic-controllers'].user;

    router.get('/wantlist', userController.getWantList);
    router.put('/wantlist', userController.updateWantList);
    router.delete('/wantlist', userController.deleteRecordFromWantList);

    router.get('/watchedlist', userController.getWatchedList);

    return router;
};
