/**
 * Created by alex_rudyak on 1/29/15.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var peopleController = imports['basic-controllers'].people;

    router.get('/:id', peopleController.getById);
    router.get('/:id/details', peopleController.getDetails);
    router.post('/:id/favorite', peopleController.postFavorite);

    return router;
};
