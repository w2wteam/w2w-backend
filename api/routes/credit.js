/**
 * Created by alex_rudyak on 2/4/15.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var creditController = imports['basic-controllers'].credit;

    router.get('/:id', creditController.getById);
    router.post('/:id/like', creditController.postLike);

    return router;
};
