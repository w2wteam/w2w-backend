/**
 * Created by alesanro on 1/25/15.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var discoverController = imports['basic-controllers'].discover;

    router.get('/movie', discoverController.getMovies);

    return router;
};
