/**
 * Created by alesanro on 11/10/14.
 */

module.exports = function(options, imports, register) {
    var passport = imports.passport;
    var bearerAuth = passport.authenticate('bearer', { session: false });

    var router = require('express').Router();
    router.use('/', require('./home')(imports));
    router.use('/search', bearerAuth, require('./search')(imports));
    router.use('/movie', bearerAuth, require('./movie')(imports));
    router.use('/person', bearerAuth, require('./person')(imports));
    router.use('/user', bearerAuth, require('./user')(imports));
    router.use('/oauth', require('./authentication')(imports));
    router.use('/discover', bearerAuth, require('./discover')(imports));
    router.use('/genre', bearerAuth, require('./genre')(imports));
    router.use('/credit', bearerAuth, require('./credit')(imports));

    register(null, {
        "router-default": router
    });
};

