/**
 * Created by alesanro on 1/25/15.
 */

module.exports = function(imports) {
    var router = require('express').Router();
    var movieController = imports['basic-controllers'].movie;

    router.get('/list', movieController.getGenres);

    return router;
};
