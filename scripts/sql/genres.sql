/*
-- Query: SELECT * FROM w2wtestdb.Genre
LIMIT 0, 1000

-- Date: 2015-01-26 00:43
*/
INSERT INTO `Genre` (`id`,`label`) VALUES (1,'Action');
INSERT INTO `Genre` (`id`,`label`) VALUES (3,'Adventure');
INSERT INTO `Genre` (`id`,`label`) VALUES (6,'Animation');
INSERT INTO `Genre` (`id`,`label`) VALUES (4,'Comedy');
INSERT INTO `Genre` (`id`,`label`) VALUES (5,'Crime');
INSERT INTO `Genre` (`id`,`label`) VALUES (7,'Documentary');
INSERT INTO `Genre` (`id`,`label`) VALUES (8,'Drama');
INSERT INTO `Genre` (`id`,`label`) VALUES (9,'Family');
INSERT INTO `Genre` (`id`,`label`) VALUES (10,'Fantasy');
INSERT INTO `Genre` (`id`,`label`) VALUES (11,'Foreign');
INSERT INTO `Genre` (`id`,`label`) VALUES (2,'History');
INSERT INTO `Genre` (`id`,`label`) VALUES (12,'Horror');
INSERT INTO `Genre` (`id`,`label`) VALUES (13,'Music');
INSERT INTO `Genre` (`id`,`label`) VALUES (14,'Mystery');
INSERT INTO `Genre` (`id`,`label`) VALUES (15,'Romance');
INSERT INTO `Genre` (`id`,`label`) VALUES (16,'Science Fiction');
INSERT INTO `Genre` (`id`,`label`) VALUES (18,'Thriller');
INSERT INTO `Genre` (`id`,`label`) VALUES (17,'TV Movie');
INSERT INTO `Genre` (`id`,`label`) VALUES (19,'War');
INSERT INTO `Genre` (`id`,`label`) VALUES (20,'Western');
