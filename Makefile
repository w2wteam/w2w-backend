ISTANBUL = @NODE_ENV=test ./node_modules/.bin/istanbul
TEST_COMMAND = ./node_modules/.bin/mocha
COVER_TEST_COMMAND = ./node_modules/.bin/_mocha
CODECOV_IO_COMMAND = ./node_modules/codecov.io/bin/codecov.io.js

REPORTER = spec

SUPPORT_DIR = test/support
CONFIG_DIR = $(SUPPORT_DIR)/config

REQUIRE_MODULES = --require $(CONFIG_DIR)/init
TEST_FILES = test/*.unit.js test/api/*.unit.js
TEST_CONFIG = --check-leaks $(REQUIRE_MODULES) $(TEST_FILES)

test:
	@NODE_ENV=test $(TEST_COMMAND) --reporter $(REPORTER) $(TEST_CONFIG)

test-cov:
	$(ISTANBUL) cover $(COVER_TEST_COMMAND) -- -R $(REPORTER) $(REQUIRE_MODULES) $(TEST_FILES)

test-shippable:
	multi='xunit=$(XUNIT_FILE)' NODE_ENV=test $(TEST_COMMAND) --reporter mocha-multi $(TEST_CONFIG)

cov-shippable:
	$(ISTANBUL) cover $(COVER_TEST_COMMAND) --report lcovonly -- -R $(REPORTER) $(REQUIRE_MODULES) $(TEST_MODULES)
	$(ISTANBUL) report cobertura --dir $(CI_CODECOVERAGE_DIR)
	@cat coverage/lcov.info | $(CODECOV_IO_COMMAND)

default:
	@echo 'Choose one of command: `test`, `test-cov`'

.PHONY: default