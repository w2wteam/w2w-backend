/**
 * Created by alesanro on 11/16/14.
 */

var _ = require('lodash');
var AuthorizationError = require('./errors/authorizationerror');

module.exports = function jwtBearer(options, issue) {
    if (typeof options == 'function') {
        issue = options;
        options = null;
    }
    options = options || {};

    if (!issue) throw new Error('OAuth 2.0 simple-token exchange middleware requires an issue function.');

    var userProperty = options.userProperty || 'user';

    return function jwt_bearer(req, res, next) {
        if (!req.body) {
            return next(new Error('Request body not parsed. Use bodyParser middleware.'));
        }

        // The 'user' property of `req` holds the authenticated user.  In the case
        // of the token endpoint, the property will contain the OAuth 2.0 client.
        var client = req[userProperty];

        function issued(err, accessToken, refreshToken, expired, params) {
            if (err) { return next(err); }
            if (!accessToken) { return next(new AuthorizationError('invalid JWT', 'invalid_grant')); }

            var tok = {};
            tok['access_token'] = accessToken;
            tok['refresh_token'] = refreshToken;
            tok['expired'] = expired;
            if (params) { _.assign(tok, params); }
            tok['token_type'] = tok['token_type'] || 'bearer';

            var json = JSON.stringify(tok);
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Cache-Control', 'no-store');
            res.setHeader('Pragma', 'no-cache');
            res.end(json);
        }

        issue(client, issued);
    }
};