/**
 * Created by alesanro on 11/16/14.
 */

module.exports = function(options, imports, register) {
    register(null, {
        "simple-token": {
            Exchange: require('./exchange')
        }
    });
};

