/**
 * Created by alesanro on 11/16/14.
 */

var config = require('config');
var _ = require('lodash');
var Q = require('q');


module.exports = function(options, imports, register) {
    var oauth2orize = require('oauth2orize');
    var simpleTokenExchange = imports['simple-token'].Exchange;
    var authProvider = imports.dataProviders.UserAuthenticationProvider;

    var server = oauth2orize.createServer();

    server.exchange('simple-token', simpleTokenExchange(function (client, done) {
        authProvider.generateBearerToken(client)
            .then(function(token) {
                done(null, token.accessToken, token.refreshToken, token.expiredIn);
            })
            .fail(done)
            .done();
    }));

    server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {
        authProvider.refreshBearerToken(client, refreshToken)
            .then(function(token) {
                done(null, token.accessToken, token.refreshToken, { 'expired': token.expiredIn });
            })
            .fail(done)
            .done();
    }));

    register(null, {
        "oauth2schemes": {
            token: [server.token()],
            refreshToken: [server.token()]
        }
    });
};

