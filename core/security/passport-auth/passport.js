/**
 * Created by alesanro on 11/13/14.
 */

"use strict";

var _ = require('lodash');
var Q = require('q');



module.exports = function(options, imports, register) {
    var passport = require('passport');
    var BearerStrategy = require('passport-http-bearer');
    var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;

    var providers = imports.dataProviders;

    passport.use(new ClientPasswordStrategy(function(clientId, clientSecret, done) {
        providers.UserAuthenticationProvider.authenticateWithPasswordStrategy(clientId, clientSecret)
            .then(function(user) {
                done(null, user.toInternalObject());
            })
            .fail(function(err) {
                done(err);
            })
            .done();
    }));

    passport.use(new BearerStrategy(function(token, done) {
        providers.UserAuthenticationProvider.authenticateWithBearerTokenStrategy(token)
            .then(function(user) {
                done(null, user.toInternalObject());
            })
            .fail(function(err) {
                done(err);
            })
            .done();
    }));

    register(null, {
        passport: passport
    });
};