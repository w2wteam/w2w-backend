var Q = require('q');

module.exports = function(options, imports, register) {

    function emptyResolve() {
        return Q.Promise(function(resolve) {
            resolve(null);
        });
    }

    function empty() {}

    register(null, {
        "cache": {
            get: emptyResolve,
            set: empty
        }
    });
};
