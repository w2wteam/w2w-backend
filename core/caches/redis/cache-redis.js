/**
 * Created by alesanro on 1/31/15.
 */

"use strict";

var config = require('config');
var redis = require('redis');
var Q = require('q');

module.exports = function(options, imports, register) {
    var log = imports.logger;

    var client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
    redis.debug_mode = true;

    client
        .on('error', function(error) {
            log.error(error);
        })
        .on('ready', function(arg) {
            log.info('Redis is ready to use on ' + process.env.REDIS_HOST + ':' +  process.env.REDIS_PORT);
        })
        .on('end', function() {
            log.info('Redis connection was closed');
        });

    register(null, {
        "cache": {
            get: function (key) {
                return Q.Promise(function (resolve, reject) {
                    client.get(key, function (err, reply) {
                        if (err) {
                            return reject(err);
                        } else {
                            resolve(JSON.parse(reply));
                        }
                    });
                });
            },

            set: function (key, value) {
                client.setex(key, config.get('Backend.cache.expire'), JSON.stringify(value), client.print);
            }
        }
    });
};



