/**
 * Created by alesanro on 11/15/14.
 */

var _ = require('lodash');
var WErrorReason = require('./error_reasons').WErrorReason;

var ErrorCodes = {
    // authentication codes
    WOAuth: 'OAuthError',

    // database access error
    WDb: 'DatabaseError',

    // registration keys
    WRegisterPassword: 'NotValidPassword',
    WRegisterUsername: 'NotValidUsername',
    WRegisterEmail: 'NotValidEmail',

    // external service codes
    WTmdb: 'TheMovieDBError',

    // common codes
    WNotFound: 'ServiceNotFoundError',
    WUnknown: 'ServiceUnknownError',
    WInternal: 'ServiceInternalError',
    WValidation: 'ServiceValidationError',
    WForbidden: 'ServiceAccessForbidden',
    WInvalidRequest: 'ClientInvalidRequestError'
};

var _defaultErrorType = ErrorCodes.WUnknown;

/*
Basic class for W2W project errors
 */
function W2WError(message, type, reasons) {
    Error.call(this);

    this.message = arguments[0] || '{No error message have been provided.}';
    this.status = 400;
    this.type = type || _defaultErrorType;
    this.reasons = [];

    this.addReasons(reasons || []);
}

W2WError.prototype = Object.create(Error.prototype);
W2WError.prototype.constructor = W2WError;
W2WError.prototype.name = 'W2WError';
W2WError.prototype.toString = function() {
    return '[' + this.name + ' error with \'' + this.message + '\' with \'' + this.type + '\' type]';
};
W2WError.prototype.addReasons = function (reasons) {
    var mappedReasons = WErrorReason.parseReasons(reasons);
    Array.prototype.push.apply(this.reasons, mappedReasons);
};

/*
ORM errors class
 */
function W2WDbError() {
    W2WError.apply(this, arguments);

    this.type = this.type === _defaultErrorType ? ErrorCodes.WDb : this.type;
}

W2WDbError.prototype = Object.create(W2WError.prototype);
W2WDbError.prototype.constructor = W2WDbError;
W2WDbError.prototype.name = 'W2WDbError';


/*
TheMovieDB errors class
 */
function W2WTmdbError() {
    W2WError.apply(this, arguments);

    this.type = this.type === _defaultErrorType ? ErrorCodes.WTmdb : this.type;
}

W2WTmdbError.prototype = Object.create(W2WError.prototype);
W2WTmdbError.prototype.constructor = W2WTmdbError;
W2WTmdbError.prototype.name = 'W2WTmdbError';



module.exports.ErrorCodes = ErrorCodes;
module.exports.W2WError = W2WError;
module.exports.W2WDbError = W2WDbError;
module.exports.W2WTmdbError = W2WTmdbError;