/**
 * Created by alesanro on 1/26/15.
 */

module.exports = function(options, imports, register) {
    register(null, {
        wErrorScope: {
            W2WErrorCodes: require('./error').ErrorCodes,
            W2WReasonKeys: require('./error_reasons').WReasonsKeys,
            W2WErrorParser: require('./error_parsing')(imports),
            W2WErrorReason: require('./error_reasons').WErrorReason,
            classes: {
                W2WError: require('./error').W2WError,
                W2WDbError: require('./error').W2WDbError,
                W2WTmdbError: require('./error').W2WTmdbError
            }
        }
    });
};
