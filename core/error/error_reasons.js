/**
 * Created by alesanro on 1/26/15.
 */

var _ = require('lodash');

var keys = {
    // The Movie DB
    WTmdbInternal: 'TmdbInternal',

    // User registration
    WPasswordStrength: 'PasswordStrength',
    WEmailExists: 'EmailExists',
    WEmailBadStructure: 'EmailStructure',
    WNicknameStrength: 'NicknameStrength',
    WNicknameExists: 'NicknameExists',
    WUserNotFound: 'UserNotFound',
    WInvalidUserOrPassword: 'WInvalidUserOrPassword',
    WInvalidRefreshToken: 'WInvalidRefreshToken',
    WInvalidAccessToken: 'WInvalidAccessToken',
    WAccessTokenExpired: 'WAccessTokenExpired',
    WRequiredParametersMissed: 'WRequiredParametersMissed',

    // User
    WResourceNotFound: 'ResourceNotFound',

    // Service
    WServiceUnknownError: 'ServiceUnknownError',
    WServiceDbConnectionLost: 'ServiceDbConnectionLost',
    WServiceNotSupport: 'ServiceNotSupport',
    WServiceInternalError: 'ServiceInternalError',
    WServiceBadParameters: 'ServiceBadParameters'
};

var values = {};
values[keys.WTmdbInternal] = "External service error (The MovieDB) was encountered";
values[keys.WPasswordStrength] = "Passwords should be with at least 1 letter, at least 1 non-letter, and at least 6 characters in length";
values[keys.WEmailExists] = 'It seems user with the same email is already exist. Try another email address';
values[keys.WEmailBadStructure] = 'Email address bad-formed. Enter a valid one';
values[keys.WNicknameStrength] = 'Username must be of length ranging(3-30). Starting letter should be a number or a character. Underscore allowed';
values[keys.WNicknameExists] = 'It seems user with the same nickname is already exist. Try another alias';
values[keys.WUserNotFound] = 'User with such credentials was not found';
values[keys.WInvalidUserOrPassword] = 'You\'ve entered invalid username of password';
values[keys.WInvalidRefreshToken] = 'Invalid refreshToken to refresh expired access token';
values[keys.WInvalidAccessToken] = 'Invalid access token to get access';
values[keys.WAccessTokenExpired] = 'Valid date of using access token is expired';
values[keys.WServiceUnknownError] = 'Unknown error encountered on server';
values[keys.WServiceDbConnectionLost] = 'Server has lost connection with database';
values[keys.WServiceNotSupport] = 'Service does not supported this functionality';
values[keys.WServiceInternalError] = 'Something occurred in service internals';
values[keys.WServiceBadParameters] = 'Service has received bad parameters';
values[keys.WResourceNotFound] = 'Not found';
values[keys.WRequiredParametersMissed] = 'Required parameter `[parameterName]` missed';


var _priority = {};
_priority[keys.WTmdbInternal] = 3;
_priority[keys.WPasswordStrength] = 18;
_priority[keys.WEmailExists] = 13;
_priority[keys.WEmailBadStructure] = 14;
_priority[keys.WNicknameStrength] = 18;
_priority[keys.WNicknameExists] = 13;
_priority[keys.WUserNotFound] = 12;
_priority[keys.WInvalidUserOrPassword] = 12;
_priority[keys.WInvalidRefreshToken] = 12;
_priority[keys.WInvalidAccessToken] = 12;
_priority[keys.WAccessTokenExpired] = 12;
_priority[keys.WRequiredParametersMissed] = 14;
_priority[keys.WServiceUnknownError] = 1;
_priority[keys.WServiceDbConnectionLost] = 1;
_priority[keys.WServiceNotSupport] =  5;
_priority[keys.WServiceInternalError] = 1;
_priority[keys.WServiceBadParameters] = 5;
_priority[keys.WResourceNotFound] = 5;

function priorityForKey(key) {
    var priority = _priority[key];
    if (!priority) {
        priority = 0;
    }
    return priority;
}

function W2WErrorReason(key, description) {
    this.title = key || keys.WServiceInternalError;

    var desc;
    if (_.isString(description)) {
        desc = description;
    } else if (_.isPlainObject(description) && key) {
        desc = replaceStringParameters(values[key], description);
    } else {
        desc = key ? values[key] : 'No description provided';
    }

    this.text = desc;
    this.priority = priorityForKey(key);
}

W2WErrorReason.prototype.name = 'W2WErrorReason';
W2WErrorReason.parseReasons = function(reasons) {
    var reasonsObjCount = -1;
    if (_.isArray(reasons)) {
        reasonsObjCount = _.filter(reasons, function(item) {
            return item.name === W2WErrorReason.prototype.name;
        }).length;
    }

    if (reasonsObjCount === _.size(reasons)) {
        return reasons;
    } else if (reasons && reasons.name === W2WErrorReason.prototype.name) {
        return [reasons];
    } else {
        var mappedReasons = _.clone(reasons);
        if (_.isPlainObject(reasons)) {
            mappedReasons = _.map(mappedReasons, function(reasonValue, reasonKey) {
                return new W2WErrorReason(reasonKey, reasonValue);
            });

            return mappedReasons;
        }

        throw new Error("Unable to parse parameters to W2WErrorReason object");
    }
};

function replaceStringParameters(source, opts) {
    return _.reduce(opts, function(result, value, key) {
        var regexp = new RegExp(require('util').format('\\[%s\\]', key), 'gi');
        return result.replace(regexp, value);
    }, source);
}

module.exports.WReasonsKeys = keys;
module.exports.WErrorReason = W2WErrorReason;