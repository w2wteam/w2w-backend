/**
 * Created by alesanro on 3/19/15.
 */

module.exports = function(options, imports, register) {
    var parametersChecker = require('./parameters-checker')(imports, options);

    register(null, {
        requestParametersCheckerMiddleware: parametersChecker
    });
};