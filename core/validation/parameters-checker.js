/**
 * Created by alesanro on 3/19/15.
 */

var _ = require('lodash');

module.exports = function(imports, opts) {
    var errorScope = imports.wErrorScope;

    return {
        check: function _checkParameters(paramKeys, req, res, next) {
            req.performParamsCheck(function() {
                var reasons = checkParameters(_.bind(req.param, req), paramKeys);
                if (!_.isEmpty(reasons)) {
                    var  error = new errorScope.classes.W2WError('One or more parameters missed', errorScope.W2WErrorCodes.WInvalidRequest, reasons);
                    return res.status(400).json(error);
                } else {
                    next();
                }
            }, next);
        },

        query: {
            check: function _checkQueryParameters(keys, req, res, next) {
                req.performParamsCheck(function() {
                    var reasons = checkQueryParameters(req.query, keys);
                    if (!_.isEmpty(reasons)) {
                        var  error = new errorScope.classes.W2WError('One or more query parameters missed', errorScope.W2WErrorCodes.WInvalidRequest, reasons);
                        return res.status(400).json(error);
                    } else {
                        next();
                    }
                }, next);
            }
        }
    };

    function checkParameter(holder, parameterName) {
        if (_.isUndefined(holder(parameterName))) {
            return new errorScope.W2WErrorReason(errorScope.W2WReasonKeys.WRequiredParametersMissed, { parameterName: parameterName});
        }
        return undefined;
    }

    function checkParameters(holder, parametersNames) {
        return _.reduce(parametersNames, function(reasons, value) {
            var result = checkParameter(holder, value);
            if (result) {
                reasons.push(result);
            }

            return reasons;
        }, []);
    }

    function checkQueryParameters(holder, parametersNames) {
        return _.reduce(parametersNames, function(reasons, value) {
            var result = checkParameter(function(p) { return holder[p]; }, value);
            if (result) {
                reasons.push(result);
            }

            return reasons;
        }, []);
    }
};