/**
 * Created by alex_rudyak on 2/25/15.
 */

module.exports = function(options, imports, register) {
    var config = require('config');
    var logger = require('log4js').getLogger(config.get('logger.default_category'));

    register(null, {
        logger: logger
    });
};