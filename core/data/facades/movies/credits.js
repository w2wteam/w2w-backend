/**
 * Created by alex_rudyak on 2/26/15.
 */

var Q = require('q');
var _ = require('lodash');

module.exports = function(imports) {
    "use strict";

    var db = imports.database.db;
    var movieModels = imports.movieModels;
    var movieServiceApi = imports.movieServiceAPI;
    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var collectionFinder = new imports.externalLinksFinder.ExternalLinksCollectionFinder(movieServiceApi.serviceIds());
    var databaseQueryMaker = new imports.databaseQueryMakers.DatabaseQueryMaker(movieServiceApi.serviceIds());

    return {
        /**
         * Fetch credits for some movie. It will be actors, director, music assistants and so forth.
         *
         * @param {Movie} movie movie object from DB
         *
         * @return {Q.Promise} promise with credits
         */
        getCreditsForMovie: function(movie) {
            if (!movie) {
                return Q.reject(WErrorParser.tryParseError(new Error("Can't get credits for undefined movie")));
            }

            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.Credit.find, { movie_id: movie.id })
                    .then(function(internalCredits) {
                        if (movie.shouldUpdateCredits()) {
                            var requestParameters = movieServiceApi.RequestParameterTranslator.movieRequestParameterTranslator.translate({
                                params: movie
                            });
                            return movieServiceApi.movie.getCredits(requestParameters)
                                .then(function(externalCredits) {
                                    return updateAndSaveCreditsForMovie(internalCredits, externalCredits, movie.id);
                                })
                                .then(resolve);
                        } else {
                            resolve(internalCredits);
                        }
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch credits for some person. It will be roles as an actor (actress), directors and so forth.
         *
         * @param {Person} person person object from DB
         *
         * @return {Q.Promise} promise with credits
         */
        getCreditsForPerson: function(person) {
            if (!person) {
                return Q.reject(WErrorParser.tryParseError(new Error("Can't get credits for undefined person")));
            }

            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.Credit.find, { person_id: person.id })
                    .then(function(internalCredits) {
                        if (person.shouldUpdateCredits()) {
                            var requestParameters = movieServiceApi.RequestParameterTranslator.movieRequestParameterTranslator.translate({
                                params: person
                            });
                            return movieServiceApi.person.getCredits(requestParameters)
                                .then(function(externalCredits) {
                                    return updateAndSaveCreditsForPerson(internalCredits, externalCredits, person.id);
                                })
                                .then(resolve);
                        } else {
                            resolve(internalCredits);
                        }
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        }
    };

    /**
     * Provide updating information about credits from fetched collection of credits for concrete movie;
     * Saves new credits and updated credits into database
     *
     * @param {Array} internalCredits fetched from database (for need of update)
     * @param {Array} externalCredits fetched from external services (for need to create)
     * @param {Number} movieId id of movie that credits fetched for
     *
     * @return {Q.Promise} promise with collection of credits
     */
    function updateAndSaveCreditsForMovie(internalCredits, externalCredits, movieId) {
        var credits = filterCredits(internalCredits, externalCredits);

        return Q.nfcall(db.models.Person.find, databaseQueryMaker.makeFindOr(credits.createCredits, "Person"))
            .then(function(existedPeople) {
                var notExistedPeople = _.filter(credits.createCredits, function(credit) {
                    return !collectionFinder.findWithModel(existedPeople, credit, "Person");
                });

                return createThumbsForPeople(notExistedPeople)
                    .then(function(thumbPeople) {
                        if (_.size(thumbPeople) === 0) {
                            thumbPeople = existedPeople;
                        } else if (_.size(existedPeople) !== 0) {
                            Array.prototype.push.apply(thumbPeople, existedPeople);
                        }

                        _.forEach(credits.createCredits, function(credit) {
                            credit.movie_id = movieId;
                            credit.person_id = _.result(collectionFinder.findWithModel(thumbPeople, credit, "Person"), 'id');
                        });

                        return Q(credits);
                    });
            })
            .then(updateAndSaveCredits);
    }

    /**
     * Provide updating information about credits from fetched collection of credits for concrete person;
     * Saves new credits and updated credits into database.
     *
     * @param {Array} internalCredits credits fetched from database (for need of update)
     * @param {Array} externalCredits credits fetched from external services (for need to create)
     * @param {Number} personId id of person that credits fetched for
     *
     * @return {Q.Promise} promise with collection of credits
     */
    function updateAndSaveCreditsForPerson(internalCredits, externalCredits, personId) {
        var credits = filterCredits(internalCredits, externalCredits);

        return Q.nfcall(db.models.Movie.find, databaseQueryMaker.makeFindOr(credits.createCredits, "Movie"))
            .then(function(existedMovies) {
                var notExistedMoviesIds = _.filter(credits.createCredits, function(credit) {
                    return !collectionFinder.findWithModel(existedMovies, credit, "Movie");
                });

                return createThumbsForMovies(notExistedMoviesIds)
                    .then(function(thumbMovies) {
                        if(_.size(thumbMovies) === 0) {
                            thumbMovies = existedMovies;
                        } else if (_.size(existedMovies) != 0) {
                            Array.prototype.push.apply(thumbMovies, existedMovies);
                        }

                        _.forEach(credits.createCredits, function(credit) {
                            credit.person_id = personId;
                            credit.movie_id = _.result(collectionFinder.findWithModel(thumbMovies, credit, "Movie"), 'id');
                        });

                        return Q(credits);
                    });
            })
            .then(updateAndSaveCredits);
    }

    /**
     * Perform filtering and grouping of credits into two groups: update and create.
     * Update group contains credits which exist in database;
     * Create group contains credits which absent in database and needed to save to it.
     *
     * @param {Array} internalCredits credits fetched from database
     * @param {Array} externalCredits credits fetched from external services
     *
     * @return {Object} create and update credits
     */
    function filterCredits(internalCredits, externalCredits) {
        var updateCredits = _.map(internalCredits, function(credit) {
            var foundCredit = collectionFinder.find(externalCredits, credit);
            return credit.updateWithCredit(foundCredit);
        });
        var createCredits = _.filter(externalCredits, function(credit) {
            return !collectionFinder.find(updateCredits, credit);
        });

        return {
            updateCredits: updateCredits,
            createCredits: createCredits
        };
    }

    /**
     * Save credits to database (creating or updating)
     *
     * @param {Object} credits object with created and updated credits
     *
     * @return {Q.Promise} promise with persisted credits
     */
    function updateAndSaveCredits(credits) {
        var persistCredits = [];
        if (_.size(credits.updateCredits) > 0) {
            _.forEach(credits.updateCredits, function(credit) {
                persistCredits.push(Q.nfcall(credit.save));
            });
        }
        if (_.size(credits.createCredits) > 0) {
            persistCredits.push(Q.nfcall(db.models.Credit.create, credits.createCredits));
        }

        return Q.all(persistCredits)
            .then(function(persistedCredits) {
                var resultCredits = [];
                _.forEach(persistedCredits, function(credits) {
                    if (_.isArray(credits)) {
                        Array.prototype.push.apply(resultCredits, credits);
                    } else {
                        resultCredits.push(credits);
                    }
                });
                return Q(resultCredits);
            });
    }

    /**
     * Create thumb objects for movies which are not existed in database
     *
     * @param {Array} movieCredits credits from person cast
     *
     * @return {Q.Promise} promise with created movies (thumbs)
     */
    function createThumbsForMovies(moviesCredits) {
        var movies = collectionFinder.unique(_.map(moviesCredits, function(credit) {
            var externalLinks = movieModels.Credit.externalLinksWithModel(credit, "Movie");
            return _.assign({
                title: credit.title,
                originalTitle: credit.originalTitle,
                posterPath: credit.posterPath,
                _thumb: true
            }, externalLinks);
        }));

        return Q.nfcall(db.models.Movie.create, movies);
    }

    /**
     * Create thumb objects for persons which are not existed in database
     *
     * @param {Array} peopleCredits credits from movie cast
     *
     * @return {Q.Promise} promise with created people (thumbs)
     */
    function createThumbsForPeople(peopleCredits) {
        var people = collectionFinder.unique(_.map(peopleCredits, function(credit) {
            var externalLinks = movieModels.Credit.externalLinksWithModel(credit, "Person");
            return _.assign({
                name: credit.name,
                profilePath: credit.profilePath,
                _thumb: true
            }, externalLinks);
        }));
        return Q.nfcall(db.models.Person.create, people);
    }
};