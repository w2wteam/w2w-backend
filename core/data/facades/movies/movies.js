/**
 * Created by alex_rudyak on 2/26/15.
 */

var Q =  require('q');
var _ = require('lodash');

module.exports = function(imports) {
    "use strict";
    var db = imports.database.db;
    var movieServiceApi = imports.movieServiceAPI;
    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var log = imports.logger;
    var collectionFinder = new imports.externalLinksFinder.ExternalLinksCollectionFinder(movieServiceApi.serviceIds());
    var databaseQueryMaker = new imports.databaseQueryMakers.DatabaseQueryMaker(movieServiceApi.serviceIds());

    return {
        /**
         * Fetch of movie by ID (only internal id).
         *
         * @param {Number} movieId id of movie in DB
         *
         * @return {Q.Promise} promise with Movie object
         */
        getById: function (movieId) {
            return Q.Promise(function (resolve, reject) {
                Q.nfcall(db.models.Movie.get, movieId)
                    .then(function (movie) {
                        if (!movie._thumb) {
                            return Q(movie);
                        } else {
                            var requestParameters = movieServiceApi.RequestParameterTranslator.movieRequestParameterTranslator.translate({
                                params: movie
                            });
                            return movieServiceApi.movie.getById(requestParameters)
                                .then(function (externalMovie) {
                                    movie.updateWithMovie(externalMovie);
                                    movie._thumb = false;

                                    return Q.nfcall(db.models.Genre.find, {
                                        genreLabel: movie.genres
                                    }).then(function (genres) {
                                        movie.genres = genres;
                                        return Q.nfcall(movie.save);
                                    });
                                });
                        }
                    })
                    .then(resolve)
                    .fail(function (err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch possible genres for movies (only names)
         *
         * @return {Q.Promise} promise with collection of genres names
         */
        getGenresLabels: function() {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.Genre.find)
                    .then(function(genres) {
                        if (_.size(genres) > 0) {
                            resolve(_.map(genres, function(genre) {
                                return genre.genreLabel;
                            }));
                        } else {
                            return movieServiceApi.genre.getGenres()
                                .then(function(genres) {
                                    var objGenres = _.map(genres, function(name) {
                                        return {
                                            genreLabel: name
                                        };
                                    });

                                    Q.nfcall(db.models.Genre.create, objGenres)
                                        .then(function(dbGenres) {
                                            return Q(_.map(dbGenres, function(dbGenre) {
                                                return dbGenre.genreLabel;
                                            }));
                                        })
                                        .then(resolve)
                                        .fail(log.warn);
                                });
                        }
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Perform searching movies by some query syntax.
         *
         * @param {String} query search query like `q=matrix`
         *
         * @return {Q.Promise} promise with collection of movies
         */
        searchMovieWithQuery: function(query) {
            return Q.Promise(function(resolve, reject) {
                var requestParameters = movieServiceApi.RequestParameterTranslator.searchRequestParameterTranslator.translate({
                    query: query
                });
                movieServiceApi.search.getSearchMovie(requestParameters)
                    .then(function(results) {
                        var rawMovies = results;
                        _.forEach(rawMovies, function(movie) {
                            movie._thumb = true;
                        });

                        return Q.nfcall(db.models.Movie.find, databaseQueryMaker.makeFindOr(rawMovies))
                            .then(function(existedMovies) {
                                var notExistedMovies = _.filter(rawMovies, function(movie) {
                                    return !collectionFinder.find(existedMovies, movie);
                                });

                                return Q.nfcall(db.models.Movie.create, notExistedMovies)
                                    .then(function(createdMovies) {
                                        var allMovies = [];
                                        Array.prototype.push.apply(allMovies, existedMovies);
                                        Array.prototype.push.apply(allMovies, createdMovies);

                                        return Q(allMovies);
                                    })
                                    .then(resolve);
                            });
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        }
    };
};