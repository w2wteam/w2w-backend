/**
 * Created by alex_rudyak on 2/26/15.
 */

module.exports = function(options, imports, register) {
    var self = {};
    imports.facade = self;

    self.Movies = require('./movies')(imports);
    self.Credits = require('./credits')(imports);
    self.People = require('./people')(imports);

    register(null, {
        moviesFacade: self
    });
};