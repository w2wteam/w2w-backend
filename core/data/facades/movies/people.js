/**
 * Created by alex_rudyak on 2/26/15.
 */

var Q = require('q');
var _ = require('lodash');

module.exports = function(imports) {
    "use strict";

    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var db = imports.database.db;
    var movieServiceApi = imports.movieServiceAPI;
    var collectionFinder = new imports.externalLinksFinder.ExternalLinksCollectionFinder(movieServiceApi.serviceIds());
    var databaseQueryMaker = new imports.databaseQueryMakers.DatabaseQueryMaker(movieServiceApi.serviceIds());

    return {
        /**
         * Fetch person by ID (only internal id)
         *
         * @param {Number} personId id of person
         *
         * @return {Q.Promise} promise with Person object
         */
        getById: function(personId) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.Person.get, personId)
                    .then(function(person) {
                        if (!person._thumb) {
                            return Q(person);
                        } else {
                            var requestParameters = movieServiceApi.RequestParameterTranslator.personRequestParameterTranslator.translate({
                                params: person
                            });
                            return movieServiceApi.person.getById(requestParameters)
                                .then(function(externalPerson) {
                                    person.updateWithPerson(externalPerson);
                                    person._thumb = false;

                                    return Q.nfcall(person.save);
                                });
                        }
                    })
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Perform searching people by some query syntax.
         *
         * @param {Object} query search query like `q=brad%20pitt`
         *
         * @return {Q.Promise} promise with collection of people
         */
        searchPersonWithQuery: function(query) {
            return Q.Promise(function(resolve, reject) {
                var requestParameters = movieServiceApi.RequestParameterTranslator.searchRequestParameterTranslator.translate({
                    query: query
                });
                movieServiceApi.search.getSearchPerson(requestParameters)
                    .then(function(results) {
                        var rawPeople = results;
                        _.forEach(rawPeople, function(person) {
                            person._thumb = true;
                        });

                        return Q.nfcall(db.models.Person.find, databaseQueryMaker.makeFindAnd(rawPeople))
                            .then(function(existedPeople) {
                                var notExistedPeople = _.filter(rawPeople, function(person) {
                                    return !collectionFinder.find(existedPeople, person);
                                });

                                return Q.nfcall(db.models.Person.create, notExistedPeople)
                                    .then(function(createdPeople) {
                                        var allPeople = [];
                                        Array.prototype.push.apply(allPeople, existedPeople);
                                        Array.prototype.push.apply(allPeople, createdPeople);

                                        return Q(allPeople);
                                    })
                                    .then(resolve);
                            });
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        }
    };
};