/**
 * Created by alex_rudyak on 1/29/15.
 */

var _ = require('lodash');
var Q = require('q');

module.exports = function(imports) {
    "use strict";

    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var providers = imports.dataProviders;
    var db = imports.database.db;
    var moviesFacade = imports.moviesFacade;

    return {
        /**
         * Fetch person by ID (only internal DB)
         *
         * @param {Number} personId id of person
         *
         * @return {Q.Promise} promise with Person model object
         */
        getById: function(personId) {
            return moviesFacade.People.getById(personId);
        },

        /**
         * Populate or add user-specific properties to person object
         *
         * @param {Person} person object of Person model
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated person
         */
        getPersonPopulatedForUser: function(person, userId) {
            if (!person) {
                return Q();
            }

            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.UserRelationsToPerson.find, {
                    user_id: userId,
                    person_id: person.id
                }, { limit: 1 })
                    .then(function(relations) {
                        resolve(db.models.UserRelationsToPerson.markObjectWithPerson(person, _.first(relations)));
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Populate or add user-specific properties to collection of people
         *
         * @param {Array} people collection of Person model objects
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated people objects
         */
        getPeoplePopulatedForUser: function(people, userId) {
            if (_.isEmpty(people)) {
                return Q();
            }

            return Q.Promise(function(resolve, reject) {
                var peopleIds = _.map(people, function(person) {
                    return person.id;
                });

                Q.nfcall(db.models.UserRelationsToPerson.find, {
                    person_id: peopleIds,
                    user_id: userId
                })
                    .then(function(markedPeople) {
                        _.forEach(people, function(person) {
                            var markedPerson = _.find(markedPeople, { person_id: person.id });
                            db.models.UserRelationsToPerson.markObjectWithPerson(person, markedPerson);
                        });

                        resolve(people);
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch detailed object about person with ID (only internal DB)
         *
         * @param {Number} personId
         *
         * @return {Q.Promise} promise with additional properties of person (credits, etc.)
         */
        getPersonDetails: function(personId) {
            return providers.PersonProvider.getById(personId)
                .then(function(person) {
                    return providers.CreditProvider.getCreditsForPerson(person)
                        .then(function(credits) {
                            return Q(person.populateWithCredits(credits));
                        });
                });
        },

        /**
         * Populate or add user-specific properties to person object
         *
         * @param {Person} person object of Person model
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated person object
         */
        getPersonDetailsPopulatedForUser: function(person, userId) {
            if (!person) {
                return Q();
            }

            return providers.PersonProvider.getPersonPopulatedForUser(person, userId)
                .then(function(person) {
                    return providers.CreditProvider.getCreditsPopulatedForUser(person.credits, userId)
                        .then(function(credits) {
                            return Q(person.populateWithCredits(credits));
                        });
                });
        },

        /**
         * Perform search of people with query (like `q=brad%20pitt`)
         *
         * @param {Object} query query string
         *
         * @return {Q.Promise} promise with collection of search results
         */
        getSearchPersonWithQuery: function(query) {
            return moviesFacade.People.searchPersonWithQuery(query);
        },

        /**
         * Populate or add user-specific preferences to search results collection
         *
         * @param {Object|Array} searchResults collection of search results of people
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated collection of search results
         */
        getSearchPeoplePopulatedForUser: function(searchResults, userId) {
            return providers.PersonProvider.getPeoplePopulatedForUser(searchResults, userId);
        }
    };
};