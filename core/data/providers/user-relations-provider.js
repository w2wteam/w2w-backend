/**
 * Created by alesanro on 1/28/15.
 */

var _ = require('lodash');
var Q = require('q');

module.exports = function(imports) {
    "use strict";

    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var WErrorHelper = require('../../../helpers').errors;
    var providers = imports.dataProviders;
    var db = imports.database.db;

    return {
        /**
         * Perform update of user-specific properties for movie
         *
         * @param {Number} userId id of user which preferences will be updated
         * @param {Number} movieId id of movie
         * @param {Object} properties object with updated properties values
         *
         * @return {Q.Promise} promise with updated user's preferences
         */
        updateRelationsToMovie: function(userId, movieId, properties) {
            var requestObject = {
                movie_id: movieId,
                user_id: userId
            };

            return updateRelationsToModel(requestObject, movieId, properties, db.models.UserRelationsToMovie, providers.MovieProvider);
        },

        /**
         * Fetch movies which user used to save its preferences
         *
         * @param {Number} userId id of user
         *
         * @return {Q.Promise} promise with found collection of movies
         */
        getMarkedMovies: function(userId) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.UserAccount.get, userId)
                    .then(function(user) {
                        return Q.nfcall(user.getMarkedMovies);
                    })
                    .then(function(movies) {
                        return _.isEmpty(movies) ? [] : movies;
                    })
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Perform update of user-specific properties for person
         *
         * @param {Number} userId id of user which preferences will be updated
         * @param {Number} personId id of person
         * @param {Object} properties object with updated properties values
         *
         * @return {Q.Promise} promise with updated user's preferences
         */
        updateRelationsToPerson: function(userId, personId, properties) {
            var requestObject = {
                person_id: personId,
                user_id: userId
            };

            return updateRelationsToModel(requestObject, personId, properties, db.models.UserRelationsToPerson, providers.PersonProvider);
        },

        /**
         * Fetch movies which user marked as `want to watch`
         *
         * @param {Number} userId id of target user
         */
        getWantMovies: function(userId) {
            return Q.Promise(function(resolve, reject) {
                providers.UserRelationsProvider.getMarkedMovies(userId)
                    .then(function(movies) {
                        var wantToWatchMovies = _.filter(movies, function(movieRelation) {
                            return movieRelation.wantToWatch === true && movieRelation.movie;
                        }).map(function(relation) {
                            return db.models.UserRelationsToMovie.markObjectWithMovie(relation.movie, relation);
                        });

                        resolve(_.isEmpty(wantToWatchMovies) ? [] : wantToWatchMovies);
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch movies which were watched by user
         *
         * @param {Number} userId id of target user
         */
        getWatchedMovies: function(userId) {
            return Q.Promise(function(resolve, reject) {
                providers.UserRelationsProvider.getMarkedMovies(userId)
                    .then(function(movies) {
                        var watchedMovies = _.filter(movies, function(movieRelation) {
                            return (movieRelation.like !== 'unknown' || movieRelation.rate !== 0 && movieRelation.rate !== null)
                                && movieRelation.movie;
                        }).map(function(relation) {
                            return db.models.UserRelationsToMovie.markObjectWithMovie(relation.movie, relation);
                        });
                        resolve(_.isEmpty(watchedMovies) ? [] : watchedMovies);
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Perform update of user-specific properties for credit
         *
         * @param {Number} userId id of user which preferences will be updated
         * @param {Number} creditId id of credit
         * @param {Object} properties object with updated properties values
         *
         * @return {Q.Promise} promise with updated user's preferences
         */
        updateRelationsToCredit: function(userId, creditId, properties) {
            var requestObject = {
                credit_id: creditId,
                user_id: userId
            };

            return updateRelationsToModel(requestObject, creditId, properties, db.models.UserRelationsToCredit, providers.CreditProvider);
        }
    };

    /**
     * General-target function to operate with user-specific information for different models
     *
     * @param {Object} requestObject object with information used to search needed model objects
     * @param {Number} modelId id of model
     * @param {Object} properties object with properties to update
     * @param {String} model name of model
     * @param {dataProvider} modelProvider one from the set of data providers
     *
     * @return {Q.Promise} promise with updated user preferences
     */
    function updateRelationsToModel(requestObject, modelId, properties, model, modelProvider) {
        return Q.Promise(function(resolve, reject) {
            Q.nfcall(model.find, requestObject, { limit: 1 })
                .then(function(relations) {
                    if (_.size(relations) > 0) {
                        var relation = updateRelationObjectWithProperties(_.first(relations), properties);
                        return Q.nfcall(relation.save);
                    } else {
                        return createRelation(model, modelProvider, requestObject, properties);
                    }
                })
                .then(resolve)
                .fail(function(err) {
                    if (!WErrorHelper.isNotFoundError(err)) {
                        return reject(WErrorParser.tryParseError(err));
                    }

                    createRelation(model, modelProvider, requestObject, properties)
                        .then(resolve)
                        .fail(reject)
                        .done();
                }).done();
        });


        /**
         * Create preference object for user for a specific model
         *
         * @param {String} model name of a model
         * @param {dataProvider} modelProvider one from the set of data providers
         * @param {Object} newObject relationship data between user and model object
         * @param {Object} properties object with properties to update
         *
         * @return {Q.Promise} promise with created preference object
         */
        function createRelation(model, modelProvider, newObject, properties) {
            return Q.Promise(function(resolve, reject) {
                modelProvider.getById(modelId)
                    .then(function() {
                        var relation = updateRelationObjectWithProperties(newObject, properties);
                        return Q.nfcall(model.create, relation);
                    })
                    .then(resolve)
                    .fail(reject)
                    .done();
            });
        }

        /**
         * Update preference object with properties object. Just like simple assigning
         *
         * @param {Object} relation preference object
         * @param {Object} properties object with properties to updated
         *
         * @return {Object} updated preference object
         */
        function updateRelationObjectWithProperties(relation, properties) {
            _.forEach(properties, function(propertyValue, propertyKey) {
                relation[propertyKey] = propertyValue;
                relation[propertyKey + 'UpdatedAt'] = new Date();
            });
            return relation;
        }
    }
};