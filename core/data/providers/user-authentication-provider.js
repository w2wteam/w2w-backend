/**
 * Created by alesanro on 1/28/15.
 */

var _ = require('lodash');
var Q = require('q');
var moment = require('moment');
var bcrypt = require('bcryptjs');
var validator = require('./validators');
var utils = require('../../../helpers/index').utils;

module.exports = function(imports) {
    "use strict";

    var wErrorScope = imports.wErrorScope;
    var WErrorParser = wErrorScope.W2WErrorParser;
    var WReasonKeys = wErrorScope.W2WReasonKeys;
    var WErrorReason = wErrorScope.W2WErrorReason;
    var db = imports.database.db;
    var log = imports.logger;

    return {
        /**
         * Perform user registration function with initial credentials
         *
         * @param {String} nickname nickname (alias) of user
         * @param {String} email e-mail of user (should be unique)
         * @param {String} password password (should conform security rules)
         */
        registerUser: function (nickname, email, password) {
            return Q.Promise(function (resolve, reject) {
                validateEmail(email, null)
                    .then(_.curry(validateNickname)(nickname))
                    .then(_.curry(validatePassword)(password))
                    .then(function (validationError) {
                        if (validationError) {
                            validationError.status = 400;
                            log.warn(validationError.toString());
                            reject(validationError);
                        } else {
                            return Q.nfcall(bcrypt.genSalt)
                                .then(function (salt) {
                                    Q.nfcall(bcrypt.hash, password, salt)
                                        .then(function (hash) {
                                            return _createUser(salt, hash);
                                        }).then(resolve)
                                        .fail(reject).done();
                                });
                        }
                    }).fail(function (err) {
                        err.status = 500;
                        log.error(err.toString());
                        reject(err);
                    }).done();
            });

            function _createUser(salt, passwordHash) {
                var newUserObj = {
                    nickname: nickname,
                    email: email,
                    passwordHash: passwordHash,
                    salt: salt
                };

                return Q.Promise(function (resolve, reject) {
                    Q.nfcall(db.models.UserAccount.create, newUserObj)
                        .then(function (user) {
                            if (!user) {
                                var internalErr = new wErrorScope.classes.W2WDbError('Some internal error was occurred while registering new user.', wErrorScope.W2WErrorCodes.WInternal);
                                internalErr.status = 500;
                                log.error(internalErr);
                                reject(internalErr);
                            } else {
                                resolve(user);
                            }
                        }).fail(function (err) {
                            var internalErr = WErrorParser.tryParseError(err);
                            internalErr.status = 500;
                            log.error(internalErr);
                            reject(internalErr);
                        }).done();
                });
            }
        },

        /**
         * Method for user authentication with password and client unique name
         *
         * @param {String} uniqueName unique field of user (email, nickname)
         * @param {String} password password
         *
         * @return {Q.Promise} promise promise with success or failure of user authentication
         */
        authenticateWithPasswordStrategy: function(uniqueName, password) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.UserAccount.find, {
                    or: [{
                        email: uniqueName
                    }, {
                        nickname: uniqueName
                    }]
                }, {
                    limit: 1
                })
                    .then(function (users) {
                        var user = _.first(users);
                        if (!user) {
                            var reason = new wErrorScope.W2WErrorReason(wErrorScope.W2WReasonKeys.WUserNotFound);
                            var err = new wErrorScope.classes.W2WError('Wrong user or password', wErrorScope.W2WErrorCodes.WNotFound, reason);
                            err.status = 400;
                            return reject(err);
                        }

                        return Q.nfcall(bcrypt.compare, password, user.passwordHash)
                            .then(function (result) {
                                if (!result) {
                                    var reason  = new wErrorScope.W2WErrorReason(wErrorScope.W2WReasonKeys.WInvalidUserOrPassword);
                                    var err = new wErrorScope.classes.W2WError('Wrong user or password', wErrorScope.W2WErrorCodes.WValidation, reason);
                                    err.status = 400;
                                    return reject(err);
                                } else {
                                    return resolve(user);
                                }
                            });
                    })
                    .fail(function (err) {
                        reject(WErrorParser.tryParseError(err));
                    })
                    .done();
            });
        },

        /**
         * Method for authentication with bearer token strategy
         *
         * @param {String} token token from request
         * @returns {Q.Promise} promise promise with success or failure of user authentication
         */
        authenticateWithBearerTokenStrategy: function(token) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.SecurityUserToken.find, {
                    accessToken: token
                }, {
                    limit: 1
                })
                    .then(function(tokens) {
                        var userToken = _.first(tokens);
                        if (!userToken) {
                            var reason = new WErrorReason(wErrorScope.W2WReasonKeys.WInvalidAccessToken);
                            var error = new wErrorScope.classes.W2WDbError('Invalid access token', wErrorScope.W2WErrorCodes.WForbidden, reason);
                            error.status = 401;
                            return reject(error);
                        }

                        var payload = utils.token.decodeAccessToken(userToken.accessToken);
                        if (moment(payload.expired).isBefore(utils.date.now())) {
                            var reason = new WErrorReason(wErrorScope.W2WReasonKeys.WAccessTokenExpired);
                            var error = new wErrorScope.classes.W2WDbError('Access token expired', wErrorScope.W2WErrorCodes.WForbidden, reason);
                            error.status = 401;
                            return reject(error);
                        }

                        return Q.nfcall(userToken.getUser);
                    })
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Generate token for authenticated user
         *
         * @param {UserAccount} user user which was authenticated
         *
         * @returns {Q.Promise} promise promise with result of token generation
         */
        generateBearerToken: function(user) {
            return Q.Promise(function(resolve, reject) {
                var expiredIn = utils.date.expiredInWeek();
                var genToken = utils.token.generateAccessToken({ expired: expiredIn });
                var refreshToken = utils.token.generateRefreshToken({
                    created: utils.date.now(),
                    client: user.id
                });

                Q.nfcall(db.models.SecurityUserToken.find, {
                    user_id: user.id
                })
                    .then(function(tokens) {
                        if (_.isEmpty(tokens)) {
                            return Q.nfcall(db.models.SecurityUserToken.create, {
                                expiredIn: expiredIn,
                                accessToken: genToken,
                                refreshToken: refreshToken,
                                user_id: user.id
                            });
                        }

                        var token = _.first(tokens);
                        token.expiredIn = expiredIn;
                        token.accessToken = genToken;
                        return Q.nfcall(token.save);
                    })
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    })
                    .done();
            });
        },

        /**
         * Refreshing expired or near to it token with special refreshToken
         *
         * @param {UserAccount} user user which is trying to refresh token
         * @param {String} refreshToken refreshToken special token which allow user to refresh token
         *
         * @returns {Q.Promise} promise promise with result of refreshing token process
         */
        refreshBearerToken: function(user, refreshToken) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.SecurityUserToken.find, {
                    user_id: user.id,
                    refreshToken: refreshToken
                }, {
                    limit: 1
                })
                    .then(function(tokens) {
                        var token = _.first(tokens);
                        if (!token) {
                            var reason = new WErrorReason(WReasonKeys.WInvalidRefreshToken);
                            var error = new wErrorScope.classes.W2WDbError('Invalid refreshToken', wErrorScope.W2WErrorCodes.WServiceBadParameters, reason);
                            error.status = 400;
                            return reject(error);
                        }

                        token.expiredIn = utils.date.expiredInWeek();
                        token.accessToken = utils.token.generateAccessToken({ expired: token.expiredIn });
                        return Q.nfcall(token.save);
                    })
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    })
                    .done();
            });
        },

        revokeBearerToken: function(user) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.SecurityUserToken.find, { user_id: user.id })
                    .then(function (userTokens) {
                        if (_.isEmpty(userTokens)) {
                            var reason = new WErrorReason(WReasonKeys.WServiceBadParameters);
                            var internalErr = new wErrorScope.classes.W2WError('Invalid token was used', wErrorScope.W2WErrorCodes.WInvalidRequest, reason);
                            internalErr.status = 401;
                            return reject(internalErr);
                        }
                        
                        return Q.nfcall(_.first(userTokens).remove);
                    })
                    .then(resolve)
                    .fail(function (err) {
                        var internalErr = WErrorParser.tryParseError(err);
                        internalErr.status = 500;
                        reject(internalErr);
                    })
                    .done();
            });
        }
    };

    /**
     * Perform validation of e-mail address. It uses DB to check uniqueness of e-mail and some
     * other technique.
     *
     * @param {String} email e-mail
     * @param {W2WError} validationError error object which accumulate error reasons if they appears
     *
     * @return {Q.Promise} promise with accumulated or created error object
     */
    function validateEmail(email, validationError) {
        return Q.Promise(function (resolve, reject) {
            if (!validator.validateEmail(email)) {
                var reasonObj = new WErrorReason(WReasonKeys.WEmailBadStructure);
                validationError = WErrorParser.updateServiceValidationError(validationError, reasonObj);
                resolve(validationError);
            } else {
                Q.nfcall(db.models.UserAccount.exists, { email: email })
                    .then(function (userAlreadyExists) {
                        if (userAlreadyExists) {
                            var reasonObj = new WErrorReason(WReasonKeys.WEmailExists);
                            validationError = WErrorParser.updateServiceValidationError(validationError, reasonObj);
                        }

                        resolve(validationError);
                    })
                    .fail(function (err) {
                        reject(WErrorParser.tryParseError(err));
                    })
                    .done();
            }
        });
    }

    /**
     * Perform validation of nickname. It uses DB to check uniqueness of nickname and some
     * other technique.
     *
     * @param {String} nickname nickname
     * @param {W2WError} validationError error object which accumulate error reasons if they appears
     *
     * @return {Q.Promise} promise with accumulated or created error object
     */
    function validateNickname(nickname, validationError) {
        return Q.Promise(function (resolve, reject) {
            if (!validator.validateUsername(nickname)) {
                var reasonObj = new WErrorReason(WReasonKeys.WNicknameStrength);
                validationError = WErrorParser.updateServiceValidationError(validationError, reasonObj);
                resolve(validationError);
            } else {
                Q.nfcall(db.models.UserAccount.exists, { nickname: nickname })
                    .then(function (nicknameAlreadyExists) {
                        if (nicknameAlreadyExists) {
                            var reasonObj = new WErrorReason(WReasonKeys.WNicknameExists);
                            validationError = WErrorParser.updateServiceValidationError(validationError, reasonObj);
                        }

                        resolve(validationError);
                    })
                    .fail(function (err) {
                        reject(WErrorParser.tryParseError(err));
                    })
                    .done();
            }
        });
    }

    /**
     * Perform validation of password. It uses regex to check uniqueness of password.
     *
     * @param {String} password
     * @param {W2WError} validationError error object which accumulate error reasons if they appears
     *
     * @return {Q.Promise} promise with accumulated or created error object
     */
    function validatePassword(password, validationError) {
        return Q.Promise(function (resolve) {
            if (!validator.validatePassword(password)) {
                var reasonObj = new WErrorReason(WReasonKeys.WPasswordStrength);
                validationError = WErrorParser.updateServiceValidationError(validationError, reasonObj);
            }
            resolve(validationError);
        });
    }
};