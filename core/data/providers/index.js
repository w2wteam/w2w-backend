/**
 * Created by alesanro on 1/27/15.
 */
module.exports = function(options, imports, register) {
    // WARNING: some distortion in object initialization (think about refactoring)
    var self = {};
    imports.dataProviders = self;

    self.MovieProvider = require('./movie-provider')(imports);
    self.PersonProvider = require('./person-provider')(imports);
    self.UserRelationsProvider = require('./user-relations-provider')(imports);
    self.UserAuthenticationProvider = require('./user-authentication-provider')(imports);
    self.CreditProvider = require('./credit-provider')(imports);

    register(null, {
        dataProviders: self
    });
};
