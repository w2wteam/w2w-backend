/**
 * Created by alesanro on 1/28/15.
 */

var validator = require('validator');

module.exports = {
    /**
     * Validate passwords with at least 1 letter, at least 1 non-letter, and at least 6 characters in length.
     *
     * @param password
     * @returns {boolean}
     */
    validatePassword: function(password) {
        var pattern = '^(.{0,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{4,})|' +
            '(.{1,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{3,})|(.{2,}(([a-zA-Z][^a-zA-Z])|' +
            '([^a-zA-Z][a-zA-Z])).{2,})|(.{3,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{1,})|' +
            '(.{4,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{0,})$';

        return !validator.isNull(password) && validator.matches(password, pattern);
    },

    /**
     * Validate username which doesn't allow special characters other than underscore.
     * Username must be of length ranging(3-30). starting letter should be a number or a character.
     *
     * @param username
     * @returns {boolean}
     */
    validateUsername: function(username) {
        var pattern = '^[a-zA-Z0-9][a-zA-Z0-9_]{2,29}$';
        return !validator.isNull(username) && validator.matches(username, pattern);
    },

    /**
     * Validate email
     *
     * @param email
     * @returns {boolean}
     */
    validateEmail: function(email) {
        return validator.isEmail(email);
    }
};