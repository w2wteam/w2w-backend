/**
 * Created by alesanro on 1/27/15.
 */

var _ = require('lodash');
var Q = require('q');

module.exports = function(imports) {
    "use strict";

    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var providers = imports.dataProviders;
    var db = imports.database.db;
    var moviesFacade = imports.moviesFacade;

    return {
        /**
         * Fetch movie by ID (only internal DB)
         *
         * @param {Number} movieId id of movie
         *
         * @return {Q.Promise} promise with Movie model object
         */
        getById: function(movieId) {
            return moviesFacade.Movies.getById(movieId);
        },

        /**
         * Populate or add user-specific properties to movie object
         *
         * @param {Movie} movie object of Movie model
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated movie
         */
        getMoviePopulatedForUser: function(movie, userId) {
            if (!movie) {
                return Q();
            }

            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.UserRelationsToMovie.find, {
                    movie_id: movie.id,
                    user_id: userId
                }, { limit: 1 })
                    .then(function(markedMovie) {
                        db.models.UserRelationsToMovie.markObjectWithMovie(movie, _.first(markedMovie));
                        resolve(movie);
                    }).fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Populate or add user-specific properties to collection of movies
         *
         * @param {Array} movies collections of Movie models
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated collection of movies
         */
        getMoviesPopulatedForUser: function(movies, userId) {
            if (_.isEmpty(movies)) {
                return Q();
            }

            return Q.Promise(function(resolve, reject) {
                var moviesIds = _.map(movies, function(movie) {
                    return movie.id;
                });

                Q.nfcall(db.models.UserRelationsToMovie.find, {
                    movie_id: moviesIds,
                    user_id: userId
                })
                    .then(function(markedMovies) {
                        _.forEach(movies, function(movie) {
                            var markedMovie = _.find(markedMovies, { movie_id: movie.id });
                            db.models.UserRelationsToMovie.markObjectWithMovie(movie, markedMovie);
                        });

                        resolve(movies);
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch names of genres for movie category
         *
         * @return {Q.Promise} promise with collection of genre names
         */
        getGenresLabels: function() {
            return moviesFacade.Movies.getGenresLabels();
        },

        /**
         * Fetch movie by ID. Much the same as `getById`, but provide more specific properties like credits
         *
         * @param {Number} movieId id of movie
         *
         * @return {Q.Promise} promise with movie object
         */
        getMovieDetails: function(movieId) {
            return Q.Promise(function(resolve, reject) {
                providers.MovieProvider.getById(movieId)
                    .then(function(movie) {
                        return providers.CreditProvider.getCreditsForMovie(movie)
                            .then(function(credits) {
                                resolve(movie.populateWithCredits(credits));
                            });
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Populate movie with user-specific properties and detailed information about movie
         *
         * @param {Movie} movie object of Movie model
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated movie object
         */
        getMovieDetailsPopulatedForUser: function(movie, userId) {
            if (!movie) {
                return Q();
            }

            return providers.MovieProvider.getMoviePopulatedForUser(movie, userId)
                .then(function(movie) {
                    return providers.CreditProvider.getCreditsPopulatedForUser(movie.credits, userId)
                        .then(function(credits) {
                            return Q(movie.populateWithCredits(credits));
                        });
                });
        },

        /**
         * Perform search of movies by some query (like `q=matrix`)
         *
         * @param {Object} query query object
         *
         * @return {Q.Promise} promise with collection of found movies
         */
        getSearchMovieWithQuery: function(query) {
            return moviesFacade.Movies.searchMovieWithQuery(query);
        },

        /**
         * Populate or add user-specific properties to search results
         *
         * @param {Object|Array} searchResults results of search process
         * @param {Number} userId id of user which preferences will be used
         *
         * @return {Q.Promise} promise with populated search results
         */
        getSearchMoviesPopulatedForUser: function(searchResults, userId) {
            return providers.MovieProvider.getMoviesPopulatedForUser(searchResults, userId);
        }
    };
};