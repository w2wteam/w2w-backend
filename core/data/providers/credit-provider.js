/**
 * Created by alesanro on 2/1/15.
 */

var _ = require('lodash');
var Q = require('q');

module.exports = function(imports) {
    "use strict";
    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var db = imports.database.db;
    var moviesFacade = imports.moviesFacade;

    return {
        /**
         * Fetch credit by ID (only internal DB).
         *
         * @param {Number} creditId id of credit
         *
         * @return {Q.Promise} promise with Credit
         */
        getById: function(creditId) {
            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.Credit.get, creditId)
                    .then(resolve)
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Populate or add some user-specific properties to credits
         *
         * @param {Array} credits collections for target credits
         * @param {Number} userId user which will be used to populate credits with preferences
         *
         * @return {Q.Promise} promise with updated collection of credits
         */
        getCreditsPopulatedForUser: function(credits, userId) {
            if (_.isEmpty(credits)) {
                return Q();
            }

            var creditsIds = _.map(credits, function(credit) {
                return credit.id;
            });

            return Q.Promise(function(resolve, reject) {
                Q.nfcall(db.models.UserRelationsToCredit.find, {
                    user_id: userId,
                    credit_id: creditsIds
                })
                    .then(function(userMarkedCredits) {
                        var updatedCredits = _.map(credits, function(credit) {
                            var foundCredit = _.find(userMarkedCredits, { credit_id: credit.id });
                            db.models.UserRelationsToCredit.markObjectWithCredit(credit, foundCredit);
                            return credit;
                        });
                        resolve(updatedCredits);
                    })
                    .fail(function(err) {
                        reject(WErrorParser.tryParseError(err));
                    }).done();
            });
        },

        /**
         * Fetch credits for Movie
         *
         * @param {Movie} movie object of Movie model
         *
         * @return {Q.Promise} promise with collection of credits
         */
        getCreditsForMovie: function(movie) {
            return moviesFacade.Credits.getCreditsForMovie(movie)
                .then(function(credits) {
                    var persons = _.map(credits, function(credit) {
                        return Q.nfcall(_.bind(credit.getPerson, credit));
                    });

                    return Q.all(persons)
                        .then(function(persons) {
                            return _.map(persons, function(person, idx) {
                                return mergePersonIntoCredit(person, credits[idx]);
                            });
                        });


                    function mergePersonIntoCredit(person, credit) {
                        credit.personName = person.name;
                        credit.personProfilePath = person.profilePath;
                        return credit;
                    }
                });
        },

        /**
         * Fetch credits for Person
         *
         * @param {Person} person object of Person model
         *
         * @return {Q.Promise} promise with collection of credits
         */
        getCreditsForPerson: function(person) {
            return moviesFacade.Credits.getCreditsForPerson(person)
                .then(function(credits) {
                    var movies = _.map(credits, function(credit) {
                        return Q.nfcall(_.bind(credit.getMovie, credit));
                    });

                    return Q.all(movies)
                        .then(function(movies) {
                            return _.map(movies, function(movie, idx) {
                                return mergeMovieIntoCredit(movie, credits[idx]);
                            });
                        });

                    function mergeMovieIntoCredit(movie, credit) {
                        credit.moviePosterPath = movie.posterPath;
                        return credit;
                    }
                });
        }
    };
};