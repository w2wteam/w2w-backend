/**
 * Created by alex_rudyak on 2/27/15.
 */

var _ = require('lodash');

module.exports = function(options, imports, register) {
    /*
     * Collection operational class. Perform find by models' names, unique elements
     * and mapping. Should be used through whole project as a main class for finding
     * and filtering elements of models.
     *
     * TODO: needed to implement methods to support any used services
     */
    function ExternalLinksCollectionFinder(servicesNames) {
        console.assert(_.isArray(servicesNames));
        this._servicesNames = servicesNames;
    }
    ExternalLinksCollectionFinder.prototype.name = "ExternalLinksCollectionFinder";
    ExternalLinksCollectionFinder.prototype.find = function(collection, source) {
        // TODO: implement
        if (!source) {
            return undefined;
        }
        return _.find(collection, { tmdbId: source.tmdbId });
    };
    ExternalLinksCollectionFinder.prototype.findWithModel = function(collection, source, modelName) {
        // TODO: implement
        if (!source || _.isEmpty(modelName)) {
            return undefined;
        }
        return _.find(collection, { tmdbId: source["tmdb_" + modelName.toLowerCase() + "_id"] });
    };
    ExternalLinksCollectionFinder.prototype.unique = function(collections) {
        // TODO: implement
        return _.uniq(collections, "tmdbId");
    };

    register(null, {
        externalLinksFinder: {
            ExternalLinksCollectionFinder: ExternalLinksCollectionFinder
        }
    });
};