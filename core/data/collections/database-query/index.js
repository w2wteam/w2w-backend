/**
 * Created by alex_rudyak on 2/27/15.
 */

var _ = require('lodash');


module.exports = function(options, imports, register) {
    /*
     * Class for implementing database queries with set of external ids.
     * It provides different logic operations about elements such as AND, OR and etc.
     * Should be used to make queries to models.
     */
    function DatabaseQueryMaker(servicesNames) {
        console.assert(_.isArray(servicesNames));
        this._servicesNames = servicesNames || [];
    }
    DatabaseQueryMaker.prototype.name = "DatabaseQueryMaker";
    DatabaseQueryMaker.prototype.makeFindAnd = function(model, modelName) {
        // TODO: implement
        var itemsAnd = _.map(model, function(item) {
            return item["tmdb" + (modelName ? ("_" + modelName.toLowerCase()) + "_id" : "Id")];
        });
        return {
            tmdbId: itemsAnd
        };
    };
    DatabaseQueryMaker.prototype.makeFindOr = function(model, modelName) {
        // TODO: implement
        var itemsOr = _.map(model, function(item) {
            return item["tmdb" + (modelName ? ("_" + modelName.toLowerCase()) + "_id" : "Id")];
        });
        return {
            tmdbId: itemsOr
        };
    };

    register(null, {
        databaseQueryMakers: {
            DatabaseQueryMaker: DatabaseQueryMaker
        }
    });
};