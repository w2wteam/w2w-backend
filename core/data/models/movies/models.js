/**
 * Created by alesanro on 3/1/15.
 */

module.exports = function(options, imports, register) {
    var db = imports.database.db;

    var Person = {
        externalLinks: function(model) {
            return db.models.Person.externalLinks.call(model);
        }
    };

    var Movie = {
        externalLinks: function(model) {
            return db.models.Movie.externalLinks.call(model);
        }
    };

    var Credit = {
        externalLinks: function(model) {
            return db.models.Credit.externalLinks.call(model);
        },

        externalLinksWithModel: function(model, modelName) {
            return {
                tmdbId: model["tmdb_" + modelName.toLowerCase() + "_id"]
            };
        }
    };


    register(null, {
        movieModels: {
            Person: Person,
            Movie: Movie,
            Credit: Credit
        }
    });
};