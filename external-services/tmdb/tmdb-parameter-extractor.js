/**
 * Created by alex_rudyak on 3/5/15.
 */

var _ = require('lodash');
var RequestParameterTranslator = require('../general-transformations').RequestParameterTranslator;

var serviceIdFieldName = 'tmdbId';
var serviceName = 'tmdb';

module.exports = {
    TmdbRequestParameterTranslator: function() {
        /*
            Movie Translator
         */
        function TmdbMovieRequestParameterTranslator() {
            RequestParameterTranslator.apply(this, arguments);
        }

        TmdbMovieRequestParameterTranslator.prototype = _.create(RequestParameterTranslator.prototype);
        TmdbMovieRequestParameterTranslator.prototype.name = "TmdbMovieRequestParameterTranslator";
        TmdbMovieRequestParameterTranslator.prototype.translate = function(obj) {
            return obj.params[serviceIdFieldName];
        };

        /*
            Person Translator
         */
        function TmdbPersonRequestParameterTranslator() {
            RequestParameterTranslator.apply(this, arguments);
        }

        TmdbPersonRequestParameterTranslator.prototype = _.create(RequestParameterTranslator.prototype);
        TmdbPersonRequestParameterTranslator.prototype.name = "TmdbPersonRequestParameterTranslator";
        TmdbPersonRequestParameterTranslator.prototype.translate = function(obj) {
            return obj.params[serviceIdFieldName];
        };

        /*
            Credit Translator
         */
        function TmdbCreditRequestParameterTranslator() {
            RequestParameterTranslator.apply(this, arguments);
        }

        TmdbCreditRequestParameterTranslator.prototype = _.create(RequestParameterTranslator.prototype);
        TmdbCreditRequestParameterTranslator.prototype.name = "TmdbCreditRequestParameterTranslator";
        TmdbCreditRequestParameterTranslator.prototype.translate = function(obj) {
            return obj.params[serviceIdFieldName];
        };
        TmdbCreditRequestParameterTranslator.prototype.translateWithModel = function(obj, modelName) {
            return obj.params[serviceName + _.isEmpty(modelName) ? "Id" : ("_" + modelName.toLowerCase() + "_id")];
        };

        /*
         Search Request Translator
         */
        function TmdbSearchRequestParameterTranslator() {
            RequestParameterTranslator.apply(this, arguments);
        }

        TmdbSearchRequestParameterTranslator.prototype = _.create(RequestParameterTranslator.prototype);
        TmdbSearchRequestParameterTranslator.prototype.name = "TmdbSearchRequestParameterTranslator";
        TmdbSearchRequestParameterTranslator.prototype.translate = function(obj) {
            return {
                query: obj.query
            };
        };

        return {
            movieRequestParameterTranslator: new TmdbMovieRequestParameterTranslator(),
            personRequestParameterTranslator: new TmdbPersonRequestParameterTranslator(),
            creditRequestParameterTranslator: new TmdbCreditRequestParameterTranslator(),
            searchRequestParameterTranslator: new TmdbSearchRequestParameterTranslator()
        };
    }
};