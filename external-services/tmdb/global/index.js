/**
 * Created by alesanro on 3/20/15.
 */

module.exports = function(options, imports, register) {
    require('../tmdb')(options, imports, function(err, modules) {
        register(err, {
            movieServiceAPI: modules.tmdbAPI
        });
    });
};