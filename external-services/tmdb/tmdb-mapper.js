/**
 * Created by alesanro on 1/29/15.
 */

var _ = require('lodash');
var ResponseMapper = require('../general-transformations').ResponseMapper;


module.exports = {
    TmdbResponseMapper: function() {

        /*
         Movie
         */
        function TmdbMovieResponseMapper() {
            ResponseMapper.call(this);
        }

        TmdbMovieResponseMapper.prototype = _.create(ResponseMapper.prototype);
        TmdbMovieResponseMapper.prototype.mapModelFromResponseObject = function (obj) {
            var genres = _.map(obj.genres, function (genre) {
                return genre.name;
            });
            return {
                tmdbId: obj.id,
                adult: obj.adult,
                originalTitle: obj["original_title"],
                title: obj.title,
                releaseDate: obj["release_date"] === '' ? null : obj["release_date"],
                posterPath: obj["poster_path"],
                backdropPath: obj["backdrop_path"],
                budget: obj.budget,
                revenue: obj.revenue,
                homepage: obj.homepage,
                runtime: obj.runtime,
                status: obj.status,
                tagline: obj.tagline,
                overview: obj.overview,
                imdbId: obj["imdb_id"],
                genres: genres,
                tmdbRateAverage: obj["vote_average"],
                tmdbVoteCount: obj["vote_count"]
            };
        };
        TmdbMovieResponseMapper.prototype.mapModelsFromResponseArray = function (response) {
            return _.map(response, function (movie) {
                var mappedMovie = TmdbMovieResponseMapper.prototype.mapModelFromResponseObject(movie);
                mappedMovie._thumb = true;
                return mappedMovie;
            });
        };

        /*
         Credit
         */
        function TmdbPersonCreditResponseMapper() {
            ResponseMapper.call(this);
        }

        TmdbPersonCreditResponseMapper.prototype = _.create(ResponseMapper.prototype);
        TmdbPersonCreditResponseMapper.prototype.mapModelFromResponseObject = function (obj, personId, creditType, mediaType) {
            return {
                creditType: creditType,
                department: obj.department,
                job: obj.job,
                character: obj.character,
                mediaType: mediaType,
                title: obj.title,
                originalTitle: obj['original_title'],
                profilePath: obj['profile_path'],
                posterPath: obj['poster_path'],
                tmdb_person_id: personId,
                tmdb_movie_id: obj.id,
                tmdbId: obj['credit_id']
            };
        };
        TmdbPersonCreditResponseMapper.prototype.mapModelsFromResponseArray = function (response) {
            var personId = response.id;
            var cast = response.cast;
            var crew = response.crew;
            var credits = [];
            Array.prototype.push.apply(credits, _.map(cast, function (item) {
                return TmdbPersonCreditResponseMapper.prototype.mapModelFromResponseObject(item, personId, "cast", "movie");
            }));
            Array.prototype.push.apply(credits, _.map(crew, function (item) {
                return TmdbPersonCreditResponseMapper.prototype.mapModelFromResponseObject(item, personId, "crew", "movie");
            }));

            return credits;
        };

        function TmdbMovieCreditResponseMapper() {
            ResponseMapper.call(this);
        }

        TmdbMovieCreditResponseMapper.prototype = _.create(ResponseMapper.prototype);
        TmdbMovieCreditResponseMapper.prototype.mapModelFromResponseObject = function (obj, movieId, creditType, mediaType) {
            return {
                creditType: creditType,
                department: obj.department,
                job: obj.job,
                character: obj.character,
                mediaType: mediaType,
                name: obj.name,
                profilePath: obj['profile_path'],
                tmdb_person_id: obj.id,
                tmdb_movie_id: movieId,
                tmdbId: obj['credit_id']
            };
        };
        TmdbMovieCreditResponseMapper.prototype.mapModelsFromResponseArray = function (response) {
            var movieId = response.id;
            var cast = response.cast;
            var crew = response.crew;
            var credits = [];
            Array.prototype.push.apply(credits, _.map(cast, function (item) {
                return TmdbMovieCreditResponseMapper.prototype.mapModelFromResponseObject(item, movieId, "cast", "movie");
            }));
            Array.prototype.push.apply(credits, _.map(crew, function (item) {
                return TmdbMovieCreditResponseMapper.prototype.mapModelFromResponseObject(item, movieId, "crew", "movie");
            }));

            return credits;
        };

        /*
         Person
         */
        function TmdbPersonResponseMapper() {
            ResponseMapper.call(this);
        }

        TmdbPersonResponseMapper.prototype = _.create(ResponseMapper.prototype);
        TmdbPersonResponseMapper.prototype.mapModelFromResponseObject = function (obj) {
            return {
                tmdbId: obj.id,
                name: obj.name,
                alsoKnownAs: _.first(obj["also_known_as"]),
                profilePath: obj["profile_path"],
                biography: obj.biography,
                birthday: obj.birthday === "" ? null : obj.birthday,
                deathday: obj.deathday === "" ? null : obj.deathday,
                homepage: obj.homepage,
                placeOfBirth: obj["place_of_birth"]
            };
        };
        TmdbPersonResponseMapper.prototype.mapModelsFromResponseArray = function (response) {
            return _.map(response, function (person) {
                var mappedPerson = TmdbPersonResponseMapper.prototype.mapModelFromResponseObject(person);
                mappedPerson._thumb = true;
                return mappedPerson;
            });
        };

        /*
         Genre
         */
        function TmdbGenreResponseMapper() {
            ResponseMapper.call(this);
        }
        TmdbGenreResponseMapper.prototype = _.create(ResponseMapper.prototype);
        TmdbGenreResponseMapper.prototype.mapModelFromResponseObject = function(obj) {
            return obj.name;
        };
        TmdbGenreResponseMapper.prototype.mapModelsFromResponseArray = function(response) {
            return _.map(response, function(genre) {
                return TmdbGenreResponseMapper.prototype.mapModelFromResponseObject(genre);
            });
        };

        return {
            MovieMapper: new TmdbMovieResponseMapper(),
            PersonMapper: new TmdbPersonResponseMapper(),
            CreditFromPersonMapper: new TmdbPersonCreditResponseMapper(),
            CreditFromMovieMapper: new TmdbMovieCreditResponseMapper(),
            GenreMapper: new TmdbGenreResponseMapper()
        };

    }
};