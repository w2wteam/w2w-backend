/**
 * Created by alex_rudyak on 2/27/15.
 */

var RequestQueryTranslator = require('../general-transformations').RequestQueryTranslator;

function TmdbRequestQueryTranslator() {
    RequestQueryTranslator.apply(this, arguments);
}

module.exports = {
    tmdbRequestQueryTranslator: new TmdbRequestQueryTranslator()
};