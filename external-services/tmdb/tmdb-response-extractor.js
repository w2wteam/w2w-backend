/**
 * Created by alex_rudyak on 2/26/15.
 */

var _ = require('lodash');
var ResponseObjectExtractor = require('../general-transformations').ResponseObjectExtractor;


module.exports = {
    TmdbResponseObjectExtractor: (function() {

        /**
         Movie
         */
        function TmdbMovieResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbMovieResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);

        /**
         Person
         */
        function TmdbPersonResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbPersonResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);

        /**
         Search Movies
         */
        function TmdbSearchMoviesResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbSearchMoviesResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);
        TmdbSearchMoviesResponseObjectExtractor.prototype.extractObject = function() {
            return this.response.results;
        };
        TmdbSearchMoviesResponseObjectExtractor.prototype.wrapObject = function(obj) {
            this.response.results = obj;
            return this.response.results;
        };

        /**
         Search People
         */
        function TmdbSearchPeopleResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbSearchPeopleResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);
        TmdbSearchPeopleResponseObjectExtractor.prototype.extractObject = function() {
            return this.response.results;
        };
        TmdbSearchPeopleResponseObjectExtractor.prototype.wrapObject = function(obj) {
            this.response.results = obj;
            return this.response.results;
        };

        /**
         Credits From Movies
         */
        function TmdbCreditsFromMoviesResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbCreditsFromMoviesResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);

        /**
         Credits From People
         */
        function TmdbCreditsFromPeopleResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbCreditsFromPeopleResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);

        /**
         Genres
         */
        function TmdbGenresResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbGenresResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);
        TmdbGenresResponseObjectExtractor.prototype.extractObject = function() {
            return this.response.genres;
        };
        TmdbGenresResponseObjectExtractor.prototype.wrapObject = function(obj) {
            this.response.genres = obj;
            return this.response.genres;
        };

        /**
         Discover movies
         */
        function TmdbDiscoverResponseObjectExtractor() {
            ResponseObjectExtractor.apply(this, arguments);
        }
        TmdbDiscoverResponseObjectExtractor.prototype = _.create(ResponseObjectExtractor.prototype);

        return {
            MovieResponseObjectExtractor: TmdbMovieResponseObjectExtractor,
            PersonResponseObjectExtractor: TmdbPersonResponseObjectExtractor,
            SearchMoviesResponseObjectExtractor: TmdbSearchMoviesResponseObjectExtractor,
            SearchPeopleResponseObjectExtractor: TmdbSearchPeopleResponseObjectExtractor,
            CreditsFromMoviesResponseObjectExtractor: TmdbCreditsFromMoviesResponseObjectExtractor,
            CreditsFromPeopleResponseObjectExtractor: TmdbCreditsFromPeopleResponseObjectExtractor,
            GenresResponseObjectExtractor: TmdbGenresResponseObjectExtractor,
            DiscoverResponseObjectExtractor: TmdbDiscoverResponseObjectExtractor
        };
    })()
};