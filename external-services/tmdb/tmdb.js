/**
 * Created by alesanro on 11/9/14.
 */

var _ = require('lodash');
var pathjs = require('path');
var querystring = require('querystring');
var Q = require('q');
var config = require('config');

var ResponseMapper = require('./tmdb-mapper').TmdbResponseMapper();
var ResponseObjectExtractor = require('./tmdb-response-extractor').TmdbResponseObjectExtractor;
var requestQueryTranslator = require('./tmdb-query').tmdbRequestQueryTranslator;
var requestParameterTranslator = require('./tmdb-parameter-extractor').TmdbRequestParameterTranslator();


module.exports = function(options, imports, register) {
    var _cacheMiddleware = imports.cache;
    var WErrorParser = imports.wErrorScope.W2WErrorParser;
    var request = imports.request;

    function uri(path, query) {
        var q = query || {};
        _.assign(q, {"api_key": config.get('Backend.tmdb.api_key')});

        var requestUri = config.get('Backend.tmdb.host') + path + (_.isEmpty(q) ? '' : ('?' + querystring.stringify(q)));

        return requestUri;
    }

    function str(obj) {
        return obj.toString();
    }

    function requestGetWithOptions(options) {
        _.merge(options, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        });

        var defer = Q.defer();

        var cacheKey = options.uri;
        _cacheMiddleware.get(cacheKey)
            .then(function(reply) {
                if (reply) {
                    log.info("Cache record found for key: " + cacheKey);
                    defer.resolve(reply);
                } else {
                    _requestResource(defer);
                }
            })
            .fail(function(err) {
                console.warn(err);
                _requestResource(defer);
            }).done();

        return defer.promise;


        function _requestResource(defer) {
            request.get(options, function (error, response, body) {
                var parsedBody = JSON.parse(body);

                var updatedError = WErrorParser.tmdbTryParseToError(error, parsedBody);
                if (updatedError) {
                    defer.reject(updatedError);
                } else {
                    _cacheMiddleware.set(cacheKey, parsedBody);
                    defer.resolve(parsedBody);
                }
            });
        }
    }

    function _parseResponse(response, ObjectExtractor, mapFunction) {
        var objectExtractor = new ObjectExtractor(response);
        var mappedObject = mapFunction(objectExtractor.extractObject());
        var mappedResponse = objectExtractor.wrapObject(mappedObject);
        return Q(mappedResponse);
    }

    var tmdbAPI = {
        movie: {
            getById: function getById(id) {
                return requestGetWithOptions({
                    uri: uri(pathjs.join('/movie', str(id)))
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.MovieResponseObjectExtractor, ResponseMapper.MovieMapper.mapModelFromResponseObject);
                    });
            },

            getCredits: function(id) {
                return requestGetWithOptions({
                    uri: uri(pathjs.join('/movie', str(id), 'credits'))
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.CreditsFromMoviesResponseObjectExtractor, ResponseMapper.CreditFromMovieMapper.mapModelsFromResponseArray);
                    });
            }
        },
        person: {
            getById: function(id) {
                return requestGetWithOptions({
                    uri: uri(pathjs.join('/person', str(id)))
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.PersonResponseObjectExtractor, ResponseMapper.PersonMapper.mapModelFromResponseObject);
                    });
            },

            getCredits: function(id) {
                return requestGetWithOptions({
                    uri: uri(pathjs.join('/person', str(id), 'credits'))
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.CreditsFromPeopleResponseObjectExtractor, ResponseMapper.CreditFromPersonMapper.mapModelsFromResponseArray);
                    });
            }
        },
        search: {
            getSearchMovie: function(query) {
                return requestGetWithOptions({
                    uri: uri('/search/movie', query)
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.SearchMoviesResponseObjectExtractor, ResponseMapper.MovieMapper.mapModelsFromResponseArray);
                    });
            },

            getSearchPerson: function(query) {
                return requestGetWithOptions({
                    uri: uri('/search/person', query)
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.SearchPeopleResponseObjectExtractor, ResponseMapper.PersonMapper.mapModelsFromResponseArray);
                    });
            }
        },
        discover: {
            getMovies: function(query) {
                return requestGetWithOptions({
                    uri: uri('/discover/movie', query)
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.DiscoverResponseObjectExtractor, ResponseMapper.MovieMapper.mapModelsFromResponseArray);
                    });
            }
        },
        genre: {
            getGenres: function() {
                return requestGetWithOptions({
                    uri: uri('/genre/movie/list')
                })
                    .then(function(response) {
                        return _parseResponse(response, ResponseObjectExtractor.GenresResponseObjectExtractor, ResponseMapper.GenreMapper.mapModelsFromResponseArray);
                    });
            }
        },
        useCache: function(cacheMiddleware) { _cacheMiddleware = cacheMiddleware },
        ResponseMapper: ResponseMapper,
        ResponseObjectExtractor: ResponseObjectExtractor,
        RequestQueryTranslator: requestQueryTranslator,
        RequestParameterTranslator: requestParameterTranslator,
        name: "The MovieDB Service",
        externalIdFields: ["tmdbId"],
        serviceId: "tmdb",
        serviceIds: function() { return ["tmdb"]; }
    };

    if (imports.movieServiceAPI) {
        imports.movieServiceAPI.register(tmdbAPI);
    }

    if (imports.movieRequestParametersTranslatorService) {
        imports.movieRequestParametersTranslatorService.register(requestQueryTranslator, tmdbAPI.serviceId);
    }

    register(null, {
        tmdbAPI: tmdbAPI
    });
};
