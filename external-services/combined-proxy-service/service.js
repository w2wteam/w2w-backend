/**
 * Created by alex_rudyak on 2/26/15.
 */

var _ = require('lodash');
var Q = require('q');
var RequestParametersTranslator = require('./request-parameters-parser').ProxyServiceRequestParameterTranslator;

module.exports = function(options, imports, register) {
    var log = imports.logger;
    var responseCombinator = imports.responseCombinator;
    var requestParametersTranslatorService = new RequestParametersTranslator();
    var _serviceQueue = [];
    var _cacheMiddleware;
    var _defaultCache;

    require('../../core/caches/empty/cache-empty')(null, { logger: log }, function(err, external) {
        _defaultCache = external.cache;
        useCache(imports.cache);
    });

    register(null, {
        movieServiceAPI: {
            register: function(service) {
                log.info("Trying to register service to movieServiceAPI...");
                service.useCache(_cacheMiddleware);
                requestParametersTranslatorService.register(service.name);
                _serviceQueue.push(service);

                log.info("Service registration completed.");
            },

            movie: {
                getById: function(id) {
                    return invokeServices("movie.getById", "combineMovie", id);
                },

                getCredits: function(id) {
                    return invokeServices("movie.getCredits", "combineCreditsFromMovie", id);
                }
            },

            person: {
                getById: function(id) {
                    return invokeServices("person.getById", "combinePerson", id);
                },

                getCredits: function(id) {
                    return invokeServices("person.getCredits", "combineCreditsFromPerson", id);
                }
            },

            search: {
                getSearchMovie: function(query) {
                    return invokeServices("search.getSearchMovie", "combineSearchMovies", query);
                },

                getSearchPerson: function(query) {
                    return invokeServices("search.getSearchPerson", "combineSearchPeople", query);
                }
            },

            discover: {
                getMovies: function(query) {
                    return invokeServices("discover.getMovies", "combineDiscover", query);
                }
            },

            genre: {
                getGenres: function() {
                    return invokeServices("genre.getGenres", "combineGenres");
                }
            },

            useCache: useCache,
            RequestParameterTranslator: requestParametersTranslatorService,
            name: "Proxy Combinator Movie Service",
            serviceId: "proxy_combinator_movie",
            serviceIds: function() {
                return _.map(_serviceQueue, function(service) {
                    return service.serviceId;
                });
            }
        }
    });

    function useCache(cacheMiddleware) {
        if (!cacheMiddleware) {
            _cacheMiddleware = _defaultCache;
        } else {
            _cacheMiddleware = cacheMiddleware;
        }

        _.forEach(_serviceQueue, function(service) {
            service.useCache(_cacheMiddleware);
        });
    }

    function getDescendantProp(obj, desc) {
        var arr = desc.split(".");
        while(arr.length && (obj = obj[arr.shift()]));
        return obj;
    }

    function invokeServices(apiMethod, combinatorMethod, parameters) {
        var requests = _.map(_serviceQueue, function(api) {
            var normalizedParameters = parameters ? parameters[api.serviceId] : undefined;
            return getDescendantProp(api, apiMethod).apply(api, normalizedParameters);
        });

        return Q.all(requests)
            .then(function(responses) {
                return Q(getDescendantProp(responseCombinator, combinatorMethod)(_serviceQueue, responses));
            });
    }
};