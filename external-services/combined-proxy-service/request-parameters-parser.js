/**
 * Created by alex_rudyak on 2/27/15.
 */

var _ = require('lodash');
var RequestParameterTranslator = require('../general-transformations').RequestParameterTranslator;

function _ProxyServiceRequestParameterTranslator() {
    RequestParameterTranslator.call(this);
    this._translators = {};
}

_ProxyServiceRequestParameterTranslator.prototype.translate = function(obj) {
    return _translateForRequestTranslator.call(this, obj);
};
_ProxyServiceRequestParameterTranslator.prototype.register = function(translator, service) {
    this._translators[service] = translator;
};
_ProxyServiceRequestParameterTranslator.prototype.unregister = function(service) {
    delete this._translators[service];
};

/*
 Movie Translator
 */
function ProxyServiceMovieRequestParameterTranslator() {
    _ProxyServiceRequestParameterTranslator.call(this);
}

ProxyServiceMovieRequestParameterTranslator.prototype = _.create(_ProxyServiceRequestParameterTranslator.prototype);
ProxyServiceMovieRequestParameterTranslator.prototype.name = "ProxyServiceMovieRequestParameterTranslator";

/*
 Person Translator
 */
function ProxyServicePersonRequestParameterTranslator() {
    _ProxyServiceRequestParameterTranslator.call(this);
}

ProxyServicePersonRequestParameterTranslator.prototype = _.create(_ProxyServiceRequestParameterTranslator.prototype);
ProxyServicePersonRequestParameterTranslator.prototype.name = "ProxyServicePersonRequestParameterTranslator";

/*
 Credit Translator
 */
function ProxyServiceCreditRequestParameterTranslator() {
    _ProxyServiceRequestParameterTranslator.call(this);
    this._translators = {};
}

ProxyServiceCreditRequestParameterTranslator.prototype = _.create(_ProxyServiceRequestParameterTranslator.prototype);
ProxyServiceCreditRequestParameterTranslator.prototype.name = "ProxyServiceCreditRequestParameterTranslator";
ProxyServiceCreditRequestParameterTranslator.prototype.translateWithModel = function(obj, modelName) {
    return _translateWithModelForRequestTranslator.call(this, obj, modelName);
};

/*
 Search Request Translator
 */
function ProxyServiceSearchRequestParameterTranslator() {
    _ProxyServiceRequestParameterTranslator.call(this);
    this._translators = {}
}

ProxyServiceSearchRequestParameterTranslator.prototype = _.create(_ProxyServiceRequestParameterTranslator.prototype);
ProxyServiceSearchRequestParameterTranslator.prototype.name = "ProxyServiceSearchRequestParameterTranslator";

/*
 Root Proxy Service Translator
 */
function ProxyServiceRequestParameterTranslator() {
    this.movieRequestParameterTranslator = new ProxyServiceMovieRequestParameterTranslator();
    this.personRequestParameterTranslator = new ProxyServicePersonRequestParameterTranslator();
    this.creditRequestParameterTranslator = new ProxyServiceCreditRequestParameterTranslator();
    this.searchRequestParameterTranslator = new ProxyServiceSearchRequestParameterTranslator();

    this._translatorsKeys = [
        "movieRequestParameterTranslator",
        "personRequestParameterTranslator",
        "creditRequestParameterTranslator",
        "searchRequestParameterTranslator"
    ];
}

ProxyServiceRequestParameterTranslator.prototype.name = 'ProxyServiceRequestParameterTranslator';
ProxyServiceRequestParameterTranslator.prototype.register = function(translator, service) {
    var _this = this;

    _.forEach(_this._translatorsKeys, function(translatorKey) {
        _this[translatorKey].register(translator[translatorKey], service);
    });

};
ProxyServiceRequestParameterTranslator.prototype.unregister = function(service) {
    var _this = this;
    _.forEach(_this._translatorsKeys, function(translatorKey) {
        _this[translatorKey].unregister(service);
    });
};

function _translateForRequestTranslator(obj) {
    var _this = this;
    return _.reduce(_this._translators, function(result, value, key) {
        result[key] = value.translate(obj);
        return result;
    }, {});
}

function _translateWithModelForRequestTranslator(obj, modelName) {
    var _this = this;
    return _.reduce(_this._translators, function(result, value, key) {
        result[key] = value.translateWithModel(obj, modelName);
        return result;
    }, {});
}


module.exports = {
    ProxyServiceRequestParameterTranslator: ProxyServiceRequestParameterTranslator
};
