/**
 * Created by alex_rudyak on 2/26/15.
 */

var _ = require('lodash');

module.exports = function(options, imports, register) {
    register(null, {
        responseCombinator: {
            combineMovie: function(services, responses) {
                return _.first(responses);
            },

            combinePerson: function(services, responses) {
                return _.first(responses);
            },

            combineCreditsFromMovie: function(services, responses) {
                return _.first(responses);
            },

            combineCreditsFromPerson: function(services, responses) {
                return _.first(responses);
            },

            combineSearchMovies: function(services, responses) {
                return _.first(responses);
            },

            combineSearchPeople: function(services, responses) {
                return _.first(responses);
            },

            combineGenres: function(services, responses) {
                return _.first(responses);
            },

            /*
            Temporary placed method used only for completeness of service API functions
             */
            combineDiscover: function(services, responses) {
                return _.first(responses);
            }
        }
    });
};