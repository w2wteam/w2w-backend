/**
 * Created by alesanro on 1/29/15.
 */

function ResponseObjectMapper() {
    this.message = "test_object_mapper";
}

ResponseObjectMapper.prototype.name = "GenericResponseObjectMapper";
ResponseObjectMapper.prototype.mapModelFromResponseObject = function(obj) {
    throw new Error("Need to overload method");
};
ResponseObjectMapper.prototype.mapModelsFromResponseArray = function(obj) {
    throw new Error("Need to overload method");
};

module.exports = {
    ResponseObjectMapper: ResponseObjectMapper
};
