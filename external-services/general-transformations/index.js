/**
 * Created by alesanro on 1/29/15.
 */

module.exports = {
    ResponseMapper: require('./generic-mapper').ResponseObjectMapper,
    ResponseObjectExtractor: require('./generic-object-extractor').ResponseObjectExtractor,
    RequestQueryTranslator: require('./generic-query-translator').RequestQueryTranslator,
    RequestParameterTranslator: require('./generic-parameter-translator').RequestParameterTranslator
};