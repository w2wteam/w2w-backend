/**
 * Created by alex_rudyak on 2/27/15.
 */

function RequestQueryTranslator() {
}

RequestQueryTranslator.prototype.name = "RequestQueryTranslator";
RequestQueryTranslator.prototype.translate = function(query) {
    return query;
};

module.exports = {
    RequestQueryTranslator: RequestQueryTranslator
};