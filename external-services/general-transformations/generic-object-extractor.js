/**
 * Created by alex_rudyak on 2/26/15.
 */

function ResponseObjectExtractor(response) {
    this.response = response;
}

ResponseObjectExtractor.prototype.name = "ResponseObjectExtractor";
ResponseObjectExtractor.prototype.extractObject = function() {
    return this.response;
};
ResponseObjectExtractor.prototype.wrapObject = function(object) {
    return object;
};

module.exports = {
    ResponseObjectExtractor: ResponseObjectExtractor
};