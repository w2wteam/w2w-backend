/**
 * Created by alex_rudyak on 3/5/15.
 */

/*
 * General parameter translator for external services.
 * Object of options has the next structure:
 * {
 *      params: {<some object>},
 *      query: {<object with query>}
 * }
 */
function RequestParameterTranslator() {

}
RequestParameterTranslator.prototype.name = "RequestParameterTranslator";
RequestParameterTranslator.prototype.translate = function(obj) {
    return undefined;
};
RequestParameterTranslator.prototype.translateWithModel = function(obj, modelName) {
    return undefined;
};

module.exports = {
    RequestParameterTranslator: RequestParameterTranslator
};