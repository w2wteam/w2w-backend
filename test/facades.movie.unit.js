/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var Q = require('q');
var _ = require('lodash');

describe("Facade-Movie", function() {
    "use strict";
    var app;
    var facadeMovie;
    var database;
    var databaseHelper;

    before(function(done){
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            facadeMovie = app.getService('moviesFacade').Movies;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    describe('search', function() {
        var searchQuery = 'matrix';

        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should save results in database and return non-empty result', function(done) {
            facadeMovie.searchMovieWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    should(searchResults).be.an.Object;
                    searchResults.should.not.be.empty;

                    return Q.nfcall(database.models.Movie.find)
                        .then(function(movies) {
                            should.exist(movies);
                            searchResults.length.should.be.equal(movies.length);
                            done();
                        });
                })
                .fail(function(err) {
                    done(err);
                })
                .done();
        });
    });

    describe('genres', function() {
        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should return list of genres title', function(done) {
            facadeMovie.getGenresLabels()
                .then(function(genres) {
                    should.exist(genres);
                    should(genres).be.an.Array;
                    genres.should.be.not.empty;
                    _.forEach(genres, function(genre) {
                        should(genre).be.String;
                    });

                    return Q.nfcall(database.models.Genre.find)
                        .then(function(dbGenres) {
                            should.exist(dbGenres);
                            dbGenres.should.be.length(genres.length);
                            done();
                        });
                })
                .fail(function(err) {
                    done(err);
                })
                .done();
        });
    });

    describe('get by id', function() {
        var searchQuery = 'matrix';
        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should return movie with existed id', function(done) {
            facadeMovie.searchMovieWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    searchResults.should.not.be.empty;
                    var movie = _.first(searchResults);
                    return facadeMovie.getById(movie.id)
                })
                .then(function(movie) {
                    should.exist(movie);
                    should(movie).be.an.Object;
                    movie.should.have.property('title');
                    done();
                })
                .fail(function(err) {
                    done(err);
                })
                .done();
        });

        it('should throw error with not existed movie id', function(done) {
            facadeMovie.getById(1)
                .then(function() {
                    done(new Error("Error should be thrown, because movie does not exist in db"));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);
                    done();
                })
                .done();
        });
    });
});