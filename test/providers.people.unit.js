/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');

describe('Providers-People', function() {
    "use strict";
    var app;
    var personProvider;
    var userHelper;
    var database;
    var databaseHelper;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            personProvider = app.getService('dataProviders').PersonProvider;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            userHelper = require('./support/user-helper')(app.getService('dataProviders'), database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    function clearSyncDb(done) {
        databaseHelper.clearDBWithSyncOfModels([
            database.models.UserAccount,
            database.models.SecurityUserToken,
            database.models.Person,
            database.models.UserRelationsToPerson,
            database.models.Credit,
            database.models.UserRelationsToCredit,
            database.models.Movie,
            database.models.UserRelationsToMovie
        ], done);
    }

    describe("when get person by id", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for non-existed person', function(done) {
            personProvider.getById(1)
                .then(function() {
                    done(new Error('Should throw error for non-existed person id'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.instanceOf(Error);

                    done();
                });
        });

        it('should return single person with basic info', function(done) {
            var searchQuery = 'brad pitt';

            console.info(require('util').inspect(Object.keys(personProvider)));
            personProvider.getSearchPersonWithQuery(searchQuery)
                .then(_.first)
                .then(function(person) {
                    return personProvider.getById(person.id);
                })
                .then(function(person) {
                    should.exist(person);
                    person.should.be.an.Object;
                    person.should.not.have.property('credits');
                    person.should.not.have.property('favorite');

                    done();
                })
                .fail(done);
        });
    });

    describe("when populate person", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should do nothing for undefined person', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getPersonPopulatedForUser(null, user.id)
                })
                .then(function(response) {
                    should.not.exist(response);
                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(person) {
                            return personProvider.getPersonPopulatedForUser(person, user.id);
                        });
                })
                .then(function(populatedPerson) {
                    should.exist(populatedPerson);
                    populatedPerson.should.be.an.Object;
                    populatedPerson.should.have.property('favorite');
                    should.exist(populatedPerson.favorite);
                    populatedPerson.favorite.should.be.not.ok;

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(_.first)
                        .then(function (person) {
                           return Q.nfcall(database.models.UserRelationsToPerson.create, {
                                user_id: user.id,
                                person_id: person.id,
                                favorite: true
                            })
                                .then(function(relation) {
                                   console.info(relation.favorite);
                                    return personProvider.getPersonPopulatedForUser(person, user.id);
                                });
                        });
                })
                .then(function(populatedPerson) {
                    should.exist(populatedPerson);
                    populatedPerson.should.be.an.Object;
                    populatedPerson.should.have.property('favorite');
                    populatedPerson.favorite.should.be.ok;

                    done();
                })
                .fail(done);
        });
    });

    describe("when populate collection of people", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should do nothing for empty collection', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getPeoplePopulatedForUser(null, user.id);
                })
                .then(function(populatedUser) {
                    should.not.exist(populatedUser);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(function(people) {
                            return personProvider.getPeoplePopulatedForUser(people, user.id);
                        });
                })
                .then(function(populatedPeople) {
                    should.exist(populatedPeople);
                    should(populatedPeople).be.an.Array;
                    populatedPeople.should.not.be.empty;

                    _.forEach(populatedPeople, function(person) {
                        should(person).be.an.Object;
                        person.should.have.property('favorite');
                        person.favorite.should.not.be.ok;
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(function(people) {
                            var relations = _.map(people, function(person) {
                                return {
                                    user_id: user.id,
                                    person_id: person.id,
                                    favorite: true
                                };
                            });
                            return Q.nfcall(database.models.UserRelationsToPerson.create, relations)
                                .then(function() {
                                    return personProvider.getPeoplePopulatedForUser(people, user.id);
                                });
                        });
                })
                .then(function(populatedPeople) {
                    should.exist(populatedPeople);
                    should(populatedPeople).be.an.Array;
                    populatedPeople.should.not.be.empty;

                    _.forEach(populatedPeople, function(person) {
                        should(person).be.an.Object;
                        person.should.have.property('favorite');
                        person.favorite.should.be.ok;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get person details", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for non-existed person', function(done) {
            personProvider.getPersonDetails()
                .then(function() {
                    done(new Error('Error for non-existed person when getting person details'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });

        });

        it('should add advanced info to basic person (credits)', function(done) {
            var searchQuery = 'brad pitt';

            personProvider.getSearchPersonWithQuery(searchQuery)
                .then(_.first)
                .then(function(person) {
                    return personProvider.getPersonDetails(person.id);
                })
                .then(function(person) {
                    should.exist(person);
                    should(person).be.an.Object;

                    person.should.have.property('credits');
                    should(person.credits).be.an.Array;
                    person.credits.should.not.be.empty;

                    done();
                })
                .fail(done);
        });
    });

    describe("when get populated person details", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should do nothing for undefined person', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getPersonDetailsPopulatedForUser(null, user.id);
                })
                .then(function(person) {
                    should.not.exist(person);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'brad pitt';
            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(person) {
                            return personProvider.getPersonDetails(person.id);
                        })
                        .then(function(person) {
                            return personProvider.getPersonDetailsPopulatedForUser(person, user.id);
                        });
                })
                .then(function(populatedPerson) {
                    should.exist(populatedPerson);
                    should(populatedPerson).be.an.Object;
                    populatedPerson.should.have.property('favorite');
                    populatedPerson.favorite.should.not.be.ok;
                    populatedPerson.should.have.property('credits');
                    populatedPerson.credits.should.not.be.empty;

                    _.forEach(populatedPerson.credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(person) {
                            return Q.nfcall(database.models.UserRelationsToPerson.create, {
                                user_id: user.id,
                                person_id: person.id,
                                favorite: true
                            })
                                .then(function(relation) {
                                    return personProvider.getPersonDetails(person.id);
                                }).then(function(person) {
                                    return personProvider.getPersonDetailsPopulatedForUser(person, user.id);
                                });
                        });
                })
                .then(function(populatedPerson) {
                    should.exist(populatedPerson);
                    should(populatedPerson).be.an.Object;
                    populatedPerson.should.have.property('favorite');
                    populatedPerson.favorite.should.be.ok;
                    populatedPerson.should.have.property('credits');
                    populatedPerson.credits.should.not.be.empty;

                    _.forEach(populatedPerson.credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get found people by search", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for wrong-formed query');

        it('should return collection of results', function(done) {
            var searchQuery = 'brad pitt';

            personProvider.getSearchPersonWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    should(searchResults).be.an.Array;
                    searchResults.should.not.be.empty;

                    _.forEach(searchResults, function(entry) {
                        should(entry).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get populated people found by search", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should do nothing for empty collection', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPeoplePopulatedForUser(null, user.id);
                })
                .then(function(populatedUser) {
                    should.not.exist(populatedUser);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(function(people) {
                            return personProvider.getSearchPeoplePopulatedForUser(people, user.id);
                        });
                })
                .then(function(populatedPeople) {
                    should.exist(populatedPeople);
                    should(populatedPeople).be.an.Array;
                    populatedPeople.should.not.be.empty;

                    _.forEach(populatedPeople, function(person) {
                        should(person).be.an.Object;
                        person.should.have.property('favorite');
                        person.favorite.should.not.be.ok;
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'brad pitt';

            userHelper.getUser()
                .then(function(user) {
                    return personProvider.getSearchPersonWithQuery(searchQuery)
                        .then(function(people) {
                            var relations = _.map(people, function(person) {
                                return {
                                    user_id: user.id,
                                    person_id: person.id,
                                    favorite: true
                                };
                            });
                            return Q.nfcall(database.models.UserRelationsToPerson.create, relations)
                                .then(function() {
                                    return personProvider.getSearchPeoplePopulatedForUser(people, user.id);
                                });
                        });
                })
                .then(function(populatedPeople) {
                    should.exist(populatedPeople);
                    should(populatedPeople).be.an.Array;
                    populatedPeople.should.not.be.empty;

                    _.forEach(populatedPeople, function(person) {
                        should(person).be.an.Object;
                        person.should.have.property('favorite');
                        person.favorite.should.be.ok;
                    });

                    done();
                })
                .fail(done);
        });
    });
});