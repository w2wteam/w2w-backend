/**
 * Created by alesanro on 3/4/15.
 */

var should = require("should");

describe("Combined Proxy Service", function() {
    "use strict";
    var app;
    var service;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            service = app.getService('movieServiceAPI');
            done();
        });
    });

    after(function() {
        app.destroy();
    });


    it('should exist', function() {
        should.exist(service);
    });

    //TODO: add test-cases
});