/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');
var moment = require('moment');
var utils = require('../helpers').utils;

describe('Providers-Authentication', function() {
    "use strict";
    var app;
    var providers;
    var authProvider;
    var database;
    var databaseHelper;

    var validNickname = 'test';
    var validEmail = 'test@email.com';
    var validPassword = '123qwe';

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            providers = app.getService('dataProviders');
            authProvider = providers.UserAuthenticationProvider;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    function createUser() {
        return authProvider.registerUser(validNickname, validEmail, validPassword);
    }

    describe('when user trying to register', function() {
        var validNickname1 = 'test1_user';
        var validEmail1 = 'test1@email.com';

        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount
            ], done);
        });

        function failUserRegistration(nickname, email, password, done) {
            authProvider.registerUser(nickname, email, password)
                .then(function() {
                    done(new Error('Should be an error for non-valid parameter'));
                })
                .fail(function(err) {
                    should.exist(err);

                    Q.nfcall(database.models.UserAccount.find)
                        .then(function(foundUsers) {
                            should.exist(foundUsers);
                            foundUsers.should.be.empty;
                            done();
                        })
                        .fail(done);
                });
        }

        it('should register new user with all valid parameters', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    should.exist(user);
                    user.nickname.should.be.equal(validNickname);
                    user.email.should.be.equal(validEmail);

                    return Q.nfcall(database.models.UserAccount.get, user.id)
                        .then(function(dbUser) {
                            should.exist(dbUser);
                            user.nickname.should.be.equal(dbUser.nickname);
                            user.email.should.be.equal(dbUser.email);

                            done();
                        });
                })
                .fail(done);
        });

        it('should throw error when user with such @email is already exist', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    should.exist(user);

                    return authProvider.registerUser(validNickname1, validEmail, validPassword)
                        .then(function() {
                            done(new Error('Should be error for registering already existed user'));
                        })
                        .fail(function(err) {
                            should.exist(err);
                            should(err).be.an.instanceOf(Error);
                            err.should.have.property('reasons');
                            err.reasons.should.not.be.empty;
                            err.reasons.should.be.length(1);

                            done();
                        });
                })
                .fail(done);
        });

        it('should throw error when user with such @nickname is already exist', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    should.exist(user);

                    return authProvider.registerUser(validNickname, validEmail1, validPassword)
                        .then(function() {
                            done(new Error('Should be error for registering already existed user'));
                        })
                        .fail(function(err) {
                            should.exist(err);
                            should(err).be.an.instanceOf(Error);
                            err.should.have.property('reasons');
                            err.reasons.should.not.be.empty;
                            err.reasons.should.be.length(1);

                            done();
                        });
                })
                .fail(done);
        });

        describe('with bad parameters', function() {
            it('should throw error when @nickname does not pass restrictions (too short)', function(done) {
                failUserRegistration("as", validEmail, validPassword, done);
            });

            it('should throw error when @nickname does not pass restrictions (has invalid symbols)', function(done) {
                failUserRegistration("@jfkd&()", validEmail, validPassword, done);
            });

            it('should throw error when @email does not pass restrictions', function(done) {
                failUserRegistration(validNickname, "", validPassword, done);
            });

            it('should throw error when @email does not pass restrictions', function(done) {
                failUserRegistration(validNickname, "test", validPassword, done);
            });

            it('should throw error when @email does not pass restrictions', function(done) {
                failUserRegistration(validNickname, "test@email", validPassword, done);
            });

            it('should throw error when @email does not pass restrictions', function(done) {
                failUserRegistration(validNickname, "@email.com", validPassword, done);
            });

            it('should throw error when @email does not pass restrictions', function(done) {
                failUserRegistration(validNickname, "t@m.r", validPassword, done);
            });

            it('should throw error when @password is too weak (short)', function(done) {
                failUserRegistration(validNickname, validEmail, '', done);
            });

            it('should throw error when @password is too weak (short)', function(done) {
                failUserRegistration(validNickname, validEmail, '23456', done);
            });

            it('should throw error when @password is too weak (has only numbers)', function(done) {
                failUserRegistration(validNickname, validEmail, '231245', done);
            });

            it('should throw error when @password is too weak (has only symbols)', function(done) {
                failUserRegistration(validNickname, validEmail, 'asdfgh', done);
            });
        });
    });

    describe('when user trying to authenticate with password', function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount
            ], done);
        });

        it('should throw error for wrong @nickname', function() {
            authProvider.authenticateWithPasswordStrategy('_test_', validPassword).should.rejectedWith(Error);
        });

        it('should throw error for wrong @email', function() {
            authProvider.authenticateWithPasswordStrategy('_test_@email.com', validPassword).should.rejectedWith(Error);
        });

        it('should throw error for wrong @password (valid @nickname)', function() {
            authProvider.authenticateWithPasswordStrategy(validNickname, '123').should.rejectedWith(Error);
        });

        it('should throw error for wrong @password (valid @email)', function() {
            authProvider.authenticateWithPasswordStrategy(validEmail, '123').should.rejectedWith(Error);
        });

        it('should success with valid @nickname', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.authenticateWithPasswordStrategy(validNickname, validPassword)
                        .then(function(authUser) {
                            should.exist(authUser);
                            should(authUser).be.an.Object;
                            authUser.nickname.should.be.equal(user.nickname);

                            done();
                        });
                })
                .fail(done);
        });

        it('should success with valid @email', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.authenticateWithPasswordStrategy(validEmail, validPassword)
                        .then(function(authUser) {
                            should.exist(authUser);
                            should(authUser).be.an.Object;
                            authUser.nickname.should.be.equal(user.nickname);

                            done();
                        });
                })
                .fail(done);
        });
    });

    describe('when user trying get access token', function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount,
                database.models.SecurityUserToken
            ], done);
        });

        it('should get new access token', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                })
                .then(function(token) {
                    should.exist(token);
                    should(token).be.an.Object;
                    token.should.have.property('accessToken');
                    token.should.have.property('refreshToken');
                    token.should.have.property('expiredIn');

                    return Q.nfcall(database.models.SecurityUserToken.find, {
                        accessToken: token.accessToken
                    })
                        .then(function(securityTokens) {
                            should.exist(securityTokens);
                            securityTokens.should.be.length(1);

                            done();
                        });
                })
                .fail(done);
        });

        it('should get updated access token when there is a valid one', function(done) {
            var token1, token2;

            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function (token) {
                            token1 = token;
                            return Q(user);
                        });
                })
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function(token) {
                            token2 = token;
                            return Q(user);
                        });
                })
                .then(function() {
                    should.exist(token1);
                    should.exist(token2);

                    token1.should.not.be.equal(token2);
                    token1.accessToken.should.not.be.equal(token2.accessToken);
                    token1.refreshToken.should.be.equal(token2.refreshToken);
                    moment(token1.expiredIn).isBefore(moment(token2.expiredIn)).should.ok;

                    done();
                })
                .fail(done);
        });
    });

    describe('when user trying refresh access token', function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount,
                database.models.SecurityUserToken
            ], done);
        });

        it('should throw an error with invalid refreshToken', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function() {
                            return authProvider.refreshBearerToken(user, "");
                        })
                        .then(function() {
                            done(new Error('For invalid refresh token error should be thrown'));
                        })
                        .fail(function(err) {
                            should.exist(err);
                            should(err).be.an.instanceOf(Error);

                            done();
                        })
                })
                .fail(done);
        });

        it('should get refreshed access token with updated expiration date', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function(token) {
                            return authProvider.refreshBearerToken(user, token.refreshToken)
                                .then(function(refreshedToken) {
                                    should.exist(refreshedToken);
                                    should(refreshedToken).be.an.Object;
                                    refreshedToken.should.have.property('accessToken');
                                    refreshedToken.should.have.property('refreshToken');
                                    refreshedToken.should.have.property('expiredIn');

                                    refreshedToken.accessToken.should.not.be.equal(token.accessToken);
                                    refreshedToken.refreshToken.should.be.equal(token.refreshToken);
                                    moment(token.expiredIn).isBefore(moment(refreshedToken.expiredIn)).should.be.ok;

                                    done();
                                });
                        });
                })
                .fail(done);
        });
    });

    describe('when user trying to authenticate with bearer token', function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount,
                database.models.SecurityUserToken
            ], done);
        });

        it('should throw error for invalid token', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function() {
                    return authProvider.authenticateWithBearerTokenStrategy("1234");
                })
                .then(function(response) {
                    done(new Error('Should be error for invalid token'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.instanceOf(Error);
                    done();
                });
        });

        it('should throw error for expired token', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function(token) {
                            return Q.nfcall(database.models.SecurityUserToken.find, {
                                accessToken: token.accessToken
                            })
                                .then(function(tokens) {
                                    should.exist(tokens);
                                    should(tokens).be.not.empty;

                                    var dbToken = _.first(tokens);
                                    var expiredToken = utils.token.generateAccessToken({ expired: moment().month(1).toDate() });
                                    dbToken.accessToken = expiredToken;
                                    return Q.nfcall(dbToken.save);
                                });
                        });

                })
                .then(function(expiredToken) {
                    return authProvider.authenticateWithBearerTokenStrategy(expiredToken.accessToken);
                })
                .then(function() {
                    done(new Error('Should be error for expired token'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.instanceOf(Error);
                    done();
                });
        });

        it('should success with valid token', function(done) {
            authProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function(token) {
                            return authProvider.authenticateWithBearerTokenStrategy(token.accessToken)
                        })
                        .then(function(authenticatedUser) {
                            should.exist(authenticatedUser);
                            should(authenticatedUser).be.an.Object;
                            authenticatedUser.id.should.be.equal(user.id);

                            done();
                        });
                })
                .fail(done);
        });
    });

    describe('when user trying to revoke bearer token', function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.UserAccount,
                database.models.SecurityUserToken
            ], done);
        });

        it('should throw error when no tokens exist for user', function(done) {
            createUser()
                .then(function(user) {
                    return authProvider.revokeBearerToken(user);
                })
                .then(function() {
                    done(new Error('Should be an error when revoke non-existed token'));
                })
                .fail(function(err) {
                    should.exist(err);
                    err.should.be.an.instanceOf(Error);

                    done();
                });
        });

        it('should return revoked token and db does not contains this token', function(done) {
            createUser()
                .then(function(user) {
                    return authProvider.generateBearerToken(user)
                        .then(function() {
                            return authProvider.revokeBearerToken(user);
                        })
                        .then(function(token) {
                            should.exist(token);
                            token.should.be.an.Object;
                            return Q.nfcall(database.models.SecurityUserToken.find)
                                .then(function(tokens) {
                                    should.exist(tokens);
                                    tokens.should.be.empty;

                                    done();
                                });
                        });
                })
                .fail(done);
        });
    })
});