/**
 * Created by alesanro on 3/4/15.
 */

var should = require("should");

describe("Merge Combinator", function() {
    "use strict";
    var app;
    var combinator;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            combinator = app.getService('responseCombinator');
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    it("should exist", function() {
        should.exist(combinator);
    });

    //TODO: add test-cases
});