/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    "./support/plugins/logger",
    "./support/plugins/proxy-response-combinator",
    "./support/plugins/external-service/proxy-service",
    "./support/plugins/external-service/tmdb/local",
    "./support/plugins/request",
    "./support/plugins/cache",
    './support/plugins/error-scope'
];