/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    './support/plugins/facades',
    "./support/plugins/database",
    "./support/plugins/error-scope",
    "./support/plugins/logger",
    './support/plugins/request',
    './support/plugins/cache',
    "./support/plugins/external-service/tmdb/global",
    "./support/plugins/core-models",
    "./support/plugins/collections-finder",
    "./support/plugins/collections-query"
];