/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    './support/plugins/logger',
    './support/plugins/error-scope'
];