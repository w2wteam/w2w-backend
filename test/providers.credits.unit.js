/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');

describe('Providers-Credit', function() {
    "use strict";
    var app;
    var creditProvider;
    var providers;
    var database;
    var databaseHelper;
    var userHelper;

    before(function (done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            providers = app.getService('dataProviders');
            creditProvider = providers.CreditProvider;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            userHelper = require('./support/user-helper')(providers, database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    function clearSyncDb(done) {
        databaseHelper.clearDBWithSyncOfModels([
            database.models.UserAccount,
            database.models.SecurityUserToken,
            database.models.Person,
            database.models.UserRelationsToPerson,
            database.models.Credit,
            database.models.UserRelationsToCredit,
            database.models.Movie,
            database.models.UserRelationsToMovie,
            database.models.Genre
        ], done);
    }

    function createCreditsFromMovies() {
        var searchQuery = 'matrix';

        return providers.MovieProvider.getSearchMovieWithQuery(searchQuery)
            .then(_.first)
            .then(function(movie) {
                return providers.MovieProvider.getMovieDetails(movie.id);
            })
            .then(function(movie) {
                return Q(movie.credits);
            });
    }

    function createCreditsFromPeople() {
        var searchQuery = 'brad pitt';

        return providers.PersonProvider.getSearchPersonWithQuery(searchQuery)
            .then(_.first)
            .then(function(person) {
                return providers.PersonProvider.getPersonDetails(person.id);
            })
            .then(function(person) {
                return Q(person.credits);
            });
    }

    describe("when get credit by id", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for non-existed credit', function(done) {
            creditProvider.getById(1)
                .then(function() {
                    done('Error for wrong non-existed credit');
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });

        it('should return single credit', function(done) {
            createCreditsFromMovies()
                .then(_.first)
                .then(function(credit) {
                    return creditProvider.getById(credit.id);
                })
                .then(function(credit) {
                    should.exist(credit);
                    should(credit).be.an.Object;
                    credit.should.not.have.property('like');

                    done();
                })
                .fail(done);
        });
    });

    describe("when populate collection of credits", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser().then(done);
        });

        it('should do nothing for empty collection', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return creditProvider.getCreditsPopulatedForUser(null, user.id);
                })
                .then(function(credits) {
                    should.not.exist(credits);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference them', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createCreditsFromMovies()
                        .then(function(credits) {
                            return creditProvider.getCreditsPopulatedForUser(credits, user.id);
                        });
                })
                .then(function(populatedCredits) {
                    should.exist(populatedCredits);
                    should(populatedCredits).be.an.Array;
                    populatedCredits.should.not.be.empty;

                    _.forEach(populatedCredits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                        credit.like.should.not.be.ok;
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences them', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createCreditsFromMovies()
                        .then(function(credits) {
                            var relations = _.map(credits, function(credit) {
                                return {
                                    credit_id: credit.id,
                                    user_id: user.id,
                                    like: true
                                };
                            });

                            return Q.nfcall(database.models.UserRelationsToCredit.create, relations)
                                .then(function() {
                                    return creditProvider.getCreditsPopulatedForUser(credits, user.id);
                                });
                        });
                })
                .then(function(populatedCredits) {
                    should.exist(populatedCredits);
                    should(populatedCredits).be.an.Array;
                    populatedCredits.should.not.be.empty;

                    _.forEach(populatedCredits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                        credit.like.should.be.ok;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get credits for movie", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for empty or undefined movie', function(done) {
            creditProvider.getCreditsForMovie(null)
                .then(function() {
                    done(new Error('Error for getting credits for undefined movie'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });

        it('should return collection of movie credits with short person info', function(done) {
            createCreditsFromMovies()
                .then(function() {
                    return providers.MovieProvider.getById(1);
                })
                .then(function(movie) {
                    should.exist(movie);
                    return creditProvider.getCreditsForMovie(movie);
                })
                .then(function(credits) {
                    should.exist(credits);
                    should(credits).be.an.Array;
                    credits.should.be.not.empty;

                    _.forEach(credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.not.have.property('like');
                        credit.should.have.properties([
                            'personName',
                            'personProfilePath'
                        ]);
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get credits for person", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for empty or undefined person', function(done) {
            creditProvider.getCreditsForPerson(null)
                .then(function() {
                    done(new Error('Error for getting credits for undefined person'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });

        it('should return collection of person credits', function(done) {
            createCreditsFromPeople()
                .then(function() {
                    return providers.PersonProvider.getById(1);
                })
                .then(function(person) {
                    return creditProvider.getCreditsForPerson(person);
                })
                .then(function(credits) {
                    should.exist(credits);
                    should(credits).be.an.Array;
                    credits.should.be.not.empty;

                    _.forEach(credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.not.have.property('like');
                        credit.should.have.properties([
                            'moviePosterPath'
                        ]);
                    });

                    done();
                })
                .fail(done);
        });
    });
});