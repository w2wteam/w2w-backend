/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');
var _ = require('lodash');

describe('API-User', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/user', function() {
        describe('/wantlist', function() {
            var path = '/user/wantlist';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it("should GET error for unauthorized user", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path)
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET paginated result of list with wanted to watch @Movie', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path)
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(200)
                                    .expect(function(res) {
                                        var body = res.body;
                                        should(body).not.be.an.Array;
                                        body.should.have.a.properties([
                                            'page',
                                            'results'
                                        ]);
                                        should(body.results).be.an.Array;
                                        _.forEach(body.results, shouldHelper.shouldHaveMovieBasicProperties);
                                    })
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should PUT error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .put(path)
                            .send({
                                'movie': 1
                            })
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should PUT error for invalid payload body", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .put(path)
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .send({
                                        'mov': 1
                                    })
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should PUT return well-formed result of applying user preference', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .put(path)
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .send({
                                                'movie': movie.id
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);

            });

            it("should DELETE error for unauthorized user", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .del(path)
                            .send({
                                'movie': 1
                            })
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should DELETE error for invalid payload body", function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .del(path)
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .send({
                                                'mov': movie.id
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should DELETE result well-formed updated state of user preferences', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .del(path)
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .send({
                                                movie: movie.id
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it("should DELETE tolerantly allow appying deleteng with the same parameters", function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .del(path)
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .send({
                                                movie: movie.id
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/watchedlist', function() {
            var path = '/user/watchedlist';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it("should GET error for unauthorized user", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path)
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET result of paginated list of @Movie', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path)
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(200)
                                    .expect(function(res) {
                                        var body = res.body;
                                        should(body).not.be.Array;
                                        body.should.have.properties([
                                            'results',
                                            'page'
                                        ]);
                                        should(body.results).be.an.Array;
                                        _.forEach(body.results, shouldHelper.shouldHaveMovieBasicProperties);
                                    })
                                    .end(done);
                            });
                    })
                    .fail(done);
            });
        });
    });
});