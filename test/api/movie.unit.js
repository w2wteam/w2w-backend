/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');
var _ = require('lodash');

describe('API-Movie', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/movie', function() {
        describe('/:id', function() {
            function path(id) {
                return require('util').format("/movie/%s", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .get(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should GET well-formed basic @Movie for valid id', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(movie.id))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should.exist(body);
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveMovieBasicProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/details', function() {
            function path(id) {
                return require('util').format("/movie/%s/details", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .get(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should GET well-formed advanced @Movie for valid id', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(movie.id))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should.exist(body);
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveMovieDetailsProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/like', function() {
            function path(id) {
                return require('util').format("/movie/%s/like", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .post(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST error for invalid payload body', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "lik": "like"
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST well-formed result of applying user preference', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "like": "like"
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                                body.like.should.be.equal('like');
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/want', function() {
            function path(id) {
                return require('util').format("/movie/%s/want", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .post(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST error for invalid payload body', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "wantToWatch": true
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST well-formed result of applying user preference', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "want_to_watch": true
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                                body['wantToWatch'].should.be.True;
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/notoffer', function() {
            function path(id) {
                return require('util').format("/movie/%s/notoffer", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .get(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST error for invalid payload body', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "notToOffer": 'yes'
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST well-formed result of applying user preference', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "not_to_offer": "yes"
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                                body['notToOffer'].should.be.equal('yes');
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/rate', function() {
            function path(id) {
                return require('util').format("/movie/%s/rate", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .post(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST error for invalid payload body', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "r": 9
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST well-formed result of applying user preference', function(done) {
                userHelper.getMovies()
                    .then(_.first)
                    .then(function(movie) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(movie.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "rate": 9
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToMovieProperties(body);
                                                body.rate.should.be.equal(9);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });
    });
});