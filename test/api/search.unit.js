/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var _ = require('lodash');
var request = require('supertest');

describe('API-Search', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/search', function() {
        describe('/movie', function() {
            function path(query) {
                return require('util').format('/search/movie?%s', query);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path(''))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should GET error for wrong query", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path("matrix"))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should GET paginated results of search', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path('q=matrix'))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(200)
                                    .expect(function(res) {
                                        var body = res.body;
                                        should(body).be.not.an.Array;
                                        body.should.have.a.properties([
                                            'page',
                                            'results'
                                        ]);
                                        should(body.results).be.an.Array;
                                        _.forEach(body.results, shouldHelper.shouldHaveMovieBasicProperties);
                                    })
                                    .end(done);

                            });
                    })
                    .fail(done);
            });
        });

        describe('/person', function() {
            function path(id) {
                return require('util').format('/search/person?%s', id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path(''))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should GET error for wrong query", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path("brad%20pitt"))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should GET paginated results of search', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path('q=brad%20pitt'))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .expect(200)
                                    .expect(function(res) {
                                        var body = res.body;
                                        should(body).be.not.an.Array;
                                        body.should.have.a.properties([
                                            'page',
                                            'results'
                                        ]);
                                        should(body.results).be.an.Array;
                                        _.forEach(body.results, shouldHelper.shouldHavePersonBasicProperties);
                                    })
                                    .end(done);

                            });
                    })
                    .fail(done);
            });
        });
    });
});