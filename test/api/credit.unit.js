/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');
var Q = require('q');
var _ = require('lodash');

describe('API-Credit', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/credit', function() {
        describe('/:id', function() {
            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            function path (id) {
                return require('util').format("/credit/%s", id);
            }

            it("should GET error for unauthenticated user", function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .get(path(1000))
                            .set('Authorization', "Bearer 11111")
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should GET error with wrong id", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path(1000))
                                    .set('Authorization', "Bearer " + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should GET credit for valid id', function(done) {
                userHelper.getCredits()
                    .then(_.first)
                    .then(function(credit) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(credit.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should.exist(body);
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveCreditProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/like', function() {
            function path (id) {
                return require('util').format("/credit/%s/like", id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST get error for unauthenticated user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .post(path(1000))
                            .send({
                                like: true
                            })
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST get error with wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', "Bearer " + token.accessToken)
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST get error for invalid payload body format', function(done) {
                userHelper.getCredits()
                    .then(_.first)
                    .then(function(credit) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(credit.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                lik: true
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST get well-formed updated @Credit with user preferences', function(done) {
                userHelper.getCredits()
                    .then(_.first)
                    .then(function(credit) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(credit.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                like: true
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should.exist(body);
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldUserToCreditHaveProperties(body);
                                                body.like.should.be.True;
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });
    });
});