/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    "../support/plugins/database",
    "../support/plugins/router",
    "../support/plugins/passport",
    "../support/plugins/error-scope",
    "../support/plugins/logger",
    "../support/plugins/app",
    "../support/plugins/oauth2",
    "../support/plugins/controllers",
    "../support/plugins/request-parameters-checker",
    "../support/plugins/data-providers",
    "../support/plugins/token",
    "../support/plugins/response-presenters",
    "../support/plugins/facades",
    "../support/plugins/external-service/tmdb/global",
    "../support/plugins/core-models",
    "../support/plugins/collections-finder",
    "../support/plugins/collections-query",
    "../support/plugins/cache",
    "../support/plugins/request"
];