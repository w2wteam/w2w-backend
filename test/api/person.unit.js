/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');
var _ = require('lodash');

describe('API-Person', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/person', function() {
        describe('/:id', function() {
            function path(id) {
                return require('util').format('/person/%s', id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET error for wrong id', function(done) {
                userHelper.getPeople()
                    .then(function(people) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(1000))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(400)
                                            .end(done);
                                    })
                            });
                    })
                    .fail(done);
            });

            it('should GET well-formed basic @Person for valid id', function(done) {
                userHelper.getPeople()
                    .then(_.first)
                    .then(function(person) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(person.id))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHavePersonBasicProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });

        describe('/id:/details', function() {
            function path(id) {
                return require('util').format('/person/%s/details', id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should GET error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        agent
                            .get(path(1000))
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should GET error for wrong id', function(done) {
                userHelper.getPeople()
                    .then(function(people) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(1000))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(400)
                                            .end(done);
                                    })
                            });
                    })
                    .fail(done);
            });

            it('should GET well-formed advanced @Person for valid id', function(done) {
                userHelper.getPeople()
                    .then(_.first)
                    .then(function(person) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .get(path(person.id))
                                            .set('Authorization', 'Bearer ' + token.accessToken)
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHavePersonDetailsProperties(body);
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            })
        });

        describe('/id:/favorite', function() {
            function path(id) {
                return require('util').format('/person/%s/favorite', id);
            }

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST error for unauthorized user', function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .post(path(1000))
                            .send({
                                favorite: true
                            })
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST error for wrong id', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .post(path(1000))
                                    .set('Authorization', 'Bearer ' + token.accessToken)
                                    .send({
                                        favorite: true
                                    })
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST error for invalid payload body', function(done) {
                userHelper.getPeople()
                    .then(_.first)
                    .then(function(person) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(person.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "fav": true
                                            })
                                            .expect(400)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });

            it('should POST well-formed result of applying user preference', function(done) {
                userHelper.getPeople()
                    .then(_.first)
                    .then(function(person) {
                        return userHelper.getUser()
                            .then(function(user) {
                                return userHelper.getBearerAccess(user)
                                    .then(function(token) {
                                        agent
                                            .post(path(person.id))
                                            .set('Authorization', "Bearer " + token.accessToken)
                                            .send({
                                                "favorite": true
                                            })
                                            .expect(200)
                                            .expect(function(res) {
                                                var body = res.body;
                                                should(body).not.be.an.Array;
                                                shouldHelper.shouldHaveUserToPersonProperties(body);
                                                body.favorite.should.be.True;
                                            })
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });
    });
});