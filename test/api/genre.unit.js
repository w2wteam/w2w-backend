/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');
var _ = require('lodash');

describe('API-Genre', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/genre', function() {
        describe('/list', function() {
            var path = '/genre/list';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            after(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it("should GET error for unauthenticated user", function(done) {
                userHelper.getUser()
                    .then(function() {
                        agent
                            .get(path)
                            .expect(401)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should GET list of genres labels", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return userHelper.getBearerAccess(user)
                            .then(function(token) {
                                agent
                                    .get(path)
                                    .set('Authorization', "Bearer " + token.accessToken)
                                    .expect(200)
                                    .expect(function(res) {
                                        var body = res.body;
                                        should.exist(body);
                                        should(body).be.an.Array;
                                        _.forEach(body, function(genre) {
                                            should(genre).be.a.String;
                                        });
                                    })
                                    .end(done);
                            });
                    })
                    .fail(done);
            });
        });
    });
});