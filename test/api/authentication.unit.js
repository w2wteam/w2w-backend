/**
 * Created by alesanro on 3/15/15.
 */

var should = require('should');
var request = require('supertest');

describe('API-Authentication', function() {
    "use strict";
    var app;
    var agent;
    var databaseHelper;
    var userHelper;
    var grantType = 'simple-token';
    var shouldHelper = require('../support/should-response-helper');

    before(function(done) {
        require('../support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            agent = request.agent(app.getService('app').express);
            databaseHelper = require('../support/database-helper')(app.getService('database'));
            userHelper = require('../support/user-helper')(app.getService('dataProviders'), app.getService('database'));
            done();
        });
    });

    after(function(done) {
        app.getService('app').server.close(function() {
            app.destroy();
            done();
        });
    });

    describe('#/oauth', function() {
        function passwordAuthForUser(user) {
            return {
                'grant_type': grantType,
                'client_id': user.nickname,
                'client_secret': userHelper.password
            };
        }

        describe('/register', function() {
            var path = '/oauth/register';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            it('should POST return error for invalid params (@nickname)', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'al',
                        email: 'email@email.ru',
                        password: '123wer'
                    })
                    .expect(400)
                    .end(done);
            });

            it('should POST return error for invalid params (@email)', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alex',
                        email: 'email',
                        password: '123qwe'
                    })
                    .expect(400)
                    .end(done);
            });

            it('should POST return error for invalid params (@password)', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alex',
                        email: 'email@email.ru',
                        password: ''
                    })
                    .expect(400)
                    .end(done);
            });

            it('should POST return error with 2 reasons for invalid @password and @nickname', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'al',
                        email: 'email@email.ru',
                        password: ''
                    })
                    .expect(400)
                    .expect(function(res) {
                        res.body.should.have.property('reasons');
                        res.body.reasons.should.have.length(2);
                    })
                    .end(done);
            });

            it('should POST return well-formed new user for valid params', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alex',
                        email: 'email@email.ru',
                        password: '123qwe'
                    })
                    .expect(201)
                    .expect(function(res) {
                        var body = res.body;
                        should.exist(body);
                        should(body).not.be.an.Array;
                        shouldHelper.shouldHaveUserRegisterProperties(body);
                    })
                    .end(done);
            });

            it('should POST return error for registering already created user', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alex',
                        email: 'email@email.ru',
                        password: '123qwe'
                    })
                    .expect(400)
                    .end(done);
            });

            it('should POST return error for already existed @email', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alesanro',
                        email: 'email@email.ru',
                        password: '123qwe'
                    })
                    .expect(400)
                    .end(done);
            });

            it('should POST return error for already existed @nickname', function(done) {
                agent
                    .post(path)
                    .send({
                        nickname: 'alex',
                        email: 'alex@email.ru',
                        password: '123qwe'
                    })
                    .expect(400)
                    .end(done);
            });
        });

        describe('/token', function() {
            var path = '/oauth/token';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            afterEach(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it('should POST return error for invalid credentials (@nickname)', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post(path)
                            .send({
                                'grant_type': grantType,
                                'client_id': 't',
                                'client_secret': userHelper.password
                            })
                            .expect(400)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST return error for invalid credentials (@email)', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post(path)
                            .send({
                                'grant_type': grantType,
                                'client_id': 'al@email.ru',
                                'client_secret': userHelper.password
                            })
                            .expect(400)
                            .end(done);
                    })
                    .fail(done);
            });

            it('should POST return error for invalid credentials (@password)', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post(path)
                            .send({
                                'grant_type': grantType,
                                'client_id': user.nickname,
                                'client_secret': ''
                            })
                            .expect(400)
                            .end(done);
                    })
                    .fail(done);
            });

            it("should POST return bearer token for valid credentials (@nickname)", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post(path)
                            .send(passwordAuthForUser(user))
                            .expect(200)
                            .expect(function(res) {
                                var body = res.body;
                                should.exist(body);
                                should(body).not.be.an.Array;
                                shouldHelper.shouldHaveAccessTokenProperties(body);
                            })
                            .end(done);
                    })
                    .fail(done);
            });

            it("should POST return bearer token for valid credentials (@email)", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post(path)
                            .send(passwordAuthForUser(user))
                            .expect(200)
                            .expect(function(res) {
                                var body = res.body;
                                should.exist(body);
                                should(body).not.be.an.Array;
                                shouldHelper.shouldHaveAccessTokenProperties(body);
                            })
                            .end(done);
                    })
                    .fail(done);
            });
        });

        describe('/refreshtoken', function() {
            var path = '/oauth/refreshtoken';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            afterEach(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it("should POST return error for invalid refreshToken field", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post('/oauth/token')
                            .send(passwordAuthForUser(user))
                            .end(function(err, res) {
                                var accessToken = res.body.access_token;

                                agent
                                    .post(path)
                                    .set('Authorization', 'Bearer ' + accessToken)
                                    .send({
                                        'grant_type': 'refresh_token',
                                        'refresh_token': ''
                                    })
                                    .expect(400)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST return updated bearer token',function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post('/oauth/token')
                            .send(passwordAuthForUser(user))
                            .expect(200)
                            .end(function(err, res) {
                                var accessToken = res.body.access_token;
                                var refreshToken = res.body.refresh_token;

                                agent
                                    .post(path)
                                    .set('Authorization', 'Bearer ' + accessToken)
                                    .send({
                                        'grant_type': 'refresh_token',
                                        'refresh_token': refreshToken
                                    })

                                    .expect(function(res) {
                                        var body = res.body;
                                        should.exist(body);
                                        should(body).not.be.an.Array;
                                        shouldHelper.shouldHaveAccessTokenProperties(body);
                                        body.access_token.should.not.be.equal(accessToken);
                                    })
                                    .expect(200)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });
        });

        describe('/revoketoken', function() {
            var path = '/oauth/revoketoken';

            before(function(done) {
                databaseHelper.clearDB(done);
            });

            afterEach(function(done) {
                userHelper.removeUser()
                    .then(done);
            });

            it("should POST get auth error with invalid bearer", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post('/oauth/token')
                            .send(passwordAuthForUser(user))
                            .end(function(err, res) {
                                agent
                                    .post(path)
                                    .set('Authorization', 'Bearer ' + '11111')
                                    .expect(401)
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it("should POST get empty result for success revoking", function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post('/oauth/token')
                            .send(passwordAuthForUser(user))
                            .end(function(err, res) {
                                var body = res.body;
                                var accessToken = body.access_token;

                                agent
                                    .post(path)
                                    .set('Authorization', 'Bearer ' + accessToken)
                                    .expect(204)
                                    .expect(function(res) {
                                        should(res.body).be.empty;
                                    })
                                    .end(done);
                            });
                    })
                    .fail(done);
            });

            it('should POST get error for access with revoked token', function(done) {
                userHelper.getUser()
                    .then(function(user) {
                        return agent
                            .post('/oauth/token')
                            .send(passwordAuthForUser(user))
                            .end(function(err, res) {
                                var body = res.body;
                                var accessToken = body.access_token;

                                agent
                                    .post(path)
                                    .set('Authorization', 'Bearer ' + accessToken)
                                    .expect(204)
                                    .end(function() {
                                        agent
                                            .post(path)
                                            .set('Authorization', 'Bearer ' + accessToken)
                                            .expect(401)
                                            .end(done);
                                    });
                            });
                    })
                    .fail(done);
            });
        });
    });
});