/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');

describe("Collections Model Finder", function() {
    var app;
    var finder;
    var servicesNames = ["tmdb"];
    var collection = [
        {
            tmdbId: 12,
            imdbId: "sd4j66",
            rottenTomatoId: 2311
        },
        {
            tmdbId: 345,
            imdbId: "3956g",
            rottenTomatoId: 488
        },
        {
            tmdbId: 234,
            imdbId: "234x2",
            rottenTomatoId: 890
        },
        {
            tmdbId: 2346,
            imdbId: "g356j",
            rottenTomatoId: 111
        },
        {
            tmdbId: 679,
            imdbId: "ad3ha1",
            rottenTomatoId: 3589
        },
        {
            tmdbId: 345,
            imdbId: "3956g",
            rottenTomatoId: 488
        }
    ];

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app =  application;
            finder = app.getService('externalLinksFinder');
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    it("service should exist", function() {
        should.exists(finder);
    });

    describe("for 'find' method", function() {
        var externalLinkFinder;
        beforeEach(function() {
            externalLinkFinder = new finder.ExternalLinksCollectionFinder(servicesNames);
        });

        it("should return first object if it was found", function() {
            var result = externalLinkFinder.find(collection, { tmdbId: 234 });
            should.exist(result);
            result.tmdbId.should.be.equal(234);
        });

        it("should return single object with multiple collection entries", function() {
            var result = externalLinkFinder.find(collection, { tmdbId: 345 });
            should.exist(result);
            should(result).be.Object;
            result.tmdbId.should.be.equal(345);
        });

        it("should return undefined if no object was found", function() {
            var result = externalLinkFinder.find(collection, { tmdbId: 0 });
            should(result).be.undefined;
        });

        it("should return undefined for empty collection", function() {
            var result = externalLinkFinder.find(null, { tmdb: 234 });
            should(result).be.undefined;
        });

        it("should return undefined for null source object", function() {
            var result = externalLinkFinder.find(collection, null);
            should(result).be.undefined;
        });
    });

    describe("for 'findWithModel' method", function() {
        var externalLinkFinder;
        var existedModel = {
            tmdbId: 23,
            tmdb_person_id: 345,
            tmdb_movie_id: 679
        };
        var nonExistedModel = {
            tmdbId: 12,
            tmdb_person_id: 66,
            tmdb_movie_id: 87
        };
        beforeEach(function() {
            externalLinkFinder = new finder.ExternalLinksCollectionFinder(servicesNames);
        });

        it("should return first object if it was found", function() {
            var result = externalLinkFinder.findWithModel(collection, existedModel, "Person");
            should.exist(result);
            result.tmdbId.should.be.equal(existedModel.tmdb_person_id);

            var result2 = externalLinkFinder.findWithModel(collection, existedModel, "Movie");
            should.exist(result2);
            result2.tmdbId.should.be.equal(existedModel.tmdb_movie_id);
        });

        it('should return single object if it was found', function() {
            var result = externalLinkFinder.findWithModel(collection, existedModel, "Person");
            should.exist(result);
            should(result).be.Object;
        });

        it('should return undefined for not found case', function() {
            var result = externalLinkFinder.findWithModel(collection, nonExistedModel, "Person");
            should(result).be.undefined;
        });

        it('should return undefined for empty model or person or all together', function() {
            var result = externalLinkFinder.findWithModel(collection, null, "Person");
            should(result).be.undefined;
            result = externalLinkFinder.findWithModel(collection, existedModel, "");
            should(result).be.undefined;
            result = externalLinkFinder.findWithModel(collection, null, "");
            should(result).be.undefined;
        });

        it('should return undefined for empty or null collection', function() {
            var result = externalLinkFinder.findWithModel(null, existedModel, "Person");
            should(result).be.undefined;
        });
    });

    describe("for 'unique' method", function() {
        var externalLinkFinder;
        var uniqueCollection = [
            {
                tmdbId: 22,
                imdbId: 11,
                rottenTomatoId: 12
            },
            {
                tmdbId: 33,
                imdbId: 22,
                rottenTomatoId: 14
            },
            {
                tmdbId: 44,
                imdbId: 33,
                rottenTomatoId: 15
            }
        ];
        beforeEach(function() {
            externalLinkFinder = new finder.ExternalLinksCollectionFinder(servicesNames);
        });

        it('should return the same collection for normalized collection', function() {
            var result = externalLinkFinder.unique(uniqueCollection);
            should.exist(result);
            should(result).be.Array;
            result.length.should.be.equal(uniqueCollection.length);
        });

        it('should return cut collection for non-unique collection', function() {
            var result = externalLinkFinder.unique(collection);
            should.exist(result);
            should(result).be.Array;
            result.length.should.be.equal(collection.length - 1);
        });
    });
});