/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');

describe('Providers-Movies', function() {
    "use strict";
    var app;
    var movieProvider;
    var database;
    var databaseHelper;
    var userHelper;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            movieProvider = app.getService('dataProviders').MovieProvider;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            userHelper = require('./support/user-helper')(app.getService('dataProviders'), database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    function clearSyncDb(done) {
        databaseHelper.clearDBWithSyncOfModels([
            database.models.UserAccount,
            database.models.SecurityUserToken,
            database.models.Person,
            database.models.UserRelationsToPerson,
            database.models.Credit,
            database.models.UserRelationsToCredit,
            database.models.Movie,
            database.models.UserRelationsToMovie,
            database.models.Genre
        ], done);
    }


    describe("when get movie by id", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for non-existed movie', function(done) {
            movieProvider.getById(1)
                .then(function() {
                    done(new Error('Error for non-existed movie request'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });

        it('should return single movie with basic info', function(done) {
            var searchQuery = 'matrix';

            movieProvider.getSearchMovieWithQuery(searchQuery)
                .then(_.first)
                .then(function(movie) {
                    return movieProvider.getById(movie.id);
                })
                .then(function(movie) {
                    should.exist(movie);
                    should(movie).be.an.Object;
                    movie.should.not.have.property('credits');

                    done();
                })
                .fail(done);
        });
    });

    describe("when populate movie", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser().then(done);
        });

        it('should do nothing for empty undefined movie', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getMoviePopulatedForUser(null, user.id);
                })
                .then(function(movie) {
                    should.not.exist(movie);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(movie) {
                            return movieProvider.getMoviePopulatedForUser(movie, user.id);
                        })
                        .then(function(populatedMovie) {
                            should.exist(populatedMovie);
                            should(populatedMovie).be.an.Object;
                            populatedMovie.should.not.have.property('credits');
                            populatedMovie.should.have.property('wantToWatch');
                            populatedMovie.wantToWatch.should.be.not.ok;
                            populatedMovie.should.have.property('rate');
                            populatedMovie.rate.should.be.equal(0);

                            done();
                        });
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(movie) {
                            return Q.nfcall(database.models.UserRelationsToMovie.create, {
                                movie_id: movie.id,
                                user_id: user.id,
                                wantToWatch: true,
                                rate: 7
                            })
                                .then(function() {
                                    return movieProvider.getMoviePopulatedForUser(movie, user.id);
                                });
                        })
                        .then(function(populatedMovie) {
                            should.exist(populatedMovie);
                            should(populatedMovie).be.an.Object;
                            populatedMovie.should.not.have.property('credits');
                            populatedMovie.should.have.property('wantToWatch');
                            populatedMovie.wantToWatch.should.be.ok;
                            populatedMovie.should.have.property('rate');
                            populatedMovie.rate.should.be.equal(7);

                            done();
                        });
                })
                .fail(done);
        });
    });

    describe("when populate collection of movies", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser().then(done);
        });

        it('should do nothing for empty collection', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getMoviesPopulatedForUser(null, user.id);
                })
                .then(function(movies) {
                    should.not.exist(movies);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(function(movies) {
                            return movieProvider.getMoviesPopulatedForUser(movies, user.id);
                        });
                })
                .then(function(populatedMovies) {
                    should.exist(populatedMovies);
                    should(populatedMovies).be.an.Array;
                    populatedMovies.should.not.be.empty;

                    _.forEach(populatedMovies, function(movie) {
                        should(movie).be.an.Object;
                        movie.should.not.have.property('credits');
                        movie.should.have.property('wantToWatch');
                        movie.wantToWatch.should.be.not.ok;
                        movie.should.have.property('rate');
                        movie.rate.should.be.equal(0);
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(function(movies) {
                            var relations = _.map(movies, function(movie) {
                                return {
                                    movie_id: movie.id,
                                    user_id: user.id,
                                    wantToWatch: true,
                                    rate: 7
                                };
                            });

                            return Q.nfcall(database.models.UserRelationsToMovie.create, relations)
                                .then(function() {
                                    return movieProvider.getMoviesPopulatedForUser(movies, user.id);
                                });
                        })
                })
                .then(function(populatedMovies) {
                    should.exist(populatedMovies);
                    should(populatedMovies).be.an.Array;
                    populatedMovies.should.not.be.empty;

                    _.forEach(populatedMovies, function(movie) {
                        should(movie).be.an.Object;
                        movie.should.not.have.property('credits');
                        movie.should.have.property('wantToWatch');
                        movie.wantToWatch.should.be.ok;
                        movie.should.have.property('rate');
                        movie.rate.should.be.equal(7);
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get genres", function() {
        beforeEach(function(done) {
            databaseHelper.clearDBWithSyncOfModels([
                database.models.Movie,
                database.models.Genre
            ], done);
        });

        it('should contain collection of genres labels', function(done) {
            movieProvider.getGenresLabels()
                .then(function(genres) {
                    should.exist(genres);
                    should(genres).be.an.Array;
                    genres.should.not.be.empty;

                    _.forEach(genres, function(genre) {
                        should(genre).be.a.String;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get movie details", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for non-existed movie', function(done) {
            movieProvider.getMovieDetails(1)
                .then(function() {
                    done(new Error('Error for wrong non-existed movie request'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });

        it('should add advanced info to basic movie (credits)', function(done) {
            var searchQuery = 'matrix';

            movieProvider.getSearchMovieWithQuery(searchQuery)
                .then(_.first)
                .then(function(movie) {
                    return movieProvider.getMovieDetails(movie.id);
                })
                .then(function(movie) {
                    should.exist(movie);
                    should(movie).be.an.Object;
                    movie.should.have.property('credits');
                    movie.credits.should.not.be.empty;

                    _.forEach(movie.credits, function(credit) {
                        should(credit).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get populated movie details", function() {
        var searchQuery = 'matrix';

        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser().then(done);
        });

        it('should do nothing for empty or undefined movie', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getMovieDetailsPopulatedForUser(null, user.id);
                })
                .then(function(movie) {
                    should.not.exist(movie);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(movie) {
                            return movieProvider.getMovieDetails(movie.id);
                        })
                        .then(function(movie) {
                            return movieProvider.getMovieDetailsPopulatedForUser(movie, user.id);
                        });
                })
                .then(function(populatedMovie) {
                    should.exist(populatedMovie);
                    should(populatedMovie).be.an.Object;
                    populatedMovie.should.have.property('wantToWatch');
                    populatedMovie.wantToWatch.should.not.be.ok;
                    populatedMovie.should.have.property('rate');
                    populatedMovie.rate.should.be.equal(0);
                    populatedMovie.should.have.property('credits');
                    should(populatedMovie.credits).be.an.Array;
                    populatedMovie.credits.should.not.be.empty;

                    _.forEach(populatedMovie.credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                        credit.like.should.not.be.ok;
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(_.first)
                        .then(function(movie) {
                            return movieProvider.getMovieDetails(movie.id);
                        })
                        .then(function(movie) {
                            return Q.nfcall(database.models.UserRelationsToMovie.create, {
                                movie_id: movie.id,
                                user_id: user.id,
                                wantToWatch: true,
                                rate: 7
                            })
                                .then(function() {
                                    var relations = _.map(movie.credits, function(credit) {
                                        return {
                                            credit_id: credit.id,
                                            user_id: user.id,
                                            like: true
                                        };
                                    });

                                    return Q.nfcall(database.models.UserRelationsToCredit.create, relations);
                                })
                                .then(function() {
                                    return movieProvider.getMovieDetailsPopulatedForUser(movie, user.id);
                                });

                        });
                })
                .then(function(populatedMovie) {
                    should.exist(populatedMovie);
                    should(populatedMovie).be.an.Object;
                    populatedMovie.should.have.property('wantToWatch');
                    populatedMovie.wantToWatch.should.be.ok;
                    populatedMovie.should.have.property('rate');
                    populatedMovie.rate.should.be.equal(7);
                    populatedMovie.should.have.property('credits');
                    should(populatedMovie.credits).be.an.Array;
                    populatedMovie.credits.should.not.be.empty;

                    _.forEach(populatedMovie.credits, function(credit) {
                        should(credit).be.an.Object;
                        credit.should.have.property('like');
                        credit.like.should.be.ok;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get found movies by search", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        it('should throw error for wrong-formed query');

        it('should return collection of results', function(done) {
            var searchQuery = 'matrix';

            movieProvider.getSearchMovieWithQuery(searchQuery)
                .then(function(movies) {
                    should.exist(movies);
                    should(movies).be.an.Array;
                    movies.should.not.be.empty;

                    _.forEach(movies, function(movie) {
                        should(movie).be.an.Object;
                        movie.should.not.have.property('credits');
                        movie.should.not.have.property('wantToWatch');
                        movie.should.not.have.property('rate');
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get populated movies found by search", function() {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser().then(done);
        });

        it('should do nothing for empty collection', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMoviesPopulatedForUser(null, user.id);
                })
                .then(function(movies) {
                    should.not.exist(movies);

                    done();
                })
                .fail(done);
        });

        it('should populate with null if user did not preference it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(function(movies) {
                            return movieProvider.getSearchMoviesPopulatedForUser(movies, user.id);
                        });
                })
                .then(function(populatedMovies) {
                    should.exist(populatedMovies);
                    should(populatedMovies).be.an.Array;
                    populatedMovies.should.not.be.empty;

                    _.forEach(populatedMovies, function(movie) {
                        should(movie).be.an.Object;
                        movie.should.not.have.property('credits');
                        movie.should.have.property('wantToWatch');
                        movie.wantToWatch.should.be.not.ok;
                        movie.should.have.property('rate');
                        movie.rate.should.be.equal(0);
                    });

                    done();
                })
                .fail(done);
        });

        it('should populate with non-null if user preferences it', function(done) {
            var searchQuery = 'matrix';

            userHelper.getUser()
                .then(function(user) {
                    return movieProvider.getSearchMovieWithQuery(searchQuery)
                        .then(function(movies) {
                            var relations = _.map(movies, function(movie) {
                                return {
                                    movie_id: movie.id,
                                    user_id: user.id,
                                    wantToWatch: true,
                                    rate: 7
                                };
                            });

                            return Q.nfcall(database.models.UserRelationsToMovie.create, relations)
                                .then(function() {
                                    return movieProvider.getSearchMoviesPopulatedForUser(movies, user.id);
                                });
                        })
                })
                .then(function(populatedMovies) {
                    should.exist(populatedMovies);
                    should(populatedMovies).be.an.Array;
                    populatedMovies.should.not.be.empty;

                    _.forEach(populatedMovies, function(movie) {
                        should(movie).be.an.Object;
                        movie.should.not.have.property('credits');
                        movie.should.have.property('wantToWatch');
                        movie.wantToWatch.should.be.ok;
                        movie.should.have.property('rate');
                        movie.rate.should.be.equal(7);
                    });

                    done();
                })
                .fail(done);
        });
    });
});