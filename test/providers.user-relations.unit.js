/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');

describe('Providers-User-Relations', function() {
    "use strict";
    var app;
    var userRelationsProvider;
    var providers;
    var database;
    var databaseHelper;
    var userHelper;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            providers = app.getService('dataProviders');
            userRelationsProvider = providers.UserRelationsProvider;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            userHelper = require('./support/user-helper')(providers, database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    function clearSyncDb(done) {
        databaseHelper.clearDB(done);
    }

    function createBunchOfMovies() {
        var searchQuery = 'matrix';

        return providers.MovieProvider.getSearchMovieWithQuery(searchQuery);
    }

    function createBunchOfPeople() {
        var searchQuery = 'brad pitt';

        return providers.PersonProvider.getSearchPersonWithQuery(searchQuery);
    }

    function createBunchOfCredits() {
        return Q.all([createBunchOfMovies(), createBunchOfPeople()])
            .spread(function(movies, people) {
                return Q.all([
                    providers.MovieProvider.getMovieDetails(_.result(_.first(movies), 'id')),
                    providers.PersonProvider.getPersonDetails(_.result(_.first(people), 'id'))
                ]);
            })
            .spread(function(movie, person) {
                return _.flatten([movie.credits, person.credits]);
            });
    }

    describe("when update relations to movie", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should update one property', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(_.first)
                        .then(function(movie) {
                            return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                'rate': 10
                            });
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('rate');
                    relation.rate.should.be.equal(10);
                    relation.should.have.property('wantToWatch');
                    relation.wantToWatch.should.not.be.ok;
                    relation.should.have.property('notToOffer');
                    relation.notToOffer.should.be.equal('unknown');

                    done();
                })
                .fail(done);
        });

        it('should update two properties', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(_.first)
                        .then(function(movie) {
                            return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                'rate': 10,
                                'wantToWatch': true
                            });
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('rate');
                    relation.rate.should.be.equal(10);
                    relation.should.have.property('wantToWatch');
                    relation.wantToWatch.should.be.ok;
                    relation.should.have.property('notToOffer');
                    relation.notToOffer.should.be.equal('unknown');

                    done();
                })
                .fail(done);
        });

        it('should update none properties', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(_.first)
                        .then(function(movie) {
                            return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, null);
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('rate');
                    relation.rate.should.be.equal(0);
                    relation.should.have.property('wantToWatch');
                    relation.wantToWatch.should.not.be.ok;
                    relation.should.have.property('notToOffer');
                    relation.notToOffer.should.be.equal('unknown');

                    done();
                })
                .fail(done);
        });
    });

    describe("when get marked movies", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should return empty array if user did not make any preference', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function() {
                            return userRelationsProvider.getMarkedMovies(user.id);
                        });
                })
                .then(function(movies) {
                    should.exist(movies);
                    movies.should.be.empty;

                    done();
                })
                .fail(done);
        });

        it('should return one movie if user did make one preference', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(_.first)
                        .then(function(movie) {
                            return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                rate: 10,
                                wantToWatch: true
                            })
                                .then(function() {
                                    return userRelationsProvider.getMarkedMovies(user.id);
                                });
                        });
                })
                .then(function(markedMovies) {
                    should.exist(markedMovies);
                    should(markedMovies).be.an.Array;
                    markedMovies.should.not.be.empty;
                    markedMovies.should.be.length(1);

                    _.forEach(markedMovies, function(movie) {
                        should(movie).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });

        it('should return collection of movies if user did make many preferences', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function(movies) {
                            return Q(_.take(movies, 3));
                        })
                        .then(function(movies) {
                            var relationsPromises = _.map(movies, function(movie) {
                                return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                    rate: 10
                                });
                            });

                            return Q.all(relationsPromises);
                        })
                        .then(function() {
                            return userRelationsProvider.getMarkedMovies(user.id);
                        });
                })
                .then(function(markedMovies) {
                    should.exist(markedMovies);
                    should(markedMovies).be.an.Array;
                    markedMovies.should.not.be.empty;
                    markedMovies.should.be.length(3);

                    _.forEach(markedMovies, function(movie) {
                        should(movie).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get want movies", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should return empty array if no such movies', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function() {
                            return userRelationsProvider.getWantMovies(user.id);
                        });
                })
                .then(function(movies) {
                    should.exist(movies);
                    movies.should.be.empty;

                    done();
                })
                .fail(done);
        });

        it('should return movies where `want-to-watch` property is `true`', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function(movies) {
                            return _.take(movies, 2);
                        })
                        .then(function(movies) {
                            var relations = _.map(movies, function(movie) {
                                return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                    wantToWatch: true
                                });
                            });

                            return Q.all(relations);
                        })
                        .then(function() {
                            return userRelationsProvider.getWantMovies(user.id);
                        });
                })
                .then(function(movies) {
                    should.exist(movies);
                    should(movies).be.an.Array;
                    movies.should.not.be.empty;
                    movies.should.be.length(2);

                    _.forEach(movies, function(movie) {
                        should(movie).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when get watched movies", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should return empty array if no such movies', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function() {
                            return userRelationsProvider.getWatchedMovies(user.id);
                        });
                })
                .then(function(movies) {
                    should.exist(movies);
                    movies.should.be.empty;

                    done();
                })
                .fail(done);
        });

        it('should return movies if movie was liked or rated', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfMovies()
                        .then(function(movies) {
                            return _.take(movies, 4);
                        })
                        .then(function(movies) {
                            var relations1 = _.map(_.slice(movies, 0, movies.length/2), function(movie) {
                                return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                    like: 'like'
                                });
                            });

                            var relations2 = _.map(_.slice(movies, movies.length/2), function(movie) {
                                return userRelationsProvider.updateRelationsToMovie(user.id, movie.id, {
                                    rate: 10
                                });
                            });

                            return Q.all(_.flatten([relations1, relations2]));
                        })
                        .then(function() {
                            return userRelationsProvider.getWatchedMovies(user.id);
                        });
                })
                .then(function(movies) {
                    should.exist(movies);
                    should(movies).be.an.Array;
                    movies.should.not.be.empty;
                    movies.should.be.length(4);

                    _.forEach(movies, function(movie) {
                        should(movie).be.an.Object;
                    });

                    done();
                })
                .fail(done);
        });
    });

    describe("when update relations to person", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should update one property', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfPeople()
                        .then(_.first)
                        .then(function(person) {
                            return userRelationsProvider.updateRelationsToPerson(user.id, person.id, {
                                'favorite': true
                            });
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('favorite');
                    relation.favorite.should.be.ok;

                    done();
                })
                .fail(done);
        });

        it('should update two properties (for now person has exactly one property so...you know..)');

        it('should update none properties', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfPeople()
                        .then(_.first)
                        .then(function(person) {
                            return userRelationsProvider.updateRelationsToPerson(user.id, person.id, null);
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('favorite');
                    relation.favorite.should.not.be.ok;

                    done();
                })
                .fail(done);
        });
    });

    describe("when update relations to credit", function () {
        beforeEach(function(done) {
            clearSyncDb(done);
        });

        afterEach(function(done) {
            userHelper.removeUser()
                .then(done);
        });

        it('should update one property', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfCredits()
                        .then(_.first)
                        .then(function(credit) {
                            return userRelationsProvider.updateRelationsToCredit(user.id, credit.id, {
                                like: true
                            });
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('like');
                    relation.like.should.be.ok;

                    done();
                })
                .fail(done);
        });

        it('should update two properties (for now credit has exactly one property so...you know..)');

        it('should update none properties', function(done) {
            userHelper.getUser()
                .then(function(user) {
                    return createBunchOfCredits()
                        .then(_.first)
                        .then(function(credit) {
                            return userRelationsProvider.updateRelationsToCredit(user.id, credit.id, null);
                        });
                })
                .then(function(relation) {
                    should.exist(relation);
                    should(relation).be.an.Object;
                    relation.should.have.property('like');
                    relation.like.should.not.be.ok;

                    done();
                })
                .fail(done);
        });
    });
});