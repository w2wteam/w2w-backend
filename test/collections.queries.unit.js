/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');

describe("Collection Query Maker", function() {
    var queryMaker;
    var servicesNames = ["tmdb"];
    var app;
    var collection = [
        {
            tmdbId: 23,
            imdbId: 23,
            rottenTomatoId: 23
        },
        {
            tmdbId: 34,
            imdbId: 34,
            rottenTomatoId: 34
        },
        {
            tmdbId: 567,
            imdbId: 567,
            rottenTomatoId: 567
        },
        {
            tmdbId: 333,
            imdbId: 333,
            rottenTomatoId: 333
        }
    ];

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            queryMaker = app.getService('databaseQueryMakers');
            done()
        });
    });

    after(function() {
        app.destroy();
    });

    it("service should exist", function() {
        should.exists(queryMaker);
    });

    describe('when make AND', function() {
        var databaseQueryMaker;

        beforeEach(function() {
            databaseQueryMaker = new queryMaker.DatabaseQueryMaker(servicesNames);
        });

        it("should return object with collection of ids", function() {
            var result = databaseQueryMaker.makeFindAnd(collection);
            should.exist(result);
            should(result).be.Object;
            _.forEach(result, function(value) {
                should(value).be.Array;
                _.forEach(value, function(item) {
                    should(item).be.Number;
                });
            });
        });
    });

    describe('when make OR', function() {
        var databaseQueryMaker;

        beforeEach(function() {
            databaseQueryMaker = new queryMaker.DatabaseQueryMaker(servicesNames);
        });

        it("should return object with collection of ids", function() {
            var result = databaseQueryMaker.makeFindAnd(collection);
            should.exist(result);
            should(result).be.Object;
            _.forEach(result, function(value) {
                should(value).be.Array;
                _.forEach(value, function(item) {
                    should(item).be.Number;
                });
            });
        });
    });
});