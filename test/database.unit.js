/**
 * Created by alesanro on 3/3/15.
 */

var should = require('should');

describe("Database", function() {
    var app;
    var database;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            database = app.getService('database');
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    it("should exist", function() {
        should.exist(database);
    });
});
