/**
 * Created by alesanro on 3/4/15.
 */

var _ = require('lodash');
var Q = require('q');
var should = require('should');

describe("Facade-Credit", function() {
    "use strict";
    var app;
    var facadeCredits;
    var facadeMovies;
    var facadePeople;
    var database;
    var databaseHelper;

    before(function(done){
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            facadeCredits = app.getService('moviesFacade').Credits;
            facadeMovies = app.getService('moviesFacade').Movies;
            facadePeople = app.getService('moviesFacade').People;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    it('should exist', function() {
        should.exist(facadeCredits);
    });

    describe('when get credits for person', function() {
        var searchQuery = 'brad pitt';
        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should save it in database', function(done) {
            facadePeople.searchPersonWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    searchResults.should.not.be.empty;

                    var person = _.first(searchResults);
                    return facadeCredits.getCreditsForPerson(person)
                })
                .then(function(credits) {
                    should.exist(credits);
                    should(credits).be.an.Array;
                    credits.should.not.be.empty;
                    done();
                })
                .fail(done);
        });

        it('should throw error for null person', function() {
            facadeCredits.getCreditsForPerson(undefined).should.be.rejectedWith(Error);
        });
    });

    describe('when get credits for movie', function() {
        var searchQuery = 'matrix';

        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should save it in database', function(done) {
            facadeMovies.searchMovieWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    should(searchResults).not.be.empty;

                    var movie = _.first(searchResults);
                    return facadeCredits.getCreditsForMovie(movie)
                })
                .then(function(credits) {
                    should.exist(credits);
                    should(credits).be.an.Array;
                    credits.should.not.be.empty;

                    done();
                })
                .fail(done);
        });

        it('should throw error for null movie', function() {
            facadeCredits.getCreditsForMovie(undefined).should.be.rejectedWith(Error);
        });
    })
});