/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    "./support/plugins/database",
    "./support/plugins/response-presenters",
    "./support/plugins/facades",
    "./support/plugins/logger",
    "./support/plugins/error-scope",
    "./support/plugins/external-service/tmdb/global",
    "./support/plugins/request",
    "./support/plugins/cache",
    "./support/plugins/core-models",
    "./support/plugins/collections-finder",
    "./support/plugins/collections-query",
    './support/plugins/data-providers'
];