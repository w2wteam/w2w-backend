/**
 * Created by alesanro on 3/21/15.
 */

var should = require('should');
var _ = require('lodash');

module.exports = {
    shouldHaveUserRegisterProperties: shouldHaveUserRegisterProperties,
    shouldHaveAccessTokenProperties: shouldHaveAccessTokenProperties,
    shouldHaveCreditProperties: shouldHaveCreditProperties,
    shouldHaveCreditPropertiesWithShortPersonInfo: shouldHaveCreditPropertiesWithShortPersonInfo,
    shouldHaveCreditPropertiesWithShortMovieInfo: shouldHaveCreditPropertiesWithShortMovieInfo,
    shouldUserToCreditHaveProperties: shouldUserToCreditHaveProperties,
    shouldHaveMovieBasicProperties: shouldHaveMovieBasicProperties,
    shouldHaveSearchMovieBasicProperties: shouldHaveSearchMovieBasicProperties,
    shouldHaveUserToMovieProperties: shouldHaveUserToMovieProperties,
    shouldHaveMovieDetailsProperties: shouldHaveMovieDetailsProperties,
    shouldHavePersonBasicProperties: shouldHavePersonBasicProperties,
    shouldHaveSearchPersonBasicProperties: shouldHaveSearchPersonBasicProperties,
    shouldHaveUserToPersonProperties: shouldHaveUserToPersonProperties,
    shouldHavePersonDetailsProperties: shouldHavePersonDetailsProperties
};


function shouldHaveUserRegisterProperties(obj) {
    obj.should.have.properties([
        'nickname',
        'email',
        'firstName',
        'lastName'
    ]);
    obj.nickname.should.not.be.empty;
    obj.email.should.not.be.empty;
}

function shouldHaveAccessTokenProperties(obj) {
    obj.should.have.properties([
        'access_token',
        'refresh_token',
        'expired'
    ]);
}

function shouldHaveCreditProperties(obj) {
    obj.should.have.properties([
        'id',
        'creditType',
        'mediaType',
        'tmdbId',
        'movie',
        'person',
        'like'
    ]);
    obj.should.have.any.properties([
        'character',
        'job',
        'department'
    ]);

    should.exist(obj.id);
    obj.id.should.be.a.Number;
    obj.creditType.should.be.a.String;
    obj.mediaType.should.be.a.String;
    should.exist(obj.movie);
    obj.movie.should.be.a.Number;
    should.exist(obj.person);
    obj.person.should.be.a.Number;
}

function shouldHaveCreditPropertiesWithShortPersonInfo(obj) {
    shouldHaveCreditProperties(obj);

    obj.should.have.properties([
        'personName',
        'personProfilePath'
    ]);

    should(obj.personName).not.be.empty;
}

function shouldHaveCreditPropertiesWithShortMovieInfo(obj) {
    shouldHaveCreditProperties(obj);

    obj.should.have.properties([
        'moviePosterPath'
    ]);
}

function shouldUserToCreditHaveProperties(obj) {
    obj.should.have.properties([
        'credit',
        'like'
    ]);
    should.exist(obj.credit);
    obj.like.should.be.a.Boolean;
}

function shouldHaveMovieBasicProperties(obj) {
    obj.should.have.properties([
        'id',
        'adult',
        'originalTitle',
        'title',
        'releaseDate',
        'posterPath',
        'backdropPath',
        'budget',
        'revenue',
        'homepage',
        'runtime',
        'status',
        'tagline',
        'overview',
        'imdbId',
        'tmdbId',
        'tmdbRateAverage',
        'tmdbVoteCount',

        'like',
        'wantToWatch',
        'notToOffer',
        'rate'
    ]);

    should.exist(obj.id);
    obj.id.should.be.a.Number;
    obj.originalTitle.should.not.be.empty;
    obj.wantToWatch.should.be.a.Boolean;
    obj.rate.should.be.a.Number;
}

function shouldHaveSearchMovieBasicProperties(obj) {
    obj.should.have.properties([
        'id',
        'adult',
        'title',
        'releaseDate',
        'posterPath',
        'backdropPath',
        'runtime',
        'status',
        'tmdbId',
        'tmdbRateAverage',
        'tmdbVoteCount',

        'like',
        'wantToWatch',
        'notToOffer',
        'rate'
    ]);

    should.exist(obj.id);
    obj.id.should.be.a.Number;
    obj.title.should.not.be.empty;
    obj.wantToWatch.should.be.a.Boolean;
    obj.rate.should.be.a.Number;
}

function shouldHaveUserToMovieProperties(obj) {
    obj.should.have.properties([
        'movie',
        'like',
        'wantToWatch',
        'notToOffer',
        'rate'
    ]);

    should.exist(obj.movie);
}

function shouldHaveMovieDetailsProperties(obj) {
    shouldHaveMovieBasicProperties(obj);

    obj.should.have.property('credits');
    obj.credits.should.be.an.Array;
    _.forEach(obj.credits, shouldHaveCreditPropertiesWithShortPersonInfo);
}

function shouldHavePersonBasicProperties(obj) {
    obj.should.have.properties([
        'id',
        'name',
        'profilePath',
        'biography',
        'birthday',
        'deathday',
        'homepage',
        'placeOfBirth',
        'tmdbId',

        'favorite'
    ]);

    should.exist(obj.id);
    obj.id.should.be.a.Number;
    obj.name.should.not.be.empty;
}

function shouldHaveSearchPersonBasicProperties(obj) {
    obj.should.have.properties([
        'id',
        'name',
        'profilePath',
        'birthday',
        'deathday',
        'tmdbId',

        'favorite'
    ]);

    should.exist(obj.id);
    obj.id.should.be.a.Number;
    obj.name.should.not.be.empty;
}

function shouldHaveUserToPersonProperties(obj) {
    obj.should.have.properties([
        'person',
        'favorite'
    ]);

    should.exist(obj.person);
    obj.favorite.should.be.a.Boolean;
}

function shouldHavePersonDetailsProperties(obj) {
    shouldHavePersonBasicProperties(obj);
    obj.should.have.property('credits');
    obj.credits.should.be.an.Array;
    _.forEach(obj.credits, shouldHaveCreditPropertiesWithShortMovieInfo);
}