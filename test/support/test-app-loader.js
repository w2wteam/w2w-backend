/**
 * Created by alesanro on 3/15/15.
 */

module.exports = function testAppLoader(opts, cb) {
    var path = require('path');
    var architect = require("architect");
    var util = require('util');

    var testCaseFileName = path.basename(opts.filename, '.js');
    var testCasePath = opts.path;
    var configPath = path.join(testCasePath, util.format("%s.config.js", testCaseFileName));
    var config = architect.loadConfig(configPath);

    architect.createApp(config, function (err, app) {
        if (err) throw err;

        cb(app);
    });
};