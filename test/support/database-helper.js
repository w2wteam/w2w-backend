/**
 * Created by alex_rudyak on 3/13/15.
 */

var Q = require('q');
var _ = require('lodash');

module.exports = function(database) {
    "use strict";

    return {
        clearDB: function(cb) {
            Q.nbind(database.db.drop, database.db)()
                .then(function() {
                    return Q.nbind(database.db.sync, database.db)();
                })
                .then(function() {
                    cb(null);
                })
                .fail(cb);
        },

        /**
         * Clear and sync database for passed models
         *
         * @param {Array} models collection of models
         * @param {Function} cb callback
         */
        clearDBWithSyncOfModels: function(models, cb) {
            Q.nbind(database.db.drop, database.db)()
                .then(function() {
                    return Q.all(_.map(models, function(model) {
                        return Q.nbind(model.sync, model)();
                    }));
                })
                .then(function() {
                    cb(null);
                })
                .fail(cb);
        },

        removeUser: function(cb) {
            cb(null);
        }
    };
};