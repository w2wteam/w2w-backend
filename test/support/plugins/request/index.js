/**
 * Created by alesanro on 3/15/15.
 */

var _ = require('lodash');
var Route = require('../../../../node_modules/express/lib/router/layer');
var util = require('util');
var config = require('config');

module.exports = function(options, imports, register) {
    function makeRoute(path) {
        var host = config.get('Backend.tmdb.host');
        return util.format('%s%s', host, path);
    }

    var resourcesPath = "../../resources/tmdb-responses/";
    var routes = [
        new Route(makeRoute("/movie/:id"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "movie.json")));
        }),
        new Route(makeRoute("/movie/:id/credits"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "movie-credits.json")));
        }),
        new Route(makeRoute("/person/:id"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "person.json")));
        }),
        new Route(makeRoute("/person/:id/credits"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "person-credits.json")));
        }),
        new Route(makeRoute("/search/movie"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "search-movies.json")));
        }),
        new Route(makeRoute("/search/person"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "search-people.json")));
        }),
        new Route(makeRoute("/discover/movie"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "discover-movies.json")));
        }),
        new Route(makeRoute("/genre/movie/list"), {}, function(options, callback) {
            return callback(null, {}, wrapResponseObject(require(resourcesPath + "genres-movie-list.json")));
        })
    ];

    register(null, {
        request: {
            get: function (options, callback) {
                console.assert(options.method.toUpperCase() === "GET");
                console.assert(options.headers['Accept'].toLowerCase() === "application/json");

                var query = options.uri.indexOf('?');
                var pathLength = query ? query : options.uri.length;
                var uri = options.uri.slice(0, pathLength);
                var route = _.find(routes, function (route) {
                    return route.match(uri);
                });

                if (!route) {
                    return callback(new Error("Route not found in stub object!"), {}, "{}");
                }

                route.handle(options, callback);
            }
        }
    });

    function wrapResponseObject(obj) {
        return JSON.stringify(obj);
    }
};