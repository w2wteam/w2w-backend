/**
 * Created by alesanro on 3/15/15.
 */

module.exports = function(options, imports, register) {
    require('../../../../../../external-services/tmdb/tmdb')(options, imports, function(err, modules) {
        register(err, {
            movieServiceAPI: modules.tmdbAPI
        });
    });
};