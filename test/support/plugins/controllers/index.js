/**
 * Created by alesanro on 3/15/15.
 */

module.exports = function(options, imports, register) {
    require('../../../../api/controllers')(options, imports, register);
};