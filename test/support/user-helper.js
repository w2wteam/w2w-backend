/**
 * Created by alesanro on 3/15/15.
 */

var Q = require('q');
var _ = require('lodash');

module.exports = function(providers, database) {
    "use strict";
    var user;

    var validNickname = 'test';
    var validEmail = 'test@email.com';
    var validPassword = '123qwe';

    return {
        getUser: function() {
            if (user) {
                return Q(user);
            }

            return providers.UserAuthenticationProvider.registerUser(validNickname, validEmail, validPassword)
                .then(function(newUser) {
                    user = newUser;
                    return Q(newUser);
                });
        },

        removeUser: function() {
            if (!user) {
                return Q(null);
            }

            return Q.nfcall(user.remove)
                .then(function() {
                    user = null;
                    return Q(null);
                })
        },

        getBearerAccess: function() {
            if (!user) {
                return Q.reject(new Error("You only can get access for existed user"));
            }

            return providers.UserAuthenticationProvider.generateBearerToken(user);
        },

        getCredits: createBunchOfCredits,
        getMovies: createBunchOfMovies,
        getPeople: createBunchOfPeople,

        password: validPassword
    };

    function createBunchOfMovies() {
        var searchQuery = 'matrix';

        return providers.MovieProvider.getSearchMovieWithQuery(searchQuery);
    }

    function createBunchOfPeople() {
        var searchQuery = 'brad pitt';

        return providers.PersonProvider.getSearchPersonWithQuery(searchQuery);
    }

    function createBunchOfCredits() {
        return Q.all([createBunchOfMovies(), createBunchOfPeople()])
            .spread(function(movies, people) {
                return Q.all([
                    providers.MovieProvider.getMovieDetails(_.result(_.first(movies), 'id')),
                    providers.PersonProvider.getPersonDetails(_.result(_.first(people), 'id'))
                ]);
            })
            .spread(function(movie, person) {
                return _.flatten([movie.credits, person.credits]);
            });
    }
};