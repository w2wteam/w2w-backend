/**
 * Created by alex_rudyak on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');

describe("ErrorScope", function() {
    var app;
    var errorScope;
    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            errorScope = app.getService("wErrorScope");
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    describe("error reason", function() {
        describe("with description", function() {
            it("should exists for each reason", function() {
                _.forEach(errorScope.W2WReasonKeys, function(value, key) {
                    (value).should.not.be.empty;
                    var reason = new errorScope.W2WErrorReason(value);
                    (reason.title).should.not.be.empty;
                    (reason.text).should.not.be.empty;
                    reason.priority.should.not.be.equal(0);
                });
            });
        });

        it("should exist", function() {
            should.exist(errorScope.W2WErrorReason);
        });

        it("should be created with no parameters", function() {
            var reason = new errorScope.W2WErrorReason();
            should.exist(reason);
            (reason.title).should.not.be.empty;
            (reason.text).should.not.be.empty;
            reason.priority.should.be.equal(0);
        });
    });

    describe("base classes", function() {
        it("should exist", function() {
            should.exist(errorScope.classes.W2WError);
            should.exist(errorScope.classes.W2WDbError);
            should.exist(errorScope.classes.W2WTmdbError);
        });

        it("should have name", function() {
            _.forEach(errorScope.classes, function(value, key) {
                value.prototype.name.should.not.be.empty;
            });
        });

        describe("when was instantiated", function() {
            it("should have empty reasons at the beginning", function () {
                _.forEach(errorScope.classes, function (value, key) {
                    var errorInstance = new value();
                    Array(errorInstance.reasons);
                    errorInstance.reasons.should.be.empty;
                });
            });

            it("should have one reason", function() {
                var reason = new errorScope.W2WErrorReason(errorScope.W2WReasonKeys.WResourceNotFound);
                _.forEach(errorScope.classes, function(value, key) {
                    var errorInstance = new value("message", errorScope.W2WErrorCodes.WNotFound, reason);
                    errorInstance.reasons.should.not.be.empty;
                    _.size(errorInstance.reasons).should.be.equal(1);
                    _.first(errorInstance.reasons).should.be.equal(reason);
                });
            });

            it("should have an empty reason", function() {
                _.forEach(errorScope.classes, function(value, key) {
                    var errorInstance = new value("message", errorScope.W2WErrorCodes.WNotFound, {});
                    errorInstance.reasons.should.be.empty;
                });
            });

            it("should parse object to reasons", function() {
                _.forEach(errorScope.classes, function(value, key) {
                    var errorInstance = new value("message", errorScope.W2WErrorCodes.WNotFound, {
                        "myReason": "1",
                        "secondMyReason": "2"
                    });
                    errorInstance.reasons.should.not.be.empty;
                    _.size(errorInstance.reasons).should.be.equal(2);
                });
            });

            it("should have message", function () {
                var messageText = "message";
                _.forEach(errorScope.classes, function (value, key) {
                    var errorInstance = new value(messageText);
                    errorInstance.message.should.be.equal(messageText);
                });
            });

            it("should have equal type after initialization", function() {
                _.forEach(errorScope.classes, function (value, key) {
                    var errorType = errorScope.W2WErrorCodes.WNotFound;
                    var errorInstance = new value("", errorType);
                    errorInstance.type.should.be.equal(errorType);
                });
            });
        });
    });
});