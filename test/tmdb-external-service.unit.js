/**
 * Created by alex_rudyak on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');

describe("The MovieDB external", function() {
    "use strict";
    var app;
    var tmdbAPI;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            tmdbAPI = app.getService('movieServiceAPI');
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    it("should exist after creation", function() {
        should.exist(tmdbAPI);
    });

    describe("when making GET", function() {
        it("/movie/:id should return response", function(done) {
            tmdbAPI.movie.getById(1)
                .then(function(movie) {
                    should.exist(movie);
                    done();
                })
                .fail(done);
        });

        it("/movie/:id/credits should return response", function(done) {
            tmdbAPI.movie.getCredits(1)
                .then(function(credits) {
                    should.exist(credits);
                    done();
                })
                .fail(done);
        });

        it("/person/:id should return response", function(done) {
            tmdbAPI.person.getById(1)
                .then(function(person) {
                    should.exist(person);
                    done();
                })
                .fail(done);
        });

        it("/person/:id/credits should return response", function(done) {
            tmdbAPI.person.getCredits(1)
                .then(function(credits) {
                    should.exist(credits);
                    done();
                })
                .fail(done);
        });

        it("/search/movie should return response", function(done) {
            tmdbAPI.search.getSearchMovie({ query: 'matrix' })
                .then(function(movies) {
                    should.exist(movies);
                    should(movies).be.an.Array;
                    should(movies).not.be.empty;
                    done();
                })
                .fail(done);
        });

        it("/search/person should return response", function(done) {
            tmdbAPI.search.getSearchPerson({ query: 'brad pitt' })
                .then(function(people) {
                    should.exist(people);
                    should(people).be.an.Array;
                    should(people).not.be.empty;
                    done();
                })
                .fail(done);
       });

        it("/discover/movie should return response", function(done) {
            tmdbAPI.discover.getMovies()
                .then(function(movies) {
                    should.exist(movies);
                    should(movies).be.an.Array;
                    should(movies).not.be.empty;
                    done();
                })
                .fail(done);
        });

        it("/genre/movie/list should return response", function(done) {
            tmdbAPI.genre.getGenres()
                .then(function(genres) {
                    should.exist(genres);
                    should(genres).be.an.Array;
                    should(genres).not.be.empty;
                    _.forEach(genres, function(item) {
                        should(item).be.a.String;
                    });
                    done();
                })
                .fail(done);
        });
    });
});