/**
 * Created by alesanro on 3/4/15.
 */

var should = require('should');
var _ = require('lodash');
var Q = require('q');

describe('Facade-Person', function() {
    "use strict";
    var app;
    var peopleFacade;
    var database;
    var databaseHelper;

    before(function(done) {
        require('./support/test-app-loader')({
            filename: __filename,
            path: __dirname
        }, function(application) {
            app = application;
            peopleFacade = app.getService('moviesFacade').People;
            database = app.getService('database');
            databaseHelper = require('./support/database-helper')(database);
            done();
        });
    });

    after(function() {
        app.destroy();
    });

    describe('search', function() {
        var searchQuery = 'brad pitt';

        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should save results in database and return non-empty collection', function(done) {
            peopleFacade.searchPersonWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    should(searchResults).be.an.Array;
                    searchResults.should.not.be.empty;

                    return Q.nfcall(database.models.Person.find)
                        .then(function(dbPeople) {
                            should.exist(dbPeople);
                            dbPeople.should.not.be.empty;
                            dbPeople.should.be.length(searchResults.length);
                            done();
                        });
                })
                .fail(done);
        });
    });

    describe('get by id', function() {
        var searchQuery = 'brad pitt';

        beforeEach(function(done) {
            databaseHelper.clearDB(done);
        });

        it('should return movie for existed', function(done) {
            peopleFacade.searchPersonWithQuery(searchQuery)
                .then(function(searchResults) {
                    should.exist(searchResults);
                    searchResults.should.not.be.empty;

                    var person = _.first(searchResults);
                    return peopleFacade.getById(person.id)
                        .then(function(dbPerson) {
                            should.exist(dbPerson);
                            should(dbPerson).be.an.Object;
                            dbPerson.id.should.be.equal(person.id);

                            done();
                        });
                })
                .fail(done);
        });

        it('should throw error for non-existed id', function(done) {
            peopleFacade.getById(1)
                .then(function() {
                   done(new Error('Should be an error for not existed user'));
                })
                .fail(function(err) {
                    should.exist(err);
                    should(err).be.an.instanceOf(Error);

                    done();
                });
        });
    });
});