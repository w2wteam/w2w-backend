/**
 * Created by alesanro on 3/15/15.
 */

module.exports = [
    "./support/plugins/logger",
    "./support/plugins/cache",
    "./support/plugins/request",
    "./support/plugins/error-scope",
    "./support/plugins/external-service/tmdb/global"
];