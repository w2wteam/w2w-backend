/**
 * Created by alesanro on 11/10/14.
 */

var _ = require('lodash');
var config = require('config');
var orm = require('orm');

module.exports = function(options, imports, register) {
    var log = imports.logger;

    var connection = null;
    var settings = {
        protocol: "mysql",
        query: {
            pool: "true",
            debug: (process.env.NODE_ENV === 'development' ? 'true' : 'false')
        },
        host: eval(config.get('Backend.db.host')),
        port: eval(config.get('Backend.db.port')),
        database: config.get('Backend.db.name'),
        user: config.get('Backend.db.user'),
        password: config.get('Backend.db.password')
    };

    function setup(db, cb) {
        var models = require('./models');
        for(var modelName in models) {
            if (_.isFunction(models[modelName])) {
                models[modelName](orm, db);
            }
        }

        return cb(null, db);
    }

    var _inside = false;

    (function getConnection(cb) {
        if (connection) {
            return cb(null, connection);
        }

        if (_inside) {
            var timerId = setTimeout(function() {
                clearTimeout(timerId);
                getConnection(cb);
            }, 100);
            return;
        }

        _inside = true;
        orm.connect(settings, function(err, db) {
            _inside = false;

            if (err) {
                log.warn(settings);
                log.error(err);
                return cb(err, null);
            }

            connection = db;
            db.settings.set('instance.returnAllErrors', true);
            db.settings.set('instance.cache', false);
            db.settings.set('connection.reconnect', true);
            setup(db, cb);
            log.info("Connection to db successful!");
        });
    })(function(err, db) {
        if (err) {
            return register(err);
        }
        register(null, {
            database: {
                db: db,
                models: db.models
            },

            onDestroy: function() {
                db.close();
            }
        });
    });
};

