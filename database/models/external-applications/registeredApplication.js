/**
 * Created by alesanro on 11/15/14.
 */

module.exports = function(orm, db) {
    var Application = db.define('RegisteredApplication', {
        id: {
            type: 'serial',
            size: 8,
            key: true,
            unique: true,
            required: false,
            mapsTo: 'id'
        },
        applicationName: {
            type: 'text',
            size: 128,
            unique: true,
            required: true,
            mapsTo: 'application_name'
        },
        registrationDate: {
            type: 'date',
            time: true,
            required: true,
            mapsTo: 'registration_date'
        },
        clientId: {
            type: 'text',
            size: 256,
           /* unique: true,*/
            required: true,
            mapsTo: 'client_id'
        },
        clientSecret: {
            type: 'text',
            size: 256,
            required: true,
            mapsTo: 'client_secret'
        }
    }, {});
};