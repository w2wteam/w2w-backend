/**
 * Created by alesanro on 11/9/14.
 */

var utils = require('../../helpers').utils;


module.exports = function(orm, db) {
    var UserRelationsToCredit = db.define('UserRelationsToCredit', {
        id: {
            type: 'serial',
            key: true,
            unique: true,
            mapsTo: 'id'
        },
        like: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: 'like'
        },
        likeUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'like_updated_at'
        }
    }, {
        hooks: {
            beforeSave: function() {
                this.likeUpdatedAt = utils.date.now();
            }
        }
    });

    UserRelationsToCredit.hasOne('credit', db.models.Credit, {
        required: true,
        key: true,
        autoFetch: false,
        mapsTo: 'credit_id'
    });
    UserRelationsToCredit.hasOne('user', db.models.UserAccount, {
        required: true,
        autoFetch: false,
        reverse: 'likedCredits',
        mapsTo: 'user_id',
        key: true
    });

    UserRelationsToCredit.markObjectWithCredit = function(obj, credit) {
        if (credit) {
            obj.like = credit.like;
        } else {
            obj.like = false;
        }

        return obj;
    };
};