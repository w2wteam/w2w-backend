/**
 * Created by alesanro on 11/9/14.
 */

var _ = require('lodash');
var utils = require('../../helpers').utils;


module.exports = function(orm, db) {
    var Movie = db.define('Movie', {
        id: {
            type: 'serial',
            key: true,
            unique: true,
            mapsTo: 'id'
        },
        adult: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: 'adult'
        },
        originalTitle: {
            type: 'text',
            size: 128,
            mapsTo: 'original_title'
        },
        title: {
            type: 'text',
            size: 128,
            mapsTo: 'title'
        },
        releaseDate: {
            type: 'date',
            time: false,
            mapsTo: 'release_date'
        },
        posterPath: {
            type: 'text',
            size: 128,
            mapsTo: 'poster_path'
        },
        backdropPath: {
            type: 'text',
            size: 128,
            mapsTo: 'backdrop_path'
        },
        budget: {
            type: 'integer',
            size: 4,
            unsigned: true,
            mapsTo: 'budget'
        },
        revenue: {
            type: 'integer',
            size: 4,
            mapsTo: 'revenue'
        },
        homepage: {
            type: 'text',
            size: 128,
            mapsTo: 'homepage'
        },
        runtime: {
            type: 'integer',
            size: 2,
            unsigned: true,
            mapsTo: 'runtime'
        },
        status: {
            type: 'enum',
            values: ['Production', 'Released'],
            defaultValue: 'Production',
            mapsTo: 'status'
        },
        tagline: {
            type: 'text',
            big: true,
            mapsTo: 'tagline'
        },
        overview: {
            type: 'text',
            size: 512,
            mapsTo: 'overview'
        },
        imdbId: {
            type: 'text',
            size: 64,
            mapsTo: 'imdb_id'
        },
        tmdbId: {
            type: 'integer',
            size: 4,
            unsigned: true,
            mapsTo: 'tmdb_id'
        },
        tmdbRateAverage: {
            type: 'integer',
            size: 2,
            unsigned: true,
            mapsTo: 'tmdb_rate_average'
        },
        tmdbVoteCount: {
            type: 'integer',
            size: 4,
            unsigned: true,
            mapsTo: 'tmdb_vote_count'
        },
        lastUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'last_updated_at'
        },
        _creditsLoaded: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: '_credits_loaded'
        },
        _thumb: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: '_thumb'
        }
    }, {
        methods: {
            populateWithCredits: function(credits) {
                this.credits = credits;

                return this;
            },

            populateWithGenres: function(genres) {
                this.genres = genres;

                return this;
            },

            shouldUpdateCredits: function() {
                return true;
            },

            updateWithMovie: function(movie) {
                if (movie) {
                    for(var prop in movie) {
                        this[prop] = movie[prop];
                    }
                }
                return this;
            }
        },

        hooks: {
            beforeSave: function(next) {
                this.lastUpdatedAt = utils.date.now();
                if (!this.status) {
                    this.status = this.__opts.fieldToPropertyMap.status.defaultValue;
                }
                if (this.rate === null) {
                    this.rate = 0;
                }
                next();
            }
        }
    });

    Movie.hasMany('genres', db.models.Genre, {}, {
        autoFetch: true,
        mergeTable: 'MovieGenres',
        mergeId: 'movie_id',
        mergeAssocId: 'genre_id',
        key: true
    });

    Movie.toPlainObject = function(obj) {
        var self = obj;
        var genres = _.map(self.genres, function(genre) {
            if (typeof genre !== 'string') {
                return genre.genreLabel;
            }
            return genre;
        });

        self.genres = genres;
        return self;
    };

    Movie.externalLinks = function() {
        return {
            tmdb: this.tmdbId
        };
    };
};