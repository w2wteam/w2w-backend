module.exports = function(orm, db) {
  var Genre = db.define('Genre', {
     id: {
         type: 'serial',
         key: true,
         unique: true,
         required: false,
         mapsTo: 'id'
     },
      genreLabel: {
          type: 'text',
          size: 64,
          required: true,
          mapsTo: 'label'
      }
  });
};