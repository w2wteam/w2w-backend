/**
 * Created by alesanro on 11/9/14.
 */

var _ = require('lodash');

module.exports = {
    genre: require('./genre'),
    movie: require('./movie'),
    userAccount: require('./userAccount'),
    person: require('./person'),
    credit: require('./credit'),
    userRelationsToPerson: require('./userRelationsToPerson'),
    userRelationsToCredit: require('./userRelationsToCredit'),
    userRelationsToMovie: require('./userRelationsToMovie')
};

_.assign(module.exports, require('./external-applications/index'));
_.assign(module.exports, require('./security/index'));