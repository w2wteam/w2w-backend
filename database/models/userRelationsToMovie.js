/**
 * Created by alesanro on 11/10/14.
 */

module.exports = function(orm, db) {
    var UserRelationsToMovie = db.define('UserRelationsToMovie', {
        id: {
            type: 'serial',
            key: true,
            required: false,
            unique: true,
            mapsTo: 'id'
        },
        like: {
            type: 'enum',
            values: ['unknown','like','dislike'],
            defaultValue: 'unknown',
            mapsTo: 'like'
        },
        rate: {
            type: 'integer',
            size: 2,
            unique: false,
            defaultValue: 0,
            mapsTo: 'rate'
        },
        isRated: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: 'is_rated'
        },
        wantToWatch: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: 'want_to_watch'
        },
        notToOffer: {
            type: 'enum',
            values: ['unknown', 'yes', 'no'],
            defaultValue: 'unknown',
            mapsTo: 'not_to_offer'
        },
        likeUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'like_updated_at'
        },
        wantToWatchUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'want_to_watch_updated_at'
        },
        notToOfferUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'not_to_offer_updated_at'
        },
        rateUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'rate_updated_at'
        }
    }, {});

    UserRelationsToMovie.hasOne('movie', db.models.Movie, {
        required: true,
        autoFetch: true,
        mapsTo: 'movie_id'
    });
    UserRelationsToMovie.hasOne('user', db.models.UserAccount, {
        required: true,
        reverse: 'markedMovies',
        autoFetch: false,
        mapsTo: 'user_id'
    });

    UserRelationsToMovie.markObjectWithMovie = function(obj, movie) {
        if (movie) {
            obj.like = movie.like;
            obj.wantToWatch = movie.wantToWatch;
            obj.notToOffer = movie.notToOffer;
            obj.rate = movie.rate;
        } else {
            obj.like = 'unknown';
            obj.wantToWatch = false;
            obj.notToOffer = 'unknown';
            obj.rate = 0;
        }

        return obj;
    };
};