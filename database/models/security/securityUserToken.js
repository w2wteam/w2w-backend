/**
 * Created by alesanro on 11/15/14.
 */

module.exports = function(orm, db) {
    require('../helper_types')(orm, db);

    var UserToken = db.define('SecurityUserToken', {
        id: {
            type: 'serial',
            size: 8,
            key: true,
            unique: true,
            require: false,
            mapsTo: 'id'
        },
        accessToken: {
            type: 'text',
            size: 256,
            required: true,
            mapsTo: 'access_token'
        },
        refreshToken: {
            type: 'text',
            size: 256,
            required: true,
            mapsTo: 'refresh_token'
        },
        scope: {
            type: 'stringArray',
            required: true,
            mapsTo: 'scope'
        },
        expiredIn: {
            type: 'date',
            time: true,
            required: true,
            mapsTo: 'expired_in'
        }
    }, {});

    UserToken.hasOne('user', db.models.UserAccount, {
        required: true,
        autoFetch: false,
        reverse: 'tokens',
        mapsTo: 'user_id'
    });
};