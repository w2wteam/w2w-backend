module.exports = {
    securityScope: require('./securityScope'),
    securityUserToken: require('./securityUserToken'),
    securityUserAuthorizationCode: require('./securityUserAuthorizationCode')
};