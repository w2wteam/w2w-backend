/**
 * Created by alesanro on 11/15/14.
 */

module.exports = function(orm, db) {
    var Scope = db.define('SecurityScope', {
        id: {
            type: 'serial',
            size: 4,
            key: true,
            unique: true,
            required: false,
            mapsTo: 'id'
        },
        name: {
            type: 'text',
            size: 32,
            unique: true,
            required: true,
            mapsTo: 'name'
        }
    }, {});
};