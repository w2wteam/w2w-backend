/**
 * Created by alesanro on 11/16/14.
 */

module.exports = function(orm, db) {
    var AuthorizationCode = db.define('SecurityUserAuthorizationCode', {
        id: {
            type: 'serial',
            size: 8,
            key: true,
            unique: true,
            required: true,
            mapsTo: 'id'
        },
        authorizationCode: {
            type: 'text',
            size: 256,
            required: true,
            mapsTo: 'authorization_code'
        },
        redirectUri: {
            type: 'text',
            mapsTo: 'redirect_uri'
        }
    }, {});

    AuthorizationCode.hasOne('user', db.models.UserAccount, {
        required: true,
        reverse: 'codes',
        autoFetch: false,
        mapsTo: 'user_id'
    });
    AuthorizationCode.hasOne('application', db.models.RegisteredApplication, {
        required: true,
        autoFetch: false,
        mapsTo: 'application_id'
    });
};