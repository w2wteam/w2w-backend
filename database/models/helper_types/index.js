/**
 * Created by alesanro on 11/15/14.
 */

module.exports = function(orm, db) {
    db.defineType('stringArray', {
        datastoreType: function(prop) {
            return 'TEXT'
        },
        // This is optional
        valueToProperty: function(value, prop) {
            if (Array.isArray(value)) {
                return value;
            } else {
                return (value && value.split(',')) || [];
            }
        },
        // This is also optional
        propertyToValue: function(value, prop) {
            return value.join(',')
        }
    });
};