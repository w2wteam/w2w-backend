/**
 * Created by alesanro on 11/9/14.
 */

var _ = require('lodash');
var utils = require('../../helpers').utils;


module.exports = function(orm, db) {
    var Person = db.define('Person', {
        id: {
            type: 'serial',
            key: true,
            unique: true,
            mapsTo: 'id'
        },
        name: {
            type: 'text',
            size: 128,
            required: true,
            mapsTo: 'name'
        },
        alsoKnownAs: {
            type: 'text',
            size: 128,
            mapsTo: 'also_known_as'
        },
        profilePath: {
            type: 'text',
            size: 128,
            mapsTo: 'profile_path'
        },
        biography: {
            type: 'text',
            big: true,
            mapsTo: 'biography'
        },
        birthday: {
            type: 'date',
            time: false,
            defaultValue: null,
            mapsTo: 'birthday'
        },
        deathday: {
            type: 'date',
            time: false,
            defaulValue: null,
            mapsTo: 'deathday'
        },
        homepage: {
            type: 'text',
            size: 128,
            mapsTo: 'homepage'
        },
        placeOfBirth: {
            type: 'text',
            size: 128,
            mapsTo: 'place_of_birth'
        },
        tmdbId: {
            type: 'integer',
            size: 4,
            unsigned: true,
            required: false,
            mapsTo: 'tmdb_id'
        },
        lastUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'last_updated_at'
        },
        _creditsLoaded: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: '_credits_loaded'
        },
        _thumb: {
            type: 'boolean',
            defaultValue: false,
            mapsTo: '_thumb'
        }
    }, {
        methods: {
            shouldUpdateCredits: function() {
                return true;
            },

            populateWithCredits: function(credits) {
                this.credits = credits;

                return this;
            },

            updateWithPerson: function(person) {
                if (person) {
                    for(var prop in person) {
                        this[prop] = person[prop];
                    }
                }
                return this;
            }
        },

        hooks: {
            beforeSave: function(next) {
                this.lastUpdatedAt = utils.date.now();
                next();
            }
        }
    });

    Person.externalLinks = function() {
        return {
            tmdb: this.tmdbId
        };
    };
};