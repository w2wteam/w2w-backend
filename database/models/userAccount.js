/**
 * Created by alesanro on 11/9/14.
 */

module.exports = function(orm, db) {
    var UserAccount = db.define('UserAccount', {
        id: {
            type: 'serial',
            size: 8,
            key: true,
            unique: true,
            required: false,
            mapsTo: 'id'
        },
        nickname: {
            type: 'text',
            size: 64,
            required: true,
            unique: true,
            mapsTo: 'nickname'
        },
        email: {
            type: 'text',
            size: 128,
            required: true,
            unique: true,
            mapsTo: 'email'
        },
        passwordHash: {
            type: 'text',
            size: 256,
            required: true,
            mapsTo: 'password_hash'
        },
        salt: {
            type: 'text',
            size: 128,
            required: true,
            mapsTo: 'salt'
        },
        firstName: {
            type: 'text',
            size: 64,
            mapsTo: 'first_name'
        },
        lastName: {
            type: 'text',
            size: 64,
            mapsTo: 'last_name'
        }
    }, {
        validations: {
            nickname: orm.enforce.security.username({length:2}),
            email: orm.enforce.patterns.email()
        },

        methods: {
            toInternalObject: function () {
                var self = this;
                return {
                    nickname: self.nickname,
                    email: self.email,
                    id: self.id
                };
            }
        }
    });
};