/**
 * Created by alesanro on 11/10/14.
 */

module.exports = function(orm, db) {
    var UserRelationsToPerson = db.define('UserRelationsToPerson', {
        id: {
            type: 'serial',
            key: true,
            required: false,
            unique: true,
            mapsTo: 'id'
        },
        favorite: {
            type: 'boolean',
            mapsTo: 'favorite'
        },
        favoriteUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'favorite_updated_at'
        }
    }, {
        hooks: {
            beforeSave: function() {
                this.favoriteUpdatedAt = new Date();
            }
        }
    });

    UserRelationsToPerson.hasOne('user', db.models.UserAccount, {
        required: true,
        reverse: 'likedPersons',
        autoFetch: false,
        mapsTo: 'user_id'
    });
    UserRelationsToPerson.hasOne('person', db.models.Person, {
        required: true,
        autoFetch: false,
        mapsTo: 'person_id'
    });

    UserRelationsToPerson.markObjectWithPerson = function(obj, person) {
        if (person) {
            obj.favorite = person.favorite;
        } else {
            obj.favorite = false;
        }

        return obj;
    }
};