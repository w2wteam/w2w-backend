/**
 * Created by alesanro on 11/9/14.
 */

var _ = require('lodash');
var utils = require('../../helpers').utils;


module.exports = function(orm, db) {
    var Credit = db.define('Credit', {
        id: {
            type: 'serial',
            key: true,
            unique: true,
            mapsTo: 'id'
        },
        creditType: {
            type: 'enum',
            values: ['cast','crew'],
            defaultValue: 'cast',
            mapsTo: 'credit_type'
        },
        department: {
            type: 'text',
            size: 64,
            defaultValue: null,
            mapsTo: 'department'
        },
        job: {
            type: 'text',
            size: 64,
            defaultValue: null,
            mapsTo: 'job'
        },
        character: {
            type: 'text',
            size: 64,
            defaultValue: null,
            mapsTo: 'character'
        },
        mediaType: {
            type: 'enum',
            values: ['movie','tv'],
            defaultValue: 'movie',
            mapsTo: 'media_type'
        },
        tmdbId: {
            type: 'text',
            size: 64,
            defaultValue: null,
            mapsTo: 'tmdb_id'
        },
        lastUpdatedAt: {
            type: 'date',
            time: true,
            mapsTo: 'last_updated_at'
        }
    }, {
        methods: {
            updateWithCredit: function(credit) {
                if (credit) {
                    for(var prop in credit) {
                        this[prop] = credit[prop];
                    }
                }
                return this;
            }
        },

        hooks: {
            beforeSave: function(next) {
                this.lastUpdatedAt = utils.date.now();
                next();
            }
        }
    });

    Credit.externalLinks = function() {
        return {
            tmdb: this.tmdbId
        };
    };

    Credit.hasOne('person', db.models.Person, {
        required: true,
        reverse: 'credits',
        autoFetch: false,
        mapsTo: 'person_id'
    });
    Credit.hasOne('movie', db.models.Movie, {
       required: true,
        reverse: 'credits',
        autoFetch: false,
        mapsTo: 'movie_id'
    });
};
