[![Build Status](https://api.shippable.com/projects/54c238f15ab6cc1352898f10/badge?branchName=development)](https://app.shippable.com/projects/54c238f15ab6cc1352898f10/builds/latest)
[![codecov.io](https://codecov.io/bitbucket/w2wteam/w2w-backend/coverage.svg?branch=development)](https://codecov.io/bitbucket/w2wteam/w2w-backend?branch=development)

# W2W #
---
## Main purpose ##
Develop service with movie recommendations and users' preferences.

## Simple API documentation ##
Documentation lays on [Apiary Service](http://docs.w2w.apiary.io/)


## Caching external responses
We use Redis for that purpose. So far redis caching only used for development.
For using redis it should be started with configuration at path **/usr/local/etc/redis.conf**
and set up `daemonize yes`.